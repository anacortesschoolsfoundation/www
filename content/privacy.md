---
title: Privacy Policy
bannerImage:
description: ASF Privacy Policy
keywords: anacortes schools foundation, Privacy Policy
featuredImage: https://asfmedia.imgix.net/logo@2x.png
layout: blocks
content_blocks:
  - _bookshop_name: hero
    title: Privacy Policy
    bannerImage:
  - _bookshop_name: content
    title:
    content: >-
      Anacortes Schools Foundation is focused on protecting your privacy. This
      privacy policy was created to outline ASF's commitment to the privacy of
      our donors, members, event attendees, and website users. This policy
      explains what types of information are collected by Anacortes Schools
      Foundation and how that information is used. Information is collected via
      the following websites:
      [www.anacortesschoolsfoundation.org](http://www.anacortesschoolsfoundation.org)
      and [www.asfkids.org](http://www.asfkids.org).


      ### Our Commitment to Our Donors


      We will not sell, share or trade our donors' names or personal information
      with any other entity, nor send mailings to our donors on behalf of other
      organizations. To the extent any donations are processed through a
      third-party service provider, our donors' information will only be used
      for purposes necessary to process the donation.


      ### What Personally Identifiable Information is Collected


      Individuals that register for event attendance, or e-communications at the
      Anacortes Schools Foundation websites voluntarily provide us with contact
      information (such as name, address, and e-mail address). We may use this
      information for specific, limited purposes. You may always "opt out,"
      either now or at any time in the future, if you do not wish to receive our
      messages. To opt out, send an email
      to&nbsp;[president@asfkids.org](mailto:president@asfkids.org).


      ### IP addresses


      Anacortes Schools Foundation uses your IP address to help diagnose
      problems with our server, to administer websites and for statistical
      metrics used to track website visitor traffic.


      ### Cookies


      [www.anacortesschoolsfoundation.org](http://www.anacortesschoolsfoundation.org)
      and [www.asfkids.org](http://www.asfkids.org) use "cookie" messages to
      automatically help provide better services. They remind us who you are and
      your preferences for our website based on what you've done and told us
      before. The "cookie" is placed in your computer and is read when you come
      back to our website. Cookies let us take you to the information and
      features you're particularly interested in. They also let us track your
      usage of our websites
      ([www.anacortesschoolsfoundation.org](http://www.anacortesschoolsfoundation.org)
      and [www.asfkids.org](http://www.asfkids.org)) so we know which parts of
      our sites are most popular. You can reject cookies or cancel them by
      instructing your Web browser accordingly.


      ### How Your Information May Be Used


      We use your personal information to provide you with personalized service;
      to send e-mail alerts to you; to answer your requests; to process your
      donations, to invite you to events; etc. You may choose to opt out at any
      time, which will cease all communications from us. We may also use your
      information to track visitors to our websites. This lets us see which of
      our features are the most popular so we can better serve our users' needs.
      It also lets us provide aggregate data about our traffic (not identifying
      you personally, but showing how many visitors used which features, for
      example) to outside parties.


      ### Email Privacy


      Anacortes Schools Foundation does not provide, sell, or rent email
      addresses to anyone outside the organization.


      ### External Links


      [www.anacortesschoolsfoundation.org](http://www.anacortesschoolsfoundation.org)
      and [www.asfkids.org](http://www.asfkids.org) include links to external
      websites. These links do not fall under the
      [www.anacortesschoolsfoundation.org](http://www.anacortesschoolsfoundation.org)
      and [www.asfkids.org](http://www.asfkids.org) domains, and Anacortes
      Schools Foundation is not responsible for the privacy practices or the
      content of external websites. Your use of any linked website is solely at
      your own risk.


      ### Modifications


      We may amend this privacy policy from time to time; please review it
      periodically. We maintain the option to modify this privacy at any time by
      electronic notice posted on our website. Your continued use of our website
      after the date that such notices are posted will be deemed to be your
      agreement to the changed terms.
    sectionclass: pt-8
---
