---
title: Homepage
description: >-
  Anacortes Schools Foundation supports Anacortes students and raises $145K
  annually for COVID Relief, STEM enrichment programs, Visual and Performing
  Arts programs, ASF's annual Ready to Learn Fair, Impact Summer School,
  Preschool Scholarships, and MORE!
keywords: >-
  Anacortes, School, Anacortes High School, Anacortes students, scholarships,
  reunions, ready to learn fair, school supplies, anacortes foundation, school
  foundation, anacortes scholarships
featuredImage: https://asfmedia.imgix.net/Featured-Image-2024.jpg
layout: blocks
content_blocks:
  - _bookshop_name: slideshow
    slides:
      - group: Scholarships
        heading: Nearly $380,000 awarded in 2024
        description: >-
          149 scholarships awarded to graduating seniors and current college
          students.
        image: >-
          https://asfmedia.imgix.net/447780786_788290850098548_2788432081250345503_n.jpg
        link_url: /scholarships/
        link_text: Read more
        meta: <span>Photo by EJ Harris</span>
      - group: Mental Health
        heading: Mental Health
        description: >-
          ASF Funds Mental Health supports K-5, at AMS and AHS and Social
          Emotional Programs at all schools.
        image: https://asfmedia.imgix.net/news-mental.jpg
        link_url: /programs/
        link_text: Read more
        meta: >-
          <span>Photo by <a href="https://www.pexels.com/@diohasbi"
          target="new">Dio Hasbi Saniskoro</a> from <a
          href="https://www.pexels.com/photo/people-doing-group-hand-cheer-3280130/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels"
          target="new">Pexels</a></span>
      - group: STEM
        heading: Cause for Celebration
        description: >-
          This year, we celebrate a decade of STEM support in Anacortes schools
          and the dreamers who made it a reality
        image: https://asfmedia.imgix.net/News-STEMwomen.jpg
        link_url: /news/2023-05-07-stem-women/
        link_text: Read more
        meta:
      - group: Art Enrichment
        heading: Support for Visual and Performing Arts
        description: >-
          ASF awards grants to choir, band, drama, digital media, ceramics, the
          After School Arts Program and more.
        image: https://asfmedia.imgix.net/news-arts.jpg
        link_url: /programs/
        link_text: Read more
        meta:
      - group: STEM
        heading: STEM Programs K-12
        description: >-
          ASF has a long history of providing hands-on-science and grade-level
          STEM Enrichment in schools.
        image: https://asfmedia.imgix.net/news-stem.jpg
        link_url: /programs/
        link_text: Read more
        meta:
      - group: Early Learning
        heading: Preschool Scholarships
        description: >-
          ASF offers grant funds to help make the cost of preschool affordable
          for all families.
        image: https://asfmedia.imgix.net/news-prek.jpg
        link_url: /programs/
        link_text: Read more
        meta:
      - group: Events
        heading: Ready to Learn Fair
        description: >-
          August 19, 2023 - Providing back-to-school essentials to over 500
          students who need a little extra support.
        image: https://asfmedia.imgix.net/news-rtl2.jpg
        link_url: /programs/
        link_text: Read more
        meta:
      - group: Celebrate the Season
        heading: Celebrate the Season
        description: >-
          Join us for ASF's biggest fundraiser of the year! Our Gala Fundraiser
          dinner features an online and live auction, live entertainment, an
          exquisite dinner and Raise the Paddle.
        image: https://asfmedia.imgix.net/CTS 2024 News reel photo.jpg
        link_url: /programs/celebrate-the-season/
        link_text: Read more
        meta:
      - group: Summer Learning
        heading: Impact
        description: 4 Weeks of Targeted Support for At-Risk Students K-5
        image: https://asfmedia.imgix.net/news-impact.jpg
        link_url: /programs/
        link_text: Read more
        meta:
      - group: COVID Relief
        heading: Fueling Education Fun Run
        description: >-
          Each September, we partner with Puget Sound Refinery to kick off the
          school year, bring the community together and help raise funds.
        image: https://asfmedia.imgix.net/news-FR5k.jpg
        link_url: /programs/run
        link_text: Read more
        meta:
  - _bookshop_name: stats
    stats:
      - name: Years of Giving
        text: >-
          <p>Celebrating 42 years of giving! Arnold and Cressa Houle made the
          first gift to the foundation in 1982</p>
        icon: hands-helping
        valueStart: 0
        valueEnd: 42
        refreshInterval: 100
        duration: 2000
        prefix: ''
        suffix: ''
        animation: bounceIn
      - name: Funding Awarded
        text: >-
          <p>Funding awarded for School Year 2022-23 for current programs
          including Impact, Mental Health supports, Pre-School Scholarships,
          STEM enrichment, CTE and vocational programs the Arts and more</p>
        icon: microscope
        valueStart: 1000
        valueEnd: 453460
        refreshInterval: 100
        duration: 2000
        prefix: $
        suffix: ''
        animation: bounceIn
      - name: Ready-to-Learn Fair Backpacks
        text: <p>Supply-filled backpacks delivered at ASF's Ready to Learn Fair</p>
        icon: pencil
        valueStart: 100
        valueEnd: 487
        refreshInterval: 100
        duration: 2500
        prefix: ''
        suffix: ''
        animation: bounceIn
      - name: Total Awarded
        text: Awarded in scholarships <br />Since 1984
        icon: user-graduate
        animation: bounceIn
        refreshInterval: 100
        duration: 2000
        prefix: $
        suffix: ''
        valueStart: 3000
        valueEnd: 3012500
  - _bookshop_name: text-blocks
    blocks:
      - heading: <span>What</span> we do.
        bottom_border: true
        text: >-
          Since its founding in 1984, ASF has provided a way for parents,
          community members and local businesses to invest in scholarships and
          quality education for students in Anacortes. We strive to provide
          exceptional opportunities district-wide that ignite learning, improve
          educational outcomes and prepare all students to become confident,
          competent and engaged citizens. ASF funds programs in all schools in
          the Anacortes School District, enriching the lives of over 2500
          students each year.
      - heading: <span>How</span> we do it.
        bottom_border: true
        text: >-
          The Anacortes Schools Foundation (ASF), works with the Anacortes
          School District, to help all students achieve the promise of their
          potential. ASF raises money through a variety of events and campaigns
          then uses those funds to support current programs including Early
          Learning, district-wide STEM Enrichment, Social Emotional and Mental
          Health, Summer Learning, Teacher Grants and the Visual and Performing
          Arts. ASF also has a long history of scholarship support and each
          year, through endowed and annual funding, gives over 100 awards and
          over $300,000 in scholarships to graduating Anacortes seniors and
          current college students.
      - heading: <span>Why</span> we do it.
        bottom_border: true
        text: >-
          We believe that education establishes a foundation for opportunity,
          growth and success in life. We raise private support so that our
          public schools have the ability to provide the highest quality
          education for ALL our students. We know that strong, healthy schools
          contribute to a strong and healthy community and we believe that every
          child in Anacortes has a right to the best education available
          anywhere.
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: ASF Video
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: video
            heading: Anacortes Schools Foundation
            youtubeID: kdGyO9yxNXM
            text: >-
              From <a name="Early-Learning" href="/programs">Preschool Early
              Learning</a> to <a name="Scholarships"
              href="/scholarships">College Scholarships</a>, we are committed to
              supporting the Anacortes School District.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
  - _bookshop_name: news
  - _bookshop_name: marquee
    width: 0
    message:
      heading: Thank You ASF Annual Sponsors!
      text: >-
        We are so grateful to our many community organizations that sponsor ASF
        events.
      url: /giving/sponsor/
    images:
      - name: Alaska Airlines
        url: https://www.alaskaair.com/
        image: https://asfmedia.imgix.net/2023-cts-alaska.jpg
      - name: Anacortes Kayak Tours
        url: https://www.anacorteskayaktours.com/
        image: https://asfmedia.imgix.net/Sponsors_akt.jpg
      - name: Anacortes Rotary
        url: https://anacortesrotary.org/
        image: https://asfmedia.imgix.net/rotary.png
      - name: Anthony's
        url: https://www.anthonys.com/restaurant/anthonys-at-cap-sante-marina/
        image: https://asfmedia.imgix.net/2021-cts-anthonys.png
      - name: Atterberry Gardens
        url: https://www.instagram.com/atterberrygardens/?hl=en
        image: https://asfmedia.imgix.net/2023-cts-atterberry.jpg
      - name: Brandywine Nursery
        url: https://brandywinenursery.com/
        image: https://asfmedia.imgix.net/2023-cts-brandywine.jpg
      - name: Burton Jewelers
        url: https://www.burtonjewelers.com/
        image: https://asfmedia.imgix.net/2022-cts-burton-r.jpg
      - name: Cap Sante Inn
        url: https://www.capsanteinn.net/
        image: https://asfmedia.imgix.net/cap-sante.png
      - name: Central Welding Supply
        url: https://www.centralwelding.com/
        image: https://asfmedia.imgix.net/1-5k-cws.jpg
      - name: Christ Episcopal Church
        url: https://www.christchurchanacortes.org/
        image: https://asfmedia.imgix.net/brand.gif
      - name: Christensen Fuels, Lubricants, Propane
        url: https://christensenusa.com/
        image: https://asfmedia.imgix.net/asf-logo-christensen.jpg
      - name: Dakota Creek Industries
        url: https://dakotacreek.com/
        image: https://asfmedia.imgix.net/dakota-creek-logo.jpg
      - name: Derek Damon Orthodontics
        url: http://www.derekdamonortho.com/
        image: https://asfmedia.imgix.net/2021-cts-damon.png
      - name: Domino's Pizza
        url: https://pizza.dominos.com/washington/anacortes/
        image: https://asfmedia.imgix.net/2023-cts-dominos.jpg
      - name: Edward Jones - Troy Kunz
        url: https://www.edwardjones.com/us-en/financial-advisor/troy-kunz
        image: https://asfmedia.imgix.net/2023-cts-edj2.jpg
      - name: Envirogreen Technologies
        url: https://envirogreentech.com
        image: https://asfmedia.imgix.net/copy-of-envirogreen.png
      - name: Fidalgo Creative
        url: https://fidalgocreative.com/
        image: https://asfmedia.imgix.net/Sponsors_fidalgo-creative.png
      - name: Fidalgo Island Rotary
        url: https://fidalgorotary.org/
        image: https://asfmedia.imgix.net/fir-stacked.png
      - name: Freedom Boat Club
        url: >-
          http://info.freedomboatclub.com/dp/sanjuanislands?utm_source=Google+My+Business&utm_medium=GMB+Extension&utm_campaign=GMB+Landing+Page&urlfeed1=What_Are_YOU_Waiting_For
        image: https://asfmedia.imgix.net/fbc-logo.png
      - name: Guemes Island Resort
        url: https://www.guemesislandresort.com/
        image: https://asfmedia.imgix.net/guemes-island-resort-blk-transparent.png
      - name: Haven Financial Partners
        url: >-
          https://www.raymondjames.com/havenfinancialpartners/about-us/anacortes-team
        image: https://asfmedia.imgix.net/2021-cts-haven.png
      - name: Heritage Bank
        url: https://www.heritagebanknw.com/home/home
        image: https://asfmedia.imgix.net/heritage-bank-logo.jpg
      - name: HF Sinclair
        url: https://www.hfsinclair.com/operations/facilities/us/anacortes-wa/
        image: https://asfmedia.imgix.net/2022-cts-sinclair.png
      - name: Island Adventures
        url: https://www.island-adventures.com/
        image: https://asfmedia.imgix.net/sponsor-cts-island.jpg
      - name: Johnny Picasso's
        url: http://johnnypicasso.com/
        image: https://asfmedia.imgix.net/2023-cts-picassos.jpg
      - name: Kiwanis Noon Kiwanis Club
        url: http://www.anacorteskiwanis.org/
        image: https://asfmedia.imgix.net/Sponsors_kiwanis-noon.jpg
      - name: Kiwanis Sunrisers
        url: https://www.facebook.com/AnacortesKiwanis/
        image: https://asfmedia.imgix.net/kiwanis-sunrisers.jpg
      - name: Majestic Inn and Spa
        url: https://www.majesticinnandspa.com/
        image: https://asfmedia.imgix.net/2023-cts-majestic.jpg
      - name: Marathon
        url: >-
          https://www.marathonpetroleum.com/Operations/Refining/Anacortes-Refinery/
        image: https://asfmedia.imgix.net/Sponsors_Marathon.jpg
      - name: Matrix Svc Inc
        url: https://www.matrixservice.com/
        image: https://asfmedia.imgix.net/matrix-services-inc.png
      - name: MP Environmental
        url: https://mpenviro.com
        image: https://asfmedia.imgix.net/Sponsors_mpe.jpg
      - name: MultiGen Wealth Services
        url: https://multigenws.com/
        image: https://asfmedia.imgix.net/2021-cts-multigen2.png
      - name: North Sound Oral and Facial Surgery
        url: https://www.northsoundoms.com/
        image: https://asfmedia.imgix.net/northsoundoralsurgery.png
      - name: OnPoint Industrial Services
        url: https://www.onpoint-us.com/
        image: https://asfmedia.imgix.net/1k-onpoint-color.jpg
      - name: Playhouse Dental
        url: https://www.playhousedentalkids.com/
        image: https://asfmedia.imgix.net/2021-cts-playhouse.png
      - name: PowerTek
        url: https://www.powertek.net/
        image: https://asfmedia.imgix.net/Sponsors_powertek.jpg
      - name: Proscapes
        url: https://www.usproscapes.com/
        image: https://asfmedia.imgix.net/2021-cts-proscapes.png
      - name: Samish Indian Nation
        url: https://www.samishtribe.nsn.us/
        image: https://asfmedia.imgix.net/Samish.jpg
      - name: Skagit Community Foundation
        url: https://www.skagitcf.org/
        image: https://asfmedia.imgix.net/Skagit-community-foundation.jpg
      - name: Sierra Pacific Industries
        url: https://www.spi-ind.com/
        image: https://asfmedia.imgix.net/sierra-pacific-industries.png
      - name: Strandberg
        url: https://strandbergconstruction.com/
        image: https://asfmedia.imgix.net/2023-cts-strandberg.jpg
      - name: Stronghold Companies
        url: https://www.thestrongholdcompanies.com/
        image: >-
          https://asfmedia.imgix.net/1.5K - Stronghold Companies Full Color
          Logo.jpg
      - name: Swinomish Casino
        url: https://www.swinomishcasinoandlodge.com/
        image: https://asfmedia.imgix.net/edit2-swinomishcasinolodge-logo.png
      - name: Synergy Sports (Sportradar)
        url: https://sportradar.com/
        image: https://asfmedia.imgix.net/2023-cts-sportradar.jpg
      - name: T Bailey Inc
        url: https://tbailey.com/
        image: https://asfmedia.imgix.net/sponsor-cts-bailey-r.jpg
      - name: The Port of Anacortes
        url: https://www.portofanacortes.com/
        image: https://asfmedia.imgix.net/poa-logo-clean.png
      - name: Transpac Marinas
        url: https://www.transpacmarinas.com/
        image: https://asfmedia.imgix.net/2021-cts-transpac.png
      - name: Trident
        url: https://www.tridentseafoods.com/
        image: https://asfmedia.imgix.net/2021-cts-trident.png
      - name: Veca
        url: https://www.veca.com
        image: https://asfmedia.imgix.net/22-veca-electric.png
      - name: Western Refinery
        url: https://wrsweb.com/
        image: https://asfmedia.imgix.net/22-wrs-logo-primary-1.png
      - name: Williams and Nulle CPA
        url: https://www.wncpa.com/
        image: https://asfmedia.imgix.net/williams-and-nulle-logo.jpg
      - name: Windermere
        url: https://anacortesrealestate.com/
        image: https://asfmedia.imgix.net/windermere.png
      - name: WR Grace
        url: https://grace.com/
        image: https://asfmedia.imgix.net/logo-grace.png
    speed: 20000
---
