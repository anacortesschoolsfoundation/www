---
title: Tom Stowe Memorial Scholarship
est: 2017
areas:
  - Memorial
description: >-
  The Stowe family honors his memory with this scholarship which is awarded to a
  student who graduated from Anacortes High School, and who will be entering
  their junior or senior year in an accredited college.
keywords: >-
  scholarship, kiwanis scholarship, kiwanis noon anacortes, anacortes noon
  kiwanis
featuredImage: https://asfmedia.imgix.net/donor-kiwanis-noon.jpg
donorImage: https://asfmedia.imgix.net/donor-kiwanis-noon.jpg
recipients:
  - name: Aynslee King
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Stowe - King,, Aynslee.jpeg
  - name: Emma Bakke
    year: 2022
    scholarshipImage:
  - name: Maggie Betts
    year: 2022
    scholarshipImage:
  - name: Gabriellia Erb
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/erb-gabriellia2.jpg
  - name: Jonathan Izquierdo-Salas
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/izquierdo-salas-jonathan.jpg
  - name: Kanane Haratani
    year: 2024
    scholarshipImage: ''
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              Tom Stowe was a lifelong member of Kiwanis and an advocate for
              higher education and/or continued education beyond high school as
              a foundation for life. He was a WWII veteran who highly valued
              God, country, family and personal responsibility and character.
              The Stowe family honors his memory with this scholarship which is
              awarded to a student who graduated from Anacortes High School, and
              who will be entering their junior or senior year in an accredited
              college.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
