---
title: Max and Janet Ramble Scholarship
est: 2019
areas:
  - General
description: >-
  The Max and Janet Ramble Scholarship was established by Max and Janet Ramble
  with the intent of assisting a motivated student who has set goals, and either
  needs or merits help to achieve them.
keywords: scholarship, Max Ramble, Janet Ramble
featuredImage:
donorImage:
recipients:
  - name: Annaly Ellis
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Ramble - Ellis, Annaly.jpg
  - name: Samantha Dziminowicz
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Max _ Janet Ramble - Dziminowicz, Samantha.jpg
  - name: Natalya Smith
    year: 2021
    scholarshipImage:
  - name: Grace Hill
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/hill-grace.jpg
  - name: Angelyna Sasso
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/sasso-angelyna.jpg
  - name: Sawyer Nichols
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Sawyer Nichols.jpg
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              The Ramble Scholarship recognizes a student who is a hard worker
              and is willing to persevere in seeking his or her goals. It is
              awarded to a student who is motivated, has set goals and either
              needs or merits help to achieve them. This scholarship is
              renewable for a second year.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
