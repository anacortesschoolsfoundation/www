---
title: Bill Wells Memorial Scholarship
est: 1996
areas:
  - Music
  - Education
  - Law
  - Memorial
featuredImage:
keywords: >-
  Anacortes School District, scholarship, scholarships, scholarship list,
  available scholarships, anacortes scholarships, Bill Wells
description: >-
  The Bill Wells Memorial Scholarship is awarded annually to a student with good
  character and a record of responsible citizenship that plans to major in law,
  music or education.
donorImage:
recipients:
  - name: Livy Gates
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Bill Wells Memorial - Gates, Livy.jpg
  - name: Ella Villines
    year: 2021
    scholarshipImage:
  - name: Dallas Moore
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Dallas Moore.jpeg
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              This scholarship is established in memory of Bill Wells. Bill
              Wells was born in Anacortes, the son of an attorney and a music
              teacher. He graduated from Anacortes High School and was a charter
              member of Key Club at AHS. Throughout his life, Bill had a deep
              love for music, a reverence for the power of education and for his
              law profession. The Bill Wells Memorial Scholarship is awarded
              annually to a student with good character and a record of
              responsible citizenship that plans to major in law, music or
              education.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
