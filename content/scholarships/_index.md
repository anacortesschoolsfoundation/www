---
title: Scholarships
aliases:
  - /scholarships/scholarships-66.html
  - /news/152-2017-scholarship-recipients.html
  - /scholarships/asf-scholarships
featuredImage: https://asfmedia.imgix.net/20230605_ASF_scholarship_awards-22.jpg
description: >-
  Anacortes Schools Foundation offers scholarships each year. Click here for
  more details.
keywords: >-
  anacortes schools foundation donation, Anacortes School District,
  scholarships, scholarship list, available scholarships, anacortes scholarships
layout: blocks
content_blocks:
  - _bookshop_name: hero
    title: Scholarships
    bannerImage: https://asfmedia.imgix.net/page-title-scholarships2023.jpg
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: Introduction
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: cta-large
            divclass:
            linkclass:
            text: >-
              Congratulations to the Class of 2024 and to all of this year's ASF
              Scholarship Awardees! 


              The password for our photo album is "awards". 
            action: View ASF Scholarship Ceremony Pictures Here
            url: >-
              https://ejharrisphotography.shootproof.com/gallery/scholarships/home
          - _bookshop_name: media-text
            heading:
            subheading:
            text: >-
              ASF Journalism Scholarship established by past ASF President Art
              Shotwell was awarded to Dashaun Coutee. Over the past 40 years,
              the Anacortes Schools Foundation has awarded over 1,000
              scholarships and over $3 million to Anacortes graduates.
            image_right: false
            images:
              - title: Scholarships
                src: >-
                  https://asfmedia.imgix.net/20230605_ASF_scholarship_awards-22.jpg
            video: {}
            url: https://asfmedia.imgix.net/2024+ASF+Scholarship+Recipient+Bios.pdf
            action: View the 2024 Scholarship Recipient Bios
          - _bookshop_name: content
            content: >-
              The following list includes all scholarships ASF typically awards.
              Due to market activity or donor preference from year to year,
              there may be scholarships that are  on this list but will not be
              offered in a given year.


              {{< button text="Browse All Scholarships" url="/scholarships/#all"
              >}}


              Thanks to the incredible generosity of families, businesses and
              organizations, each spring, ASF continues this tradition of
              support, awarding over $360,000 in scholarships each year. One
              common application makes students eligible for over 145
              scholarships. The ASF Scholarship Committee reviews the
              applications and the available scholarships and decides on the
              awards. IRS rules do not allow donors to select their scholarship
              recipients. Some scholarships may be selected based on a
              combination of criteria, such as:


              * Academic performance

              * Areas of interest

              * Financial need

              * Pursuing education in a specific field (such as STEM, the arts,
              international studies, etc.)

              * Student's demonstrated willingness and ability to help others

              * We prioritize graduates of the Anacortes School District and
              residents of Anacortes and Guemes Island.


              ### ASF multi-year scholarship


              If you are awarded a multi-year scholarship, this means that your
              scholarship may be considered for additional years provided you:


              A) Maintain good grades (some awards have specific GPA targets
              which must be supported with a transcript),


              B) You are still pursuing a major or area of study that fits with
              the scholarship criteria, and


              C) You request a renewal in March of each year by emailing us at
              [scholarships@asfkids.org](mailto:scholarships@asfkids.org).
              Requesting a renewal is your responsibility. Renewals and amounts
              are neither guaranteed nor automatic.


              {{< button text="Student Information Form" url="/student-info" >}}
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
  - _bookshop_name: asf-divider
  - _bookshop_name: scholarships-paginated
---
