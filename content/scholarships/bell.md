---
title: E.S. Red and Marilyn Bell Scholarship
aliases:
est: 2022
areas:
  - TECHNICAL OR VOCATIONAL
description: >-
  As longtime members of the Fidalgo Masonic Lodge and Eastern Star, Red and
  Marilyn Bell have supported scholarships for many years.
keywords: scholarship, Technical scholarship, vocational scholarship, Bell Scholarship
featuredImage: https://asfmedia.imgix.net/donor-bell.jpg
donorImage: https://asfmedia.imgix.net/donor-bell.jpg
recipients:
  - name: Moses Pittis
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Bell - Pittis, Moses.JPG
  - name: McKenzie Blouin
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Bell - Blouin, McKenzie.jpg
  - name: Mia Willoughby
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/D06F107E-2238-4579-AC2B-3A90CA2D8936.JPG
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              In 2022, they established this scholarship to assist a motivated
              student who has set goals and either needs or merits help to
              achieve them. Red and Marilyn have a particular interest in
              supporting students who are pursuing higher education in a trade
              or at a technical school. Red and Marilyn have lived in Anacortes
              for 61 years. All 4 of their kids - David, Daniel, Dustin and
              Nancy all graduated from Anacortes High School. Red spent 33 years
              working at the Puget Sound Refinery.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
