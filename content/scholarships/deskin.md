---
title: Deskin Scholarship
est: 2016
areas:
  - Engineering
  - Arts
  - General
description: >-
  The Deskin Scholarship seeks to encourage outstanding Anacortes High School
  graduates to continue their education through attendance at a college,
  university or trade school to better prepare them for an ever-changing world.
keywords: scholarship, Tracy Deskin, Tracy Deskin scholarship
featuredImage:
donorImage:
recipients:
  - name: Joey Keltner
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Deskin - Keltner, Joey-23.JPG
  - name: Reese Illston
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Deskin - Illston, Reese.jpg
  - name: Hamilton Hunt
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Deskin - Hunt, Hamilton.JPG
  - name: Emily Hackstadt
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Deskin - Hackstadt, Emily.JPG
  - name: Siobhan Gross
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Deskin - Gross, Siobhan.jpeg
  - name: Sarafina Dunlap
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Deskin - Dunlap, Sarafina.jpg
  - name: Riven Beckwith
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Deskin - Beckwith, Riven.jpeg
  - name: Skye Hopper
    year: 2022
    scholarshipImage:
  - name: Ryan Horr
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Deskin - Horr, Ryan.jpg
  - name: Lily Iverson
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Deskin - Iverson, Lily.JPG
  - name: Joey Keltner
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Deskin - Keltner, Joey.jpg
  - name: Georgia Stultz
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Deskin - Stultz, Georgia.JPG
  - name: Hazel Tanaka
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Deskin - Tanaka, Hazel.jpg
  - name: Mason Bakke
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/bakke-mason.jpg
  - name: Zachary Bowman
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/bowman-zachary.jpg
  - name: Gunner Hering
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/hering-gunner.jpg
  - name: Hayden Long
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/long-hayden.jpg
  - name: Isaac Paul
    year: 2021
    scholarshipImage:
  - name: Hannah Reilly
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/reilly-hannah.jpg
  - name: Lee Rudge
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/rudge-lee.jpg
  - name: Chloe Stroud
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/stroud-chloe.jpg
  - name: Alaura Swanson
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/swanson-alaura.jpg
  - name: Zachary Bowman
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/bowman-zachary.jpg
  - name: Gabriellia Erb
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/erb-gabriellia.jpg
  - name: Grace Evans
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/evans-grace-anne.jpg
  - name: Larissa Grieves
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/grieves-larissa.jpg
  - name: Geoffrey Hamilton
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/hamilton-geoffrey.jpg
  - name: Jenny Hanson
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/hanson-jenny.jpg
  - name: Michael Perron
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/perron-michael.jpg
  - name: Julia Anne Simpson
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/simpson-julia-anne.jpg
  - name: Zoe Yanega
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/yanega-zoe.jpg
  - name: Maksym Bondar
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/bondar-maksym.jpg
  - name: Brynna Bone
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/bone-brynna.jpg
  - name: Gabriellia Erb
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/erb-gabriellia3.jpg
  - name: Madeline Thomason
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/thompson-madeline.jpg
  - name: Ayden Swain
    year: 2024
    scholarshipImage: ''
  - name: Ethan Smith
    year: 2024
    scholarshipImage: ''
  - name: Shaylin Tveter
    year: 2024
    scholarshipImage: ''
  - name: Emilie Shjarback
    year: 2024
    scholarshipImage: ''
  - name: Emilie Cross
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Emilie Cross.jpg
  - name: Derek Betts
    year: 2024
    scholarshipImage: ''
  - name: Thurston Young
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Thurston Young.jpg
  - name: Kami Sellards
    year: 2024
    scholarshipImage: ''
  - name: Cody Cox
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Cody Cox.JPG
  - name: Miles King
    year: 2024
    scholarshipImage: ''
  - name: Landon Schafer
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/LandonSeniorPic.jpg
  - name: Anthony Zumpano
    year: 2024
    scholarshipImage: ''
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              It is established to honor Tracy O. Deskin. Mr. Deskin passed away
              in Anacortes, WA on November 9, 2014. Deskin served as a B-17
              bomber in the US Army Air Corps during WW II, flying over 25
              missions, and was honorably discharged on May 19, 1961. He met his
              future wife, Zillah, on a boat to Alaska when they were both
              relocating there. After leaving the service, Deskin continued to
              work as a civilian for the Army and Air Force until his retirement
              in 1983. Following retirement, the Deskins relocated to Anacortes.
              Tracy was a civil engineer and Zillah was an artist, so they
              wanted to establish scholarships to support students pursing
              further education in both the arts and engineering.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
