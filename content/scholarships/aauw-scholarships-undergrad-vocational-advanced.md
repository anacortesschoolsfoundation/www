---
title: >-
  AAUW Scholarships for Women Currently Pursing an Undergraduate Degree,
  Vocational Certification or Advanced Degree
est: 2022
areas:
  - General
featuredImage:
keywords: >-
  Anacortes School District, scholarship, scholarships, scholarship list,
  available scholarships, anacortes scholarships,
description: >-
  These scholarships are established by the American Association of University
  Women (AAUW).
donorImage:
recipients:
  - name: Carly Bates
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/AAUW - Bates, Carly.jpg
  - name: Cadence Lamphiear
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/AAUW - Lamphiear, Cadence.jpg
  - name: Ava Martin
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/AAUW - Martin, Ava.JPG
  - name: Mackenzie Wakefield
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/AAUW - Wakefield, Mackenzie.jpg
  - name: Emilie Shjarback
    year: 2022
    scholarshipImage:
  - name: Abigail Ball
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/AAUW Ball, A..jpg
  - name: Saville Feist
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/AAUW Feist, S..JPG
  - name: Joey Keltner
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/AAUW Keltner, J..jpg
  - name: Reese Vaux
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/AAUW Vaux, R..JPG
  - name: Olivia Schwartz
    year: 2024
    scholarshipImage: ''
  - name: Sarafina Dunlap
    year: 2024
    scholarshipImage: ''
  - name: Emme Mavar
    year: 2024
    scholarshipImage: ''
  - name: Macy Mong
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/AAUW Mong, M..jpg
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              AAUW empowers women and girls through research, education, and
              advocacy. The organization has more than 170,000 members and
              supporters across the United States, as well as 1,000 local
              branches and 800 college and university partners. The Anacortes
              Branch of the AAUW is offering scholarships to women who are in an
              undergraduate degree program, a vocational certification program,
              or planning to take courses beyond the undergraduate level in
              preparation for an advanced degree. Scholarships are not available
              to high school seniors or college freshmen.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
