---
title: Mark A. Edson Legacy Fund Trade School Scholarship
est: 2021
areas:
  - Technical or Vocational
description: >-
  The Mark A. Edson Legacy Fund Scholarship was established in 2020 by an
  anonymous donor to the Mark A. Edson Legacy Fund through the Seattle
  Foundation.
keywords: scholarship, Mark Edson scholarship, trade school scholarship
featuredImage:
donorImage:
recipients:
  - name: Kloee Borlin
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Edson - Borlin, Kloee.jpg
  - name: Matthew Rutz
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Edson - Rutz, Matthew.jpg
  - name: Ariana Bickley
    year: 2022
    scholarshipImage:
  - name: Oswaldo Bonilla
    year: 2022
    scholarshipImage:
  - name: McKenzie Blouin
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Mark Edson Technical - Blouin, McKenzie.jpg
  - name: Ellette Broyles
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Mark Edson Technical - Broyles, Ellette.jpg
  - name: Kevin McClellan
    year: 2021
    scholarshipImage:
  - name: Lillyan Randall
    year: 2021
    scholarshipImage:
  - name: Joshua Smeltzer
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/smeltzer-joshua.jpg
  - name: Jonah Umayam
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/umayam-jonah.jpg
  - name: Andrew O'Donnell
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Andrew+ODonnell.jpg
  - name: James Friedrichs
    year: 2024
    scholarshipImage: ''
  - name: Arturo Varela
    year: 2024
    scholarshipImage: ''
  - name: Karma Ray
    year: 2024
    scholarshipImage: ''
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              The Mark A. Edson Legacy Fund Trade School Scholarships are
              awarded to students who plan to go on to trade or technical
              school.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
