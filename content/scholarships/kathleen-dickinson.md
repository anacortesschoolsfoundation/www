---
title: Kathleen Dickinson Scholarship
est: 2020
areas:
  - General
description: This scholarship is established by Kathleen Dickinson to assist a motivated, goal-driven student, who either needs or merits help to achieve their goals.
keywords: scholarship, Kathleen Dickinson, STEM scholarship
featuredImage:
donorImage:
recipients:
  - name: Victoria Frost
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/frost-victoria.jpg
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship

            content: |
              Student must have a GPA of 3.5 or higher and plan to pursue a 4-year degree in a STEM area of study.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
