---
title: Kiwanis Anacortes Noon Club Foundation Scholarship
est: 2019
areas:
  - General
description: >-
  Kiwanis is a world-wide organization whose focus is "Serving the Children of
  the World." Kiwanis raises funds for scholarships and other community projects
  through a variety of fundraising efforts including the Kiwanis Thrift Store.
keywords: scholarships, Kiwanis Noon scholarship, Kiwanis Anacortes Noon Club
featuredImage: https://asfmedia.imgix.net/donor-kiwanis-noon.jpg
donorImage: https://asfmedia.imgix.net/donor-kiwanis-noon.jpg
recipients:
  - name: Sam Davis
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/KN - Davis, Sam.JPG
  - name: Parker King
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/KN - King, Parker.jpg
  - name: Macy Koegel
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/KN - Koegel, Macy.jpeg
  - name: Makishi Mott
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/KN - Mott, Makishi.jpg
  - name: Fletcher Olson
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/KN - Olson, Fletcher.jpg
  - name: Madelyn Shelling
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/KN - Shelling, Madelyn.jpg
  - name: Tyler Blouin
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/KN (Beeler) - Blouin, Tyler.JPG
  - name: McKernan Boland
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/KN (Key Club) - Boland, McKernan.jpg
  - name: Abbie Goodwin
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/KN (Key Club) - Goodwin, Abbie.jpg
  - name: Patrick Quinn
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/KN (Norton) - Quinn, Patrick.jpg
  - name: Olivia Schwartz
    year: 2022
    scholarshipImage:
  - name: Reese Vaux
    year: 2022
    scholarshipImage:
  - name: Alicia Ambriz-Espino
    year: 2022
    scholarshipImage: >-
      https://asfmedia.imgix.net/Kiwanis Noon (Johnson) - Ambriz-Espino,
      Alicia.jpg
  - name: Megan Carroll
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Kiwanis Noon (Norton) - Carroll, Megan.jpg
  - name: William Waldrop
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Kiwanis Noon (Wright) - Waldrop, William.jpg
  - name: Zachary Bowman
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Kiwanis Noon - Bowman, Zachary.jpg
  - name: Elizabeth Braatz
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Kiwanis Noon - Braatz, Elizabeth.jpg
  - name: Alyssa Digweed
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Kiwanis Noon - Digweed, Alyssa.jpg
  - name: Aynslee King
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Kiwanis Noon - King, Aynslee.JPG
  - name: Sarah Quinn
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Kiwanis Noon - Quinn, Sarah.jpg
  - name: Kaitlyn Sage
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Kiwanis Noon - Sage, Kaitlyn.JPG
  - name: Lindsey South
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Kiwanis Noon - South, Lindsey.JPG
  - name: Hannah Cross
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/cross-hannah.jpg
  - name: Madison DeBruler
    year: 2021
    scholarshipImage:
  - name: Joshua Digweed
    year: 2021
    scholarshipImage:
  - name: Vince Jackson
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/jackson-vincent.jpg
  - name: Matthew Lowrie
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/lowrie-matthew.jpg
  - name: Ava Martin
    year: 2021
    scholarshipImage:
  - name: Jaida Mason
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/mason-jaida.jpg
  - name: Liam Patrick
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/patrick-liam.jpg
  - name: Kaitlyn Sage
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/sage-kaitlyn.jpg
  - name: Eliza Senff
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/senff-eliza.jpg
  - name: Gabriella Zumpano
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/zumpano-gabriella.jpg
  - name: Jefferson Butler
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/butler-jefferson.jpg
  - name: Brandon Cross
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/cross-brandon.jpg
  - name: Hannah Cross
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/cross-hannah.jpg
  - name: Grayson Eaton
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/eaton-grayson.jpg
  - name: Kaeden Flynn
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/flynn-kaedan.jpg
  - name: Samuel Hardesty
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/hardesty-samuel.jpg
  - name: Vincent Jackson
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/jackson-vincent.jpg
  - name: Clare Martin
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/martin-clare.jpg
  - name: Liam Patrick
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/patrick-liam.jpg
  - name: Sarah Quinn
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/quinn-sarah.jpg
  - name: Anja Shjarback
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/shjarback-anja.jpg
  - name: Baylee South
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/south-baylee.jpg
  - name: Cabry Biddle
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/biddle-cabry.jpg
  - name: Zack Bowman
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/bowman-zack.jpg
  - name: Nicole Bunzel
    year: 2019
    scholarshipImage:
  - name: Brandon Cross
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/cross-brandon.jpg
  - name: Hannah Cross
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/cross-hannah.jpg
  - name: Kara Foster
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/foster-kara.jpg
  - name: Christoph Franssen
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/franssen-christoph.jpg
  - name: Skye Hopper
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/hopper-skye.jpg
  - name: Jonathan Izquierdo-Salas
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/izquierdo-salas-jonathan.jpg
  - name: Sadie Leavitt
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/leavitt-sadie.jpg
  - name: Ava Martin
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/martin-ava.jpg
  - name: Will McKracken
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/mckracken-will.jpg
  - name: Liam Patrick
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/patrick-liam.jpg
  - name: Conor Powell
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/powell-conor.jpg
  - name: Evan Rodriguez
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/rodriguez-evan.jpg
  - name: Riley Ward
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/ward-riley.jpg
  - name: Christopher Warmuth
    year: 2019
    scholarshipImage:
  - name: Clare Martin
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/martin-clare-2.jpg
  - name: Lindsey South
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/south-lindsey.jpg
  - name: Kaitlin Banta
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/banta-kaitlin.jpg
  - name: Alexander Carroll
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/carroll-alexander.jpg
  - name: Katelyn Hellman
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/hellman-katelyn.jpg
  - name: Alex Knudtson
    year: 2019
    scholarshipImage:
  - name: Cooper Nichols
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/nichols-cooper.jpg
  - name: Jake Romag
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/romag-jake.jpg
  - name: Gabriellia Erb
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/erb-gabriellia.jpg
  - name: Jonathan Izquierdo-Salas
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/izquierdo-salas-jonathan.jpg
  - name: Macy Mong
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/AAUW+Mong%2C+M..jpg
  - name: Emme Mavar
    year: 2024
    scholarshipImage: ''
  - name: Lily Prue
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/IMG_2743.jpg
  - name: Reagan Lee
    year: 2024
    scholarshipImage: ''
  - name: Samuel Williams
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Samuel+Williams.jpg
  - name: Mattias Thornton
    year: 2024
    scholarshipImage: ''
  - name: Gibson Groenig
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Gibson+Groenig.jpg
  - name: Kyler Martindale
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Kyler Martindale.JPG
  - name: Kaleb Lamphiear
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Kaleb Lamphiear.jpeg
  - name: Zinnia Jackson
    year: 2024
    scholarshipImage: ''
  - name: Emily Hackstadt
    year: 2024
    scholarshipImage: ''
  - name: Vincent Jackson
    year: 2024
    scholarshipImage: ''
  - name: Thomas Harris
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Thomas Harris.jpeg
  - name: Payton (Moss) Caplinger
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Moss Caplinger.jpg
  - name: Jakob Smith
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Jakob Smith.jpg
  - name: Kaeden Flynn
    year: 2024
    scholarshipImage: ''
  - name: Gabriella Zumpano
    year: 2024
    scholarshipImage: ''
  - name: Layne Prue
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Layne Prue.jpeg
  - name: Tyler Blouin
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Blouin, Tyler LSU Photo.jpg
  - name: Wolfgang Strohschein
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/wolfgang.jpeg
  - name: Hamilton Hunt
    year: 2024
    scholarshipImage: ''
  - name: Jameson Pesaturo
    year: 2024
    scholarshipImage: ''
  - name: Madelyn Schelling
    year: 2024
    scholarshipImage: ''
  - name: Emily Cox
    year: 2024
    scholarshipImage: ''
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              The Kiwanis Anacortes Noon Club Foundation facilitates a number of
              scholarships each year. Kiwanis is a world-wide organization whose
              focus is "Serving the Children of the World." Kiwanis raises funds
              for scholarships and other community projects through a variety of
              fundraising efforts including the Kiwanis Thrift Store. The amount
              and number of scholarships varies annually. A designated amount
              may support students going on to 4-year universities, a designated
              amount may support current college students and a designated
              amount may also support technical, vocational or trade school
              bound students.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
