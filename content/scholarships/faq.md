---
title: Frequently Asked Questions
aliases:
  - /scholarships/faqs.html
bannerImage: https://asfmedia.imgix.net/page-title-scholarships.jpg
noSearch: false
layout: blocks
content_blocks:
  - _bookshop_name: hero
    title: Frequently Asked Questions
    bannerImage: https://asfmedia.imgix.net/page-title-scholarships.jpg
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: FAQ
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: faq
            title: ASF Giving FAQ
            text: >-
              If you don't find the answer you're looking for, please contact us
              at scholarships@asfkids.org
            faqs:
              - q: >-
                  I've been awarded an ASF scholarship. How does the money get
                  to my school?
                a: >-
                  Scholarship recipients must complete the ASF Student
                  Information Form by June 30 for funds to be mailed to their
                  designated college or trade school. [Click
                  HERE](/scholarships/student-info) to complete the Student
                  Information Form.
              - q: >-
                  Where do I indicate the specific scholarship for which I am
                  applying?
                a: >-
                  You don't! Your general Anacortes Schools Foundation
                  scholarship application will be used to consider you for most
                  ASF scholarships. If you are interested in, and eligible for,
                  the Greg Horak Memorial Scholarship, you should submit a
                  separate application for this award.
              - q: >-
                  I am a current college student. What do I do about SAT/ACT
                  scores?
                a: >-
                  SAT/ACT scores are optional this year. AHS Seniors and current
                  college students are NOT required to submit any test
                  information.
              - q: Where do I find the FAFSA Student Aid Report (SAR)?
                a: >-
                  The SAR is the report that you should see when you log into
                  your FAFSA account and click on “View Processed Information
                  (PDF version).” You will see a box with Application Date,
                  Process Date, SAI, DRN, etc near the top of the report. The
                  committee only needs page 1 of this report which is most
                  easily viewed and printed in the PDF version.
              - q: >-
                  I am a current college student at a school using the quarter
                  system. The application says I should submit my 1st semester
                  grades. What transcripts should I submit?
                a: >-
                  You should submit all transcripts that are available by the
                  April 26 (NOON), 2025 deadline. 
              - q: I don't have an SAI from FAFSA. What should I do?
                a: >-
                  If you do not have a FAFSA SAI for any reason, please explain
                  your financial need and your family's financial circumstances
                  in the supporting information statement on the application.
              - q: >-
                  Should I include required fees along with the tuition amount
                  in my application?
                a: >-
                  Please ONLY include the cost of tuition and required fees as
                  stated on the university or programs website. Do not include
                  room and board, transportation or optional fees.
              - q: How long should my answers to personal questions be?
                a: >-
                  There is word no limit. The selection committee will read your
                  full answers regardless of length.
              - q: Do I have to include a supporting information statement?
                a: >-
                  No, as indicated on the application, that is a completely
                  optional document. If you wish to submit such a statement, you
                  may use the space to explain any extenuating financial or
                  family circumstances.
              - q: Can I submit my application materials in a binder or folder?
                a: >-
                  No. Applications can only be submitted digitally. If you have
                  any questions or have trouble with the online application,
                  please email scholarships@asfkids.org.
              - q: How will I know if I am selected to receive a scholarship?
                a: >-
                  All students will receive notification of the Anacortes
                  Schools Foundation final scholarship decisions the week of May
                  30. Please note, all scholarship selections are made by the
                  Anacortes Schools Foundation Scholarship Committee. The AHS
                  counsellors are not ASF board members. Please do not contact
                  them for information regarding selection decisions.
              - q: Can I turn in my application late?
                a: >-
                  Unfortunately, we are only able to accept on-time and complete
                  applications for consideration.
              - q: I received an ASF scholarship. How can I thank the donors?
                a: >-
                  Addresses for scholarship donors are provided to recipients at
                  the annual awards ceremony which this year will take place on
                  June 5, 2025. In most cases, contact information is located on
                  the back of the award certificate you received. If you lost
                  your address, please email us.
              - q: >-
                  How does the ASF keep personal and financial information
                  secure?
                a: >-
                  All ASF Scholarship Committee Members sign a confidentiality
                  agreement. Once applications are reviewed, personal and
                  financial information is shredded or destroyed.
      - name: Important Dates
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
