---
title: Richard and Marjorie Waldron Memorial Scholarship
est: 1990
areas:
  - Memorial
description: >-
  The Richard and Marjorie Waldron Memorial Scholarship is awarded to a
  graduating senior who plans to attend college, vocational or university in
  Washington state.
keywords: scholarship, Richard Waldron, Marjorie Waldron
featuredImage:
donorImage:
recipients:
  - name: Staely Moore
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Waldron - Moore, Staely.jpg
  - name: Carolina Schwetz
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/schwetz-carolina.jpg
  - name: Claudia Andrade
    year: 2024
    scholarshipImage: ''
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              The Richard and Marjorie Waldron Memorial Scholarship is awarded
              to students who show strength of character and responsible
              citizenship. It is named for Richard A. Waldron, who was a
              graduate of Central Washington University with a degree in
              Education and of the University of Washington in Chemistry. He
              taught for nine years in junior and senior high schools and
              afterwards worked for 20 years in research for Weyerhaeuser Timber
              Company. In 1965, he moved his family to Anacortes to work as a
              Sales Manager for Northwest Petro Chemical Company. After his
              retirement (at age 65), he developed a product from alder bark
              that acted as a glue extender for plywood glues. He set up his own
              plant and company near Marysville to manufacture this product.
              Richard Waldron was an active community member during his 20 years
              of living in Anacortes. Some of the organizations he supported and
              belonged to were Boy Scouts, Red Cross, Board of Adjustments, City
              Planning Committee, Rotary and the Methodist Church. He always
              felt Anacortes was a special place to live and he enjoyed being a
              part of the community. His Wife Margie oversaw this scholarship
              until her death in 2009. Marjorie's name was subsequently added to
              the award. Marjorie Waldron, a longtime Anacortes resident, passed
              away in January 2010. Born in 1915, Marjorie was orphaned at an
              early age. In spite of her childhood hardships, she graduated from
              Washington State Normal School (now Western Washington University)
              in 1936. Marjorie's life was marked by a devotion to children. She
              taught grade school in Longview, Washington prior to her marriage
              to Richard Waldron in 1941. In addition to being a devoted mother,
              she was an active member of Chapter W of the Philanthropic
              Education Organization, the Dr. Gordon Brooks Children's Hospital
              Auxiliary, and a lifetime member of the PTA. Marjorie continued to
              be a great example to her friends and family, even in her senior
              years. One of her last achievements in a long and amazing life was
              climbing Sauk Mountain at the age of 90. Mrs. Waldron would be
              pleased to know that her commitment to education and community
              continues with the Richard and Marjorie Waldron Scholarships.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
