---
title: Jennifer Bowman Arts Scholarship
est: 2023
areas:
  - arts
keywords: Jennifer Bowman, art scholarship, scholarships, scholarship list, available scholarships, anacortes scholarships,
description: The Anacortes Rotary is pleased to offer a scholarship honoring award-winning Puget Sound artist Jennifer Bowman.
featuredImage: https://asfmedia.imgix.net/donor-bowman.jpg
donorImage: https://asfmedia.imgix.net/donor-bowman.jpg
recipients:
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship

            content: |
              Jennifer has exhibited professionally for 30 years. Her works appear in private, public, and corporate collections, nationally and internationally, and in various publications: Washington's Heirloom Birth Certificate, North-light Art books and 48 Degrees North. She has been festival poster artist for Edmonds, Anacortes, Skagit Tulip Festival 2004 and 2020, Whidbey Island Races and Anacortes Windermere Regatta.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
