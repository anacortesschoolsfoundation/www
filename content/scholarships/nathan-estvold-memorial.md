---
title: Nathan Estvold Memorial Scholarship
est: 2004
areas:
  - Community Service
  - Sports
  - Memorial
description: This scholarship is awarded to a student headed for trade school, community college or a 4-year school who embodies Nathan's qualities of citizenship, work ethic and interest in athletics.
keywords: scholarship, Nathan Estvold, Nathan Estvold Memorial Scholarship, Estvold Memorial, Estvold scholarship
featuredImage:
donorImage:
recipients:
  - name: Bridger Wakley
    year: 2022
    scholarshipImage:
  - name: Alizee Hargrove
    year: 2021
    scholarshipImage:
  - name: Jacob Erickson
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/erickson-jacob.jpg
  - name: Nathan Johnson
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/johnson-nathan.jpg
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship

            content: |
              Nathan Michael Estvold loved athletics, the outdoors, tools, gadgets, cars and friends. Nathan started in sports at the age of five with T-ball, soccer and Parks and Rec. basketball; he was a three sport athlete and excelled at what he did. He was on select basketball throughout elementary and middle school, medaled in the shot-put and disc in middle school and lettered in football and wrestling his sophomore year at Anacortes High School. Nathan loved to snow ski, starting at the age of four and became an expert, with no trouble mastering double black diamond runs by the age of 10. Although his preference was to ski, he was very proficient at snowboarding as well. Nathan was very ambitious. He earned his first job at Anacortes Yacht Charters the summer of his freshman year, starting on the cleaning crew and working his way up to Dock Boy. With the money he earned, he bought a used truck that made him proud. Nathan also loved to mountain bike, camp and fish. One of his last fishing trips took him to the north side of Vancouver Island fishing for salmon. Nathan was also very good with his hands, building a winning trebuchet for his history class completion. Of all the things Nathan loved to do, the thing he loved the most was having fun and hanging out with his friends. As a sophomore, Nathan was not sure of his course after high school, but considered both trade school and 4-year university options that would allow him to pursue his interests in the outdoors or doing something with his hands. Sadly, Nathan Estvold died on November 23, 2003 at the age of 16 after a courageous battle with cancer. This scholarship is awarded to a student headed for trade school, community college or a 4-year school who embodies Nathan's qualities of citizenship, work ethic and interest in athletics.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
