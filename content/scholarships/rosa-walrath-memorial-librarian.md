---
title: Rosa Walrath Memorial Librarian Scholarship
est: 2004
areas:
  - Memorial
  - Librarian
description: The Rosa Walrath Memorial Librarian Scholarship is awarded to students who plan to become a librarian.
keywords: scholarship, library scholarship, Rosa Walrath
featuredImage: https://asfmedia.imgix.net/donor-walrath-2.jpg
donorImage: https://asfmedia.imgix.net/donor-walrath-2.jpg
recipients:
  - name: Willow McLaughlin
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Walrath - McLaughlin, Willow.jpg
  - name: Patricia Wilson
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/wilson-trish.jpg
  - name: Jaime Zullo
    year: 2019
    scholarshipImage:
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship

            content: |
              Rosa Walrath died on August 9, 2003 bequeathing the Rosa Walrath Memorial Librarian Scholarship to be used for students who plan to become a librarian. Rosa was born on Orcas Island on January 29, 1914 and graduated from Orcas Island High School in 1931 and from Bellingham Normal School (now Western Washington University) in 1934. She later attended the University of Hawaii, Central Washington University and UC San Diego. Rosa began her teaching career in 1934 with five years on Orcas Island. After WWII she taught at the Whitney School from 1950-1952. She was then offered a position in Lihue, Hawaii, on the island of Kauai, where she taught elementary school until 1955. In the fall of that year, Rosa returned to Anacortes where she remained for the rest of her career as both a teacher and Librarian for the Anacortes School District. She retired in 1972.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
