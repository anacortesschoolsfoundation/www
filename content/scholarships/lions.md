---
title: Lions Club Scholarships
est: 2023
areas:
  - General
keywords: >-
  Anacortes Lions Club, scholarship, scholarships, scholarship list, available
  scholarships, anacortes scholarships,
description: >-
  The Anacortes Lions Club is proud to offer 2 scholarships to graduating
  seniors whose focus is on the betterment of society.
featuredImage: https://asfmedia.imgix.net/donor-Lions.jpg
donorImage: https://asfmedia.imgix.net/donor-Lions.jpg
recipients:
  - name: Kellen Murphy
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Lion_s - Murphy, Kellen.JPG
  - name: Camryn Kerr
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Lion_s - Kerr, Camryn.JPG
  - name: Anthony Zumpano
    year: 2024
    scholarshipImage: ''
  - name: Daniel Zeller
    year: 2024
    scholarshipImage: ''
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              The Anacortes Lions Club is part of Lions Clubs International, the
              world's largest service club with more than 1.4 million members in
              approximately 46,000 clubs in more than 200 countries and
              geographical areas around the world. The Lions Club is responsible
              for displaying flags on Commercial for 8 holidays a year, they
              support vision and hearing screenings for school children, sponsor
              local events including the Causland Park Easter Egg Hunt, the
              annual Anacortes Holiday Parade, and more. The Anacortes Lions
              Club is proud to offer 2 scholarships to graduating seniors whose
              focus is on the betterment of society.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
