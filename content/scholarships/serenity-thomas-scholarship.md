---
title: Serenity Thomas Scholarship
est: 2024
areas:
  - General
  - Memorial
  - Education
  - Health and Human Services
description: >-
  This scholarship is awarded to a student who is planning to pursue a career in
  special needs education, nursing, special services or pediatric
  cancer/research.
keywords: scholarship, serenity thomas, memorial
featuredImage: https://asfmedia.imgix.net/Serenity.jpg
donorImage: https://asfmedia.imgix.net/Serenity.jpg
recipients:
  - name: Jenny Hanson
    year: 2024
    scholarshipImage:
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >-
              This scholarship is established by her parents in memory of an AHS
              student Serenity Thomas, who died in 2015 at age 16 from acute
              leukemia. She was truly a Disney girl who believed in happily ever
              after, adored wearing hot pink outfits and dressing as a Princess.
              Serenity loved life, music and singing, and her favorite classes
              were Choir, Drama and PE. She greeted everyone with a ready smile,
              never met a stranger and comforted many young people with her
              positive attitude and sense of mischievous humor. Born with Down
              Syndrome, she consistently exceeded expectations and proved that
              different did not mean less.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
