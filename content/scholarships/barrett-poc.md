---
title: Barrett Financial Student of Color Scholarship
est: 2023
areas:
  - General
keywords: >-
  Barrett Financial, POC, diversity, scholarship, scholarships, scholarship
  list, available scholarships, anacortes scholarships,
description: >-
  Barrett Financial offers an annual scholarship to an Anacortes High School
  graduating senior who identifies as a Person of Color and who plans to attend
  a 4-year college. This award focuses on high-achievers that combine stellar
  academic performance with extracurricular success.
featuredImage: https://asfmedia.imgix.net/donor-Barrett.jpg
donorImage: https://asfmedia.imgix.net/donor-Barrett.jpg
recipients:
  - name: Madison Wakefield
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/POC - Wakefield, Madison.JPG
  - name: Hyojeong Jeong
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Jeong H.jpg
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              This $5000 scholarship is established by Barrett Financial.
              Barrett Financial is a wealth management firm celebrating 30+
              years of business in Anacortes, WA.  Barrett is committed to
              giving back to the Anacortes community and believes that we can
              work together to inspire a path to a better future. Barrett also
              believes that access to higher education is an important measure
              of progress and proudly hopes to provide a leg-up towards
              increased educational mobility.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
