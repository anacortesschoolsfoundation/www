---
title: Education Trust Scholarship
est: 2024
areas:
  - General
  - Health and Human Services
description: >-
  This scholarship is intended to help provide scholarships to students pursuing
  a nursing career.
keywords: scholarship, nursing
featuredImage: https://asfmedia.imgix.net/nursing scholarship.jpg
donorImage: https://asfmedia.imgix.net/nursing+scholarship.jpg
recipients:
  - name: Macy Mong
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/AAUW+Mong%2C+M..jpg
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >-
              The Education Trust Scholarship Fund was created by a generous
              gift from an anonymous donor.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
