---
title: Richard Bliss White Memorial Scholarship
est: 1985
areas:
  - Memorial
description: >-
  The Richard Bliss White Memorial Scholarship is awarded to a student who is of
  good character and shows evidence of responsible citizenship.
keywords: scholarship, Richard Bliss White, Richard White scholarship
featuredImage:
donorImage:
recipients:
  - name: Tori Anthony
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/White - Anthony, Tori.jpg
  - name: Kellie Burch
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Richard Bliss White - Burch, Kellie.jpg
  - name: Mackenzie Wakefield
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/wakefield-mackenzie.jpg
  - name: Adam Acuff
    year: 2024
    scholarshipImage: ''
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              The scholarship may be awarded to graduating senior from the
              Anacortes High School to enable them to commence their education
              at a college, university or vocational school, or may be awarded
              to students who have graduated in previous years from Anacortes
              High School, to enable them to continue their undergraduate
              studies.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
