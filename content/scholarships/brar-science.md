---
title: Brar Science Scholarship
est: 2011
areas:
  - STEM
  - Health and Human Services
keywords: >-
  Anacortes School District, scholarship, scholarships, scholarship list,
  available scholarships, anacortes scholarships, Brar science scholarship
description: >-
  The Brar Science Scholarship is awarded to a graduating Anacortes High School
  student with a record of exemplary academic merit to pursue his or her dream
  of a science or healthcare profession.
featuredImage:
donorImage:
recipients:
  - name: Morgan Berard
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Brar - Berard, Morgan.JPG
  - name: Noah Hunter
    year: 2022
    scholarshipImage:
  - name: Ashleigh Merrill
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/merrill-ashleigh.jpg
  - name: Sophie Riley
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/riley-sophie.jpg
  - name: Natalie Dale
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/dale-natalie.jpg
  - name: Christopher Jolly
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Christopher Jolly.JPG
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              The Brar family believes strongly in the importance of supporting
              and promoting the educational dreams of Anacortes students so that
              they have the chance to develop a rewarding career. The Brar
              Science Scholarship is awarded to a graduating Anacortes High
              School student with a record of exemplary academic merit to pursue
              his or her dream of a science or healthcare profession.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
