---
title: Greg and Sue Monaghan Cap Sante Scholarship
est: 2018
areas:
  - General
description: Greg and Sue Monaghan established the Greg and Sue Monaghan Cap Sante Scholarship because they believe that few things in life can have a greater influence on a young person's journey, than the gift of a formal education or advanced training in a trade.
keywords: scholarship, Greg Monaghan scholarship, Sue Monaghan scholarship
featuredImage: https://asfmedia.imgix.net/donor-monaghan.jpg
donorImage: https://asfmedia.imgix.net/donor-monaghan.jpg
recipients:
  - name: Sharyn Richmond
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/richmond-sharyn.jpg
  - name: Astrid-Sophie Kainz
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/kainz-astrid-sophie.jpg
  - name: Lurixy Cerrillo
    year: 2019
    scholarshipImage:
  - name: Kylee Minter
    year: 2019
    scholarshipImage:
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship

            content: |
              Their sincere gift allows the "next generation" to pursue their goals and dreams in life and empowers them to connect inspiration and perspiration. With this donation, Greg and Sue Monagha would fervently like to help a student at Cap Sante High School realize that ambition.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
