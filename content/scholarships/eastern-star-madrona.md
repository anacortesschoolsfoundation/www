---
title: Eastern Star Madrona Chapter 89 Vocational Technical Scholarship
est: 2020
areas:
  - Technical or Vocational
description: >-
  The Madrona Chapter 89, Order of the Eastern Star established this Vocational/
  Technical Scholarship to assist students in attaining their goals in a
  vocational or technical field of study.
keywords: >-
  scholarship, Eastern Star scholarship, Madrona Eastern Star scholarship,
  technical scholarship, vocational scholarship
featuredImage: https://asfmedia.imgix.net/donor-eastern-star.jpg
donorImage: https://asfmedia.imgix.net/donor-eastern-star.jpg
recipients:
  - name: Moses Pittis
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Eastern Star - Pittis, Moses.JPG
  - name: Ellette Broyles
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Eastern Star - Broyles, Ellette.jpg
  - name: Alaina Schafer
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Eastern Star - Schafer, Alaina.jpg
  - name: Kevin McClellan
    year: 2021
    scholarshipImage:
  - name: Lillyan Randall
    year: 2021
    scholarshipImage:
  - name: Faith Martinez
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/martinez-faith.jpg
  - name: Amber Rogers
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Amber+Rogers.JPG
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: |
              This award is for the chosen field of study of the recipient.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
