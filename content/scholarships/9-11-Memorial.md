---
title: 9/11 Memorial Scholarship
est: 2022
areas:
  - General
  - Memorial
keywords: >-
  Anacortes School District, scholarship, scholarships, scholarship list,
  available scholarships, anacortes scholarships,
description: >-
  This scholarship is established by Jack and Josette Curtis to honor families
  of first responders.
featuredImage: https://asfmedia.imgix.net/scholarship-curtis.jpg
donorImage: https://asfmedia.imgix.net/scholarship-curtis.jpg
recipients:
  - name: Erik Dotzauer
    year: 2022
    scholarshipImage:
  - name: Wesley Hunter
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Wesley Hunter.jpg
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              Through a combined 40 years of service to the country, Jack and
              Josette developed a strong appreciation for all who serve their
              communities, not just those who choose to serve in the military. 
              This scholarship recognizes the service and sacrifices made by
              families of first responders. The recipient's parent, step-parent,
              or legal guardian must be a first responder to include:
              firefighters (full-time and volunteer), police, emergency medical
              technicians, and 911 dispatchers.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
