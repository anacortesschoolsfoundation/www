---
title: Skagit Valley Warriors Female Athlete Scholarship
est: 2023
areas:
  - Sports
keywords: >-
  Skagit Valley Warriors scholarhip, womens football scholarship Anacortes
  School District, scholarship, scholarships, scholarship list, available
  scholarships, anacortes scholarships,
description: >-
  The Skagit Valley Warriors charity women's football team raises money for
  local youth in our community. The Skagit Valley Warriors want to encourage
  women, empower women, and support female athletes in our community.
featuredImage: https://asfmedia.imgix.net/donor-svwarriors.jpg
donorImage: https://asfmedia.imgix.net/donor-svwarriors.jpg
recipients:
  - name: Erin Kennedy
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Warriors - Kennedy, Erin.jpeg
  - name: Niusha Ahsaei
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Niusha A.jpg
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              They are offering a one year scholarship to an Anacortes High
              School graduating senior Female Athlete (including cheer and
              dance) who participated in a sport for at least 2 years during
              high school.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
