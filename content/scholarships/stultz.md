---
title: Stultz Family Continuing College Scholarship
est: 2023
areas:
  - General
description: The Stultz Family believes in supporting the dreams of Anacortes graduates and establishes this scholarship to celebrate the accomplishments college student.
keywords: scholarship, Stultz scholarship
featuredImage: https://asfmedia.imgix.net/donor-Stultz.jpg
donorImage: https://asfmedia.imgix.net/donor-Stultz.jpg
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship

            content: |
              This award goes to a rising junior or senior in college.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
