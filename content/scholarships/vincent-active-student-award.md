---
title: Vincent Active Student Award
est: 2018
areas:
  - Memorial
description: >-
  The Vincent Active Student Award seeks to encourage graduates of Anacortes
  High School to attend college and pursue their hopes and dreams.
keywords: scholarship, Vincent Active
featuredImage: https://asfmedia.imgix.net/donor-vincent.jpg
donorImage: https://asfmedia.imgix.net/donor-vincent.jpg
recipients:
  - name: Emma Foley
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Vincent - Foley, Emma.jpeg
  - name: Braden Thomas
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Vincent - Thomas, Braden.jpg
  - name: Cassius Tossavainen
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Vincent - Tossavainen, Cassius.jpg
  - name: Skyler Whisler
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Vincent - Whisler, Skyler.jpg
  - name: Joseph Cutter
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/cutter-joseph.jpg
  - name: Lauren McClintock
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/mcclintock-lauren.jpg
  - name: Chase Cornett
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/cornett-chase.jpg
  - name: Mackenzie Wakefield
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/wakefield-mackenzie.jpg
  - name: Alec Rankin
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/rankin-alec.jpg
  - name: Allison Schuh
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/schuh-allison.jpg
  - name: Zachary Harris
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Zachary Harris.jpg
  - name: Olivia Turnage
    year: 2024
    scholarshipImage: ''
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              Brian Vincent was a National Merit Scholar, and was active in
              track, wrestling, band and drama in high school. He was an Eagle
              Scout and counselor at Boy Scout camp. He went on to run on the
              cross country and track teams at Iowa State University, but
              tragically lost his life in a plane crash before graduating. This
              scholarship rewards those who take advantage of the opportunities
              for broadening their lives by participating in a wide variety of
              activities while maintaining high academics. He/she would be the
              type of student who would be expected to continue that involvement
              into college and adulthood, gaining broad perspectives, and
              bringing together people with diverse backgrounds and passions.
              Each year, presents the Vincent Award to one female and one male
              AHS senior.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
