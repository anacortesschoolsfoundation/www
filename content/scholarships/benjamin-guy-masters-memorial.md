---
title: Benjamin Guy Masters Memorial Scholarship
est: 2021
areas:
  - Memorial
featuredImage:
keywords: >-
  Anacortes School District, scholarship, scholarships, scholarship list,
  available scholarships, anacortes scholarships, Benjamin Guy Masters
description: >-
  The Benjamin Guy Masters Memorial Scholarship, honors the memory of Benjamin,
  who passed away of brain cancer at age 10.
donorImage:
recipients:
  - name: Evan Baldwin
    year: 2022
    scholarshipImage:
  - name: Sydney Long
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/long-sydney.jpg
  - name: Casey Lemrick
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Casey Lemrick.png
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              The Benjamin Guy Masters Memorial Scholarship, established by the
              Masters Family, honors the memory of their son and brother,
              Benjamin, who passed away of brain cancer at age 10. Ben had many
              talents and interests, but is remembered most for his focus on the
              feelings of others. He got along with everyone. He was an ardent
              reader, deep thinker and interested in pursuing knowledge. The
              recipient of this scholarship will be a student who has gone above
              and beyond normal expectations to care for others.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
