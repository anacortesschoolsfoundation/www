---
title: Eastern Star Dorothy Etter Memorial Vocational Scholarship
est: 2020
areas:
  - Technical or Vocational
description: >-
  The Dorothy Etter Memorial Vocational Scholarship was established by Madrona
  Chapter 89, Order of the Eastern Star to assist students in attaining their
  goals in education.
keywords: >-
  scholarship, Eastern Star scholarship, Madrona Eastern Star scholarship,
  technical scholarship, vocational scholarship, Dorothy Etter scholarship
featuredImage: https://asfmedia.imgix.net/donor-eastern-star.jpg
donorImage: https://asfmedia.imgix.net/donor-eastern-star.jpg
recipients:
  - name: Elizabeth Koals
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/koals-elizabeth.jpg
  - name: Lida Ahsaei
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/IMG_4967.JPG
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: |
              This award is for the chosen field of study of the recipient.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
