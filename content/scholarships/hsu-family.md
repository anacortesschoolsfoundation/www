---
title: Hsu Family Scholarship
est: 2012
areas:
  - STEM
  - Business
description: The Hsu Family Scholarship is awarded to a student currently attending college or to a graduating senior attending a public university in Washington state who is majoring in Engineering, Sciences, or Business.
keywords: scholarship, Hsu Family scholarship, Ed Hsu scholarship, Ming Hsu scholarship, engineering scholarship, science scholarship, business scholarship
featuredImage:
donorImage:
recipients:
  - name: Milly Lee
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/lee-milly.jpg
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship

            content: |
              Ed and Ming Hsu established the Hsu Family Scholarship in March of 2007. It is awarded to a student currently attending college or to a graduating senior attending a public university in Washington state who is majoring in Engineering, Sciences, or Business. Academic achievement is the number one criteria for this award which is renewable for up to up to three additional awards in future years, provided scholarship criteria are met.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
