---
title: Joe Fox Award
est: 2001
areas:
  - Memorial
description: >-
  Joe was a young man who always enjoyed life to the fullest, but he is also
  remembered for the big heart he had for others in need. His mother, who taught
  high-risk teens, established this fund in her son's memory.
keywords: scholarship, Joe Fox scholarship
featuredImage:
donorImage:
recipients:
  - name: Cameron Berow
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Joe Fox - Berow, Cameron.jpg
  - name: Amanda Elling
    year: 2021
    scholarshipImage:
  - name: Gavon Long
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/long-gavon.jpg
  - name: Maricela (Monica) Luevanos
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/luevanos-maricela.jpg
  - name: Marin Jensen
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Marin Jensen.jpeg
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              Joe Fox was raised in a log-framed cedar home on Trafton Lake, a
              hidden woodland paradise on the south end of Fidalgo Island. The
              outdoor environment played a significant role in his upbringing,
              crafting a young man drenched in adventurous spirit. When his
              friends showed up to visit him at his home, they all knew first to
              look out at the lake, where they often found him swimming,
              sailing, fishing or canoeing. Joe was preparing to attend the
              University of Hawaii in the fall following his graduation. Joe's
              plans would change abruptly, however, when tragedy struck in the
              summer between his junior and senior year. On June 26,1988, Joe's
              life was tragically taken by a drunk driver. Joe was a young man
              who always enjoyed life to the fullest, but he is also remembered
              for the big heart he had for others in need. His mother, who
              taught high-risk teens, established this fund in her son's memory.
              Keith Anderson, a school counselor, contributed to this fund for
              many years, as well as his wife Donna Anderson. Joe's family
              continues to memorialize Joe with this scholarship which is
              awarded to a student with passion for their future and the
              determination to see it through.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
