---
title: Anacortes High School PTSA Scholarship
est: 2014
areas:
  - Community Service
featuredImage:
keywords: >-
  Anacortes School District, scholarship, scholarships, scholarship list,
  available scholarships, anacortes scholarships, AHS PTSA, AHS PTSA Scholarship
description: >-
  This scholarship is funded by the Parent, Teacher, Student Association (PTSA)
  of Anacortes High School (AHS).
donorImage:
recipients:
  - name: Claire Schnabel
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/PTSA - Schnabel, Claire.jpeg
  - name: Chloe Chambers
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/AHS PTSA - Chambers, Chloe.JPG
  - name: Ariana Bickley
    year: 2022
    scholarshipImage:
  - name: Lauren Cunningham
    year: 2021
    scholarshipImage:
  - name: Allison Weber
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/weber-allison.jpg
  - name: Connor Hutchings
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/hutchings-connor.jpg
  - name: Megan Rowell
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/rowell-megan.jpg
  - name: Parker Mong
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Parker Mong.jpg
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              This scholarship is awarded to graduating seniors who best
              exemplify the standards and ideals of the PTSA by enhancing the
              lives of their fellow students, and by giving back in some manner
              to AHS and the community at-large.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
