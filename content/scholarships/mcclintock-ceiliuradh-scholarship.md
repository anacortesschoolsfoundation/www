---
title: McClintock Ceiliuradh Scholarship
est: 2024
areas:
  - General
description: >-
  This scholarship is awarded to a successful student planning to attend a
  4-year university.
keywords: scholarship, McClintock
featuredImage: https://asfmedia.imgix.net/Copy of DSC_0491.JPG
donorImage: https://asfmedia.imgix.net/Copy+of+DSC_0491.JPG
recipients:
  - name: Franklin Peterson
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Franklin Peterson.jpg
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >-
              Ceiliuradh is “Celebrate” in Gaelic. This scholarship is
              established by friends in honor of Marta McClintock and
              “celebrates” her tenure as Executive Director of the Anacortes
              Schools Foundation. Marta served from 2017 to 2023 and was the
              first ED for ASF. This scholarship celebrates Marta’s love of
              education, her dedication to Anacortes students and her desire to
              support those continuing their education after high school.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
