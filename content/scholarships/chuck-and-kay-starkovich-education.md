---
title: Chuck and Kay Starkovich Education Scholarship
est: 2021
areas:
  - Education
keywords: >-
  Anacortes School District, scholarship, scholarships, scholarship list,
  available scholarships, anacortes scholarships, Chuck Starkovich, Kay
  Starkovich, starkovich scholarship
description: >-
  This award goes to a student pursuing a degree in education with preference to
  elementary education.
featuredImage:
donorImage:
recipients:
  - name: Abbie Goodwin
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Starkovich - Goodwin, Abbie.jpg
  - name: Connor Cunningham
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/cunningham-conner.jpg
  - name: Reese Morgenthaler
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Reese Morgenthaler.jpg
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              The Chuck and Kay Starkovich Education Scholarship was established
              by Angela and Chad Currie, Tyler and Monique Starkovich and Tim
              and Tara Starkovich in honor of their parent's 50th Wedding
              Anniversary and in honor of their dedication and devotion to
              teaching. Chuck and Kay were long time elementary educators in the
              Anacortes School District, with Chuck teaching most of his
              forty-two years at Fidalgo Elementary School and Kay teaching all
              of her twenty-five years at Mt. Erie Elementary School. This award
              goes to a student pursuing a degree in education with preference
              to elementary education
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
