---
title: Gordon Bruchner Engineering Scholarship
est: 2021
areas:
  - Engineering
description: >-
  Mr. Bruchner graduated from the Colorado School of Mines with a BS in Civil
  Engineering and Geology and went on to work in Silverton and Grand Junction,
  Colorado. He later moved to Washington State and became the City Engineer for
  Anacortes.
keywords: scholarship, engineering scholarship
featuredImage:
donorImage:
recipients:
  - name: Tenzin Stewart
    year: 2022
    scholarshipImage:
  - name: Eli Hankey
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Bruchner - Hankey, Eli.jpg
  - name: Jacob Harrison
    year: 2022
    scholarshipImage:
  - name: Ethan Harris
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/harris-ethan.jpg
  - name: Logan Hilyer
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/hilyer-logan.jpg
  - name: Sara Kumar
    year: 2021
    scholarshipImage:
  - name: Noah Masten
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/masten-noah.jpg
  - name: Maximus Adam
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Max Adam.JPG
  - name: Sofia Hernandez Rodriguez
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Sofia HR.jpeg
  - name: Ian Lujan
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Ian Lujan.jpg
  - name: Samuel Williams
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Samuel Williams.jpg
  - name: Mattias Thornton
    year: 2024
    scholarshipImage: ''
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              The Gordon Bruchner Engineering Scholarship was established by the
              estate of Gordon William Bruchner.  Mr Bruchner was born in Clear
              Lake, IA. He graduated from Clear Lake High School in 1950 and he
              was drafted into the US Army where he saw active duty in the
              Korean War. After discharge, Gordon graduated from the Colorado
              School of Mines with a BS in Civil Engineering and Geology. He
              started work in Silverton, CO, and later moved to Grand Junction
              where he worked and became partial owner of Arix. After retiring
              from Arix, Gordon moved to Washington State and became the City
              Engineer for Anacortes.  His passions in his younger days included
              hiking, camping and biking in the Colorado mountains. These
              progressed to road trips as he aged with the occasional overseas
              vacation. He was always active in his community. Mr. Bruchner
              passed away on June 13, 2019. Mr. Bruchner had the opportunity of
              going to college, earning an engineering degree, and applying that
              education to a career in engineering.  He would like to help
              provide that same opportunity to deserving Anacortes students.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
