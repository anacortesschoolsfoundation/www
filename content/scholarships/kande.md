---
title: Nancy Clark Memorial (KaNDe) Scholarship
est: 2016
areas:
  - Health and Human Services
  - Memorial
description: >-
  The Nancy Clark Memorial (KaNDe) Scholarship was established by the Clark
  family to make funds available to invest in the education of an Anacortes
  student pursuing a career in a field related to Healthcare, with a preference
  toward nursing studies.
keywords: >-
  scholarship, Clark family scholarship, Kevin Clark scholarship, Nancy Clark
  scholarship, Duane Clark scholarship
featuredImage:
donorImage:
recipients:
  - name: Mileen Gallardo
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/KaNDe - Gallardo, Mileen.jpg
  - name: Abby Ries
    year: 2022
    scholarshipImage:
  - name: Macy Mong
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/KaNDe - Mong, Macy.JPG
  - name: Makenna Goss
    year: 2021
    scholarshipImage:
  - name: Nicole Romer
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/romer-nicole.jpg
  - name: April Davis
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/davis-april.jpg
  - name: Kaitlyn Sage
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/sage-kaitlyn-2.jpg
  - name: Samantha Koals
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Samantha Koals.jpg
  - name: Lily Prue
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/IMG_2743.jpg
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >-
              The name, KaNDe, is an acronym made up of the first letters of the
              names of the Clark family – Kevin (son & computer guy), Nancy
              (wife & RN), and Duane (husband & Senior healthcare facilities
              owner and operator).
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
