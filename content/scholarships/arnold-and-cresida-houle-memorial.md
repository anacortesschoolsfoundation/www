---
title: Arnold & Cressa Houle Memorial Scholarship
est: 1982
areas:
  - Memorial
featuredImage:
keywords: >-
  Anacortes School District, scholarship, scholarships, scholarship list,
  available scholarships, anacortes scholarships, Arnold Houle scholarship,
  cressa houle scholarship, Houle memrial scholarship
description: >-
  In 2005, ASF began facilitating the Arnold and Cressa Houle Memorial
  Scholarship to help a student attending a 2 or 4-year college or vocational
  school.
donorImage:
recipients:
  - name: Kyan Bauer
    year: 2022
    scholarshipImage:
  - name: Ava Kephart
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/kephart-ava.jpg
  - name: Tatum Olsen
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Tatum Olsen.jpg
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              Arnold and Cressa did not have any children but they believed
              strongly in education and established this scholarship to help a
              student attending a 2 or 4-year college or vocational school. The
              number one criteria for this award is someone who shows
              responsible citizenship.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
