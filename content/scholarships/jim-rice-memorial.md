---
title: Jim Rice Memorial Scholarship
est: 1990
areas:
  - Memorial
description: >-
  The Jim Rice Memorial Scholarship is awarded to a graduating senior going to
  vocational school or college who manifests good citizenship in school,
  self-reliance and positive character.
keywords: Jim Rice Scholarship, scholarship, vocational school scholarship
featuredImage:
donorImage:
recipients:
  - name: Aidan Pinson
    year: 2022
    scholarshipImage:
  - name: Caden McOmber
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/mcomber-caden.jpg
  - name: Caden Trumble
    year: 2024
    scholarshipImage: ''
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              Jim Rice served as mayor of Anacortes from 1982 until his death in
              1991.Originally from Hatton, North Dakota, Jim moved to Seattle
              shortly after his high school graduation and was promptly drafted
              into the U.S. Army during W.W.II. Jim was stationed in Seward,
              Alaska with the Corps of Engineers. It was in Seward that Jim met
              his future wife Margie, a former Anacortes resident. When the war
              was over, the young couple decided to make their home in Seattle
              where Jim studied the marine carpentry trade at Edison Technical
              School. Jim worked in Seattle for several years as a journeyman
              carpenter, and was eventually hired by Don McKee, a Seattle
              architect and Anacortes High School classmate of Margie's. When
              Mr. McKee returned to Anacortes, the Rice family followed. Jim
              eventually started his own general contracting business and built
              many of the homes and commercial buildings in Anacortes. It was
              during the mid-1950's that Jim launched his political career when
              he was elected to fill a vacant city council seat. Throughout his
              nearly 40 years in politics, Mr. Rice served the citizens of
              Anacortes unselfishly. Jim is remembered for his integrity and
              dedication to honesty while in office. Throughout his life, Jim's
              Christian faith sustained him. He was a lifelong member of the
              Lutheran Church. Jim was a compassionate man who performed many
              humanitarian deeds, often known only to his family. On several
              occasions, for instance, he quietly paid property taxes or bought
              groceries for Anacortes citizens who needed some extra help. Jim
              and Margie Rice understood the importance of education and were
              ardent supporters of the Anacortes schools. All four of their
              children graduated from Anacortes High School. Jim was an active
              member of the Anacortes Elks, Eagles, Rotary and the VFW. Mr. Rice
              passed away in 1991, and Margie established this scholarship fund
              in his honor. It is awarded to a graduating senior going to
              vocational school or college who manifests good citizenship in
              school, self-reliance and positive character. Special
              consideration may be given to a student who has regularly worked
              hard in school who may have had serious difficulty and have made a
              positive change during high school.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
