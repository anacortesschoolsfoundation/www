---
title: Marathon Community Scholarship
est: 2007
areas:
  - STEM
  - Technical or Vocational
description: >-
  Marathon Petroleum supports many ASF programs and is a strong partner in STEM
  enrichment.
keywords: Marathon scholarship, STEM scholarship, Marathon STEM
featuredImage: https://asfmedia.imgix.net/donor-marathon.jpg
donorImage: https://asfmedia.imgix.net/donor-marathon.jpg
recipients:
  - name: Genevieve Kochel
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Marathon - Kochel, Genevieve.jpg
  - name: McKernan Boland
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Marathon - Boland, McKernan.jpg
  - name: Alexander Carroll
    year: 2022
    scholarshipImage:
  - name: Chase Cornett
    year: 2022
    scholarshipImage:
  - name: Henry Shaw
    year: 2022
    scholarshipImage:
  - name: Ellie Goodwin
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Marathon - Goodwin, Ellie.PNG
  - name: Alyssa Jensen
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Marathon - Jensen, Alyssa.jpg
  - name: Chase Cornett
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/cornett-chase.jpg
  - name: Jayden Frydenlund
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/frydenlund-jayden.jpg
  - name: Ellie Goodwin
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/goodwin-ellie.jpg
  - name: Kyler Martindale
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/martindale-kyler.jpg
  - name: Evan Rodriguez
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/rodriguez-evan.jpg
  - name: Aspen Eagle
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/eagle-aspen.jpg
  - name: Garrett Kennedy
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/kannedy-garrett.jpg
  - name: Quinn Millard
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/millard-quinn.jpg
  - name: Cameron Rice
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/rice-cameron.jpg
  - name: Evan Rodriguez
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/rodriguez-evan.jpg
  - name: Eliot Briefer
    year: 2019
    scholarshipImage:
  - name: Jayden Frydenlund
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/frydenlund-jayden.jpg
  - name: Hannah Gleason
    year: 2019
    scholarshipImage:
  - name: David Rodriguez
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/rodriguez-david.jpg
  - name: Riley Ward
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/ward-riley.jpg
  - name: Jose Charnley
    year: 2024
    scholarshipImage: ''
  - name: Alexander Carroll
    year: 2024
    scholarshipImage: ''
  - name: Ava Hightower
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Ava Hightower.jpg
  - name: McKernan Boland
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/McKernan Boland.jpg
  - name: Allison Pianetta
    year: 2024
    scholarshipImage: ''
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              Each year the Anacortes Marathon Refinery awards scholarships to
              students attending an accredited university or community college
              and/or a technical or trade school. Scholarships support those
              majoring in engineering, sciences, or a trade skill in process
              technology, instrumentation and control systems, welding or
              machinery and must be used for academic pursuit in the upcoming
              school year.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
