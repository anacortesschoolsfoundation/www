---
title: How to Apply for Scholarships
aliases:
  - /scholarships/scholarship-applications.html
  - /about-us/31-general/general/69-scholarship-applications.html
bannerImage: https://asfmedia.imgix.net/page-title-scholarships.jpg
description: >-
  Applying for scholarships is easy! Submitting the ASF General Scholarship
  Application makes you eligible for most ASF scholarships, so there's no need
  to submit multiple applications for the many scholarships that ASF offers.
keywords: >-
  Anacortes Schools, Anacortes School District, scholarships, how to apply for
  scholarships, how do I apply for scholarships
featuredImage: https://asfmedia.imgix.net/schol-1.jpg
layout: blocks
content_blocks:
  - _bookshop_name: hero
    title: How to Apply for Scholarships
    bannerImage: https://asfmedia.imgix.net/page-title-scholarships.jpg
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: Content
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content
            title: Applying for scholarships is easy!
            content: >-
              Submitting the online ASF General Scholarship Application makes
              you eligible for most ASF scholarships, so there's no need to
              submit multiple applications for the many scholarships that ASF
              offers!


              {{< button text="Apply Here Beginning April 1"
              url="https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2025GeneralScholarshipApplication"
              >}}


              If you have any questions about the application process, please
              email [scholarships@asfkids.org](mailto:scholarships@asfkids.org).


              ### All Applicants:


              Be sure to have the following documents available for upload as
              they are required to complete your application:


              * Resume

              * Personal Statement

              * Official Transcripts (Graduating AHS Seniors can opt to have ASD
              provide the transcripts)

              * Student Aid Report (SAR) or Written Statement of Financial Need
              (if you would like to be considered for financial need awards)
      - name: Dates
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
            sticky: true
---
