---
title: Anacortes Sister City Association Scholarship
est: 2013
areas:
  - International
featuredImage: https://asfmedia.imgix.net/donor-anacortes-sister-cities.jpg
keywords: >-
  Anacortes School District, scholarship, scholarships, scholarship list,
  available scholarships, anacortes scholarships, sister cities association,
  sister cities scholarship
description: >-
  This scholarship was established in 2013 by the Anacortes Sister Cities
  Association's (ASCA's) Board of Directors.
donorImage: https://asfmedia.imgix.net/donor-anacortes-sister-cities.jpg
recipients:
  - name: Isabel Tabor
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Sister Cities - Tabor, Isabel.jpg
  - name: Zoie Schwartz
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Anacortes Sister Cities - Schwartz, Zoie.jpg
  - name: Alison Perez
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/perez-alison.jpg
  - name: Carson Lindholm
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/lindholm-carson.jpg
  - name: Emma Riedel
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/riedel-emma.jpg
  - name: Alyssa Watson
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/watson-alyssa.jpg
  - name: Rachel Hankey
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Rachel Hankey.JPEG
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              It is awarded to a graduating senior who is pursuing a degree in
              International Studies and/or Languages or a current college
              student going abroad in an exchange program.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
