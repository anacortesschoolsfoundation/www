---
title: Zombie Apocalypse Scholarship
est: 2024
areas:
  - General
description: >-
  This scholarship is awarded to a graduating senior who has demonstrated
  personal resilience and/or scholastic improvement in the last two years of
  high school.
keywords: scholarship, resilience, determination, zombie
featuredImage: https://asfmedia.imgix.net/resilence.jpeg
donorImage: https://asfmedia.imgix.net/resilence.jpeg
recipients:
  - name: Payton (Moss) Caplinger
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Moss+Caplinger.jpg
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >-
              This scholarship is established by a local family who wishes to
              remain anonymous. This family wants to support advanced education
              for those who may have had a rocky start to their high school
              experience. Surviving a challenging situation, such as a zombie
              apocalypse, or making a marked decision to do better, to try
              harder, is no easy feat. Resilience, determination, and tenacity
              are traits that should be rewarded during the pursuit of higher
              education.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
