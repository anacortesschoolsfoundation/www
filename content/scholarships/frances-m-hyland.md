---
title: Frances M. Hyland Scholarship
est: 2015
areas:
  - Memorial
description: >-
  The Frances M. Hyland Scholarship was established by friends and family in
  loving memory of Frances M. Hyland who received a degree in Speech and Music
  from Northwestern Christian University, following which she was a speech
  professor there for 27 years.
keywords: scholarship, Frances Hyland scholarship
featuredImage:
donorImage:
recipients:
  - name: Jessica Frydenlund
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Hyland - Frydenlund, Jessica.jpeg
  - name: Keily Zuniga-Trejo
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Frances M. Hyland - Zuniga-Trejo, Keily.jpg
  - name: Ella Villines
    year: 2021
    scholarshipImage:
  - name: Madison Evans
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/evans-madison.jpg
  - name: Sarah Quinn
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/quinn-sarah.jpg
  - name: Jocelyn Garner
    year: 2024
    scholarshipImage: ''
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              This scholarship has been established in loving memory of Frances
              M. Hyland. Frances received a degree in Speech and Music from
              Northwestern Christian University, following which she was a
              speech professor there for 27 years. She leaves a legacy of
              grateful college freshman that learned to speak in front of a
              group thanks to her instruction and encouragement. Frances also
              served as an adjunct professor of speech at the University of
              Oregon during World War II. Frances was a beloved citizen of many
              communities in her lifetime and always involved in community
              service and educational or church organizations that promoted the
              value and education of young people. This scholarship is awarded
              to a female graduating AHS senior.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
