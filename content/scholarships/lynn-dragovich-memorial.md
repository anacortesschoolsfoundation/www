---
title: Lynn Dragovich Memorial Scholarship
est: 2020
areas:
  - Music
  - Memorial
description: >-
  The Lynn Dragovich Memorial Scholarship is awarded yearly to a current or past
  Anacortes High School student majoring in music.
keywords: scholarship, Lynn Dragovich, Lynn Dragovich scholarship
featuredImage: https://asfmedia.imgix.net/donor-dragovich.jpg
donorImage: https://asfmedia.imgix.net/donor-dragovich.jpg
scholarship: true
draft: false
recipients:
  - name: Patrick Quinn
    year: 2022
    scholarshipImage:
  - name: Dominic Ermi
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/ermi-dominic.jpg
  - name: Kellen Murphy
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Kellen Murphy.jpeg
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              Lynn Dragovich graduated from Anacortes High School in 1979. He
              attended Central Washington University and Arizona State
              University prior to joining the U.S. Army, where he was able to
              pursue his life's passion by playing clarinet in the Army Field
              Band. His father, Vince, recalled that Lynn "played the sax,
              flute, drums, piano, you name it. But the clarinet was his baby."
              Sadly, at the age of 26, Lynn lost his battle with AIDS. Lynn's
              family was determined to honor his life with the gift of a
              memorial scholarship in their son's name. For nearly 25 years,
              Vince Dragovich collected and recycled cans to fund this
              scholarship. The Lynn Dragovich Memorial Scholarship is awarded
              yearly to a current or past Anacortes High School student majoring
              in music.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
