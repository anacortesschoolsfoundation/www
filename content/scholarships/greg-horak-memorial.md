---
title: Greg Horak Memorial Scholarship
est: 1996
areas:
  - Memorial
description: This scholarship is offered as a tribute to Greg and his memory and is awarded to a student who is self-reliant and shows concern for others.
keywords: scholarship, Grek Horak scholarship
featuredImage:
donorImage:
recipients:
  - name: Guadalupe Ruiz
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Horak - Ruiz, Guadalupe.PNG
  - name: Allyson Von Hagel
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/von-hagel-allyson.jpg
  - name: Hannah Reilly
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/reilly-hannah.jpg
scholarship: true
draft: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship

            content: |
              This scholarship is named for Greg Horak who graduated from Anacortes High School in 1989, and passed away on July 24, 1994. 
              Greg had a caring sensitivity and concern for others; he was independent, self-reliant and had a true love of life. He is most remembered for his desire to serve others. In his senior year at AHS, Greg operated Safe Rides, a program offering late-night rides to anyone in need operating out of Dr. Anderson's (AHS counselor) basement. After graduation, Greg's desire to be of service and save lives led him to join the U.S. Coast Guard. He proudly served as a Radioman and was stationed in Kodiak, Alaska, Tokyo, Japan, and on the Polar Star icebreaker out of Seattle. This scholarship is offered as a tribute to Greg and his memory and is awarded to a student who is self-reliant and shows concern for others. It is given to a graduating senior or AHS graduate going to a vocational school or college who reflects the character qualities exemplified by Greg including a caring sensitivity and concern for others, independence, self-reliance, and a need to help and serve. The purpose of this award is to help an individual to improve him or herself and to be used in the best possible way to accomplish this goal. **A TRUE LOVE OF LIFE is a priority for this scholarship recipient!**
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
