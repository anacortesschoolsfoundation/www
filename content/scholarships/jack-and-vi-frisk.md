---
title: Jack and Vi Frisk Scholarship
est: 2016
areas:
  - General
description: >-
  The Jack and Vi Frisk scholarship was established to continue that tradition
  of giving and to further promote the education of our youth and is awarded to
  a student attending any Washington State college, university, or career
  technical institution.
keywords: >-
  scholarship, Jack Frisk scholarship, Vi Frisk scholarship, Jack Frisk, Vi
  Frisk, technical school scholarship
featuredImage:
donorImage:
recipients:
  - name: Kathern Johnson
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Jack _ Vi Frisk - Johnson, Kathern.jpg
  - name: Conner Cho
    year: 2021
    scholarshipImage:
  - name: Hunter Umayam
    year: 2024
    scholarshipImage:
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              Jack and Viola (Vi) Frisk were long-time Anacortes residents who
              were actively involved in the community and with education. While
              not originally from Anacortes, Jack's work brought them through
              Anacortes, and they eventually came back here in retirement. Jack
              was an educator, when not serving as a hospital corpsman in WWII.
              He again served during the Korean War. He started as a teacher,
              then a principal, and finally served as the Superintendent of
              Schools in several Washington cities, including Anacortes. Vi was
              an active volunteer, a wonderful mother, and Jack's biggest
              supporter. Both were active Rotarians and believed in giving back
              to the community, both financially and with their time. This
              scholarship was established to continue that tradition of giving
              and to further promote the education of our youth. It is awarded
              to a student attending any Washington State college, university,
              or career technical institution.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
