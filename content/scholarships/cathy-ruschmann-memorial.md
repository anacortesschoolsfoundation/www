---
title: Cathy Ruschmann Memorial Scholarship
est: 2021
areas:
  - Health and Human Services
  - Memorial
keywords: >-
  Anacortes School District, scholarship, scholarships, scholarship list,
  available scholarships, anacortes scholarships, Cathy Ruschmann memorial
  scholarship
description: >-
  The Cathy Ruschmann Memorial Scholarship was established through The Jean L.
  Copeland Living Trust in honor of Cathy Ruschmann.
featuredImage: https://asfmedia.imgix.net/Donor-Ruschmann.jpg
donorImage: https://asfmedia.imgix.net/Donor-Ruschmann.jpg
recipients:
  - name: Lauren Conrardy
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Cathy Ruschmann - Conrardy, Lauren.jpg
  - name: Lauren Kyllo
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Cathy Ruschmann - Kyllo, Lauren.jpg
  - name: Emmalee Carlson
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/carlson-emmalee.jpg
  - name: Samantha Koals
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Samantha+Koals.jpg
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              It is awarded to an Anacortes graduate who plans to study nursing
              or pursue a career in the field of medicine.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
