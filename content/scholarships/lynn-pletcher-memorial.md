---
title: Lynn Pletcher Memorial Scholarship
est: 2022
areas:
  - General
keywords: >-
  Anacortes School District, scholarship, scholarships, scholarship list,
  available scholarships, anacortes scholarships,
description: >-
  This scholarship is established by Jennifer and Brett Pletcher in memory of
  Brett's father Lynn Pletcher.
featuredImage: https://asfmedia.imgix.net/scholarship-pletcher.jpg
donorImage: https://asfmedia.imgix.net/scholarship-pletcher.jpg
recipients:
  - name: Keily Zuniga-Trejo
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Pletcher - Zuniga-Trejo, Keily.jpg
  - name: Tierney Boland
    year: 2024
    scholarshipImage: ''
  - name: Shrey Patel
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Shrey Patel.JPG
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              Lynn Pletcher was a teacher, principal and head of Special
              Education during his more than 40-year career in public education.
              He was always looking to extend a hand to those students who, for
              whatever reason, had to work harder than everyone else to achieve
              their education. This scholarship is awarded to an Anacortes High
              School graduating senior who has overcome challenges in their life
              to achieve their education.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
