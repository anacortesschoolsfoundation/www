---
title: Fidalgo Masonic Lodge Scholarship
est: 2020
areas:
  - Technical or Vocational
description: >-
  The Fidalgo Masonic Scholarship is established by the Fidalgo Masonic
  Scholarship Foundation to assist a motivated student who has set goals and
  either needs or merits help to achieve them.
keywords: scholarship, Fidalgo Masons scholarship, Fidalgo Masonic Scholarship
featuredImage:
donorImage:
recipients:
  - name: Lucca Dumas
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Masonic Lodge - Dumas, Lucca.jpg
  - name: Annabella Isaacson
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/isaacson-annabella.jpg
  - name: Liela Saunders
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/saunders-liela.jpg
  - name: Colin Bielitz
    year: 2024
    scholarshipImage: ''
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              The Foundation has a particular interest in students who are
              pursuing higher education in a trade or technical school.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
