---
title: Anacortes Schools Foundation Scholarship
est: 2006
areas:
  - General
featuredImage: https://asfmedia.imgix.net/donor-asf.jpg
keywords: >-
  Anacortes School District, scholarship, scholarships, scholarship list,
  available scholarships, anacortes scholarships, ASF
description: >-
  Awards are funded through generous donations from community members (some
  anonymously) and from ASF Board Members.
donorImage: https://asfmedia.imgix.net/donor-asf.jpg
recipients:
  - name: Ashley Millegan
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/ASF Science - Millegan, Ashley.JPG
  - name: Breann Morgenthaler
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/ASF - Morgenthaler, Breann.JPG
  - name: Jacob Hayes
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/ASF - Hayes, Jacob.jpg
  - name: Brittany Goss
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/ASF - Goss, Brittany.JPG
  - name: Jack Carlson
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/ASF - Carlson, Jack.jpg
  - name: Alexandra Bielitz
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/ASF - Bielitz, Alexandra.jpeg
  - name: Sophia Carlson
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/ASF - Carlson, Sophia.jpg
  - name: Ellie Notaro
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/ASF - Notaro, Ellie.jpg
  - name: Hannah Peak
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/ASF - Peak, Hannah.jpg
  - name: Victoria Pittman
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/ASF - Pittman, Victoria.jpg
  - name: Kirsha Khile
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/ASF Donor - Khile, Kirsha.jpg
  - name: Gabriel Burnett
    year: 2022
    scholarshipImage:
  - name: Kassandra Jenson
    year: 2022
    scholarshipImage:
  - name: Matthew Lujan
    year: 2022
    scholarshipImage:
  - name: Grace Tesch
    year: 2022
    scholarshipImage:
  - name: Ella Villines
    year: 2022
    scholarshipImage:
  - name: Gabriel Burnett
    year: 2022
    scholarshipImage:
  - name: Jessica Augustoni
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/augustoni-jessica.jpg
  - name: Brendan Hodgson
    year: 2021
    scholarshipImage:
  - name: Charlie Reyerson
    year: 2021
    scholarshipImage:
  - name: Abigail Schnabel
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/schnabel-abigail.jpg
  - name: Gabriella Snowman
    year: 2021
    scholarshipImage:
  - name: Aaliyah Spencer
    year: 2021
    scholarshipImage:
  - name: Grace Anne Evans
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/evans-grace-anne.jpg
  - name: Hope Evans
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/evans-hope.jpg
  - name: Olivia Feist
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/feist-olivia.jpg
  - name: Joey Keltner
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/keltner-joey.jpg
  - name: Brigid Mack
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/mack-brigid.jpg
  - name: Lauren McClintock
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/mcclintock-lauren.jpg
  - name: Teja Rasmussen
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/rasmussen-teja.jpg
  - name: Maia Stovel
    year: 2021
    scholarshipImage:
  - name: Grace Tesch
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/tesch-grace.jpg
  - name: Heidi Vandyk
    year: 2021
    scholarshipImage:
  - name: Zoe Yanega
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/yanega-zoe.jpg
  - name: Elizabeth (Ellie) Chambers
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/chambers-ellie.jpg
  - name: Abigail Hylton
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/hylton-abby.jpg
  - name: Hannah Loesch
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/loesch-hannah.jpg
  - name: Macy Yanega
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/yanega-macy.jpg
  - name: Alexa Campos
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/campos-alexa.jpg
  - name: Reese Vaux
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/vaux-reese.jpg
  - name: Parker Mong
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Parker+Mong.jpg
  - name: Faye Lopez
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Faye Lopez.jpg
  - name: Adam Stovel
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Adam Stovel.jpg
  - name: Hannah McReynolds
    year: 2024
    scholarshipImage: ''
  - name: Ruby Johnson
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Ruby Johnson.jpg
  - name: Amelia Mortimer
    year: 2024
    scholarshipImage: ''
  - name: Sienna Paggao
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Sienna Paggao.jpg
  - name: Bella Gentry
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Bella Gentry.jpg
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              The Anacortes Schools Foundation Scholarships celebrate ASF's
              commitment to scholarships since 1982. Awards are funded through
              generous donations from community members (some anonymously) and
              from ASF Board Members. Awards are based on academic achievement
              (3.0 GPA or higher) and financial need may be a consideration.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
