---
title: Ian Simensen Music Scholarship
est: 2018
areas:
  - Music
  - Memorial
description: >-
  Memorial Gifts received in honor of AHS Music Teacher Ian Simensen who passed
  in September of 2017 of brain cancer helped establish the "Ian Simensen Music
  Scholarship Fund."
keywords: >-
  scholarship, Anacortes music scholarship, Ian Simensen, Ian Simensen
  scholarship, band scholarship
featuredImage:
donorImage:
recipients:
  - name: Sydney Read
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Simensen - Read, Sydney.jpg
  - name: Kellen Murphy
    year: 2023
    scholarshipImage: https://asfmedia.imgix.net/Simensen - Murphy, Kellen.JPG
  - name: Lily Iverson
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Ian Simensen - Iverson, Lily.jpg
  - name: Cassius Tossavainen
    year: 2022
    scholarshipImage: https://asfmedia.imgix.net/Ian Simensen - Tossavainen, Cassius.jpg
  - name: Clare Martin
    year: 2021
    scholarshipImage: https://asfmedia.imgix.net/martin-clare.jpg
  - name: Dominic Ermi
    year: 2020
    scholarshipImage: https://asfmedia.imgix.net/ermi-dominic.jpg
  - name: Ava Martin
    year: 2019
    scholarshipImage: https://asfmedia.imgix.net/martin-ava.jpg
  - name: Stanton Molitor
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Stanton Molitor.jpg
  - name: Gibson Groenig
    year: 2024
    scholarshipImage: https://asfmedia.imgix.net/Gibson Groenig.jpg
scholarship: true
draft: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            hide_featured: false
            content: >
              Ian Kendal Simensen was born on August 12, 1986 in Portland,
              Oregon to Randy and Lerese Simensen and grew up in Vancouver,
              Washington. He spent his childhood enjoying music and sports and
              became a staunch fan of the Denver Broncos at the early age of
              eight. In high school, his family moved to Auburn, WA where he
              attended Auburn Riverside High School and it was here that his
              musical talent with the trumpet began to evolve. Ian excelled
              academically and especially enjoyed all things band and playing
              league soccer. After high school, Ian attended the University of
              Washington and graduated in 2009 with a major in Music Education.
              Ian also received his Master of Music degree from American Band
              College. His favorite memories of college were of his Husky
              Marching Band friends and experiences. In 2009, Ian moved to the
              Skagit Valley to teach band in Anacortes, WA. Ian's love of music
              and unbridled enthusiasm helped him turn the Anacortes band
              program into a thriving, competitive, and award winning program.
              He won the Walter A. Brodniak Cultural Education award in 2013.
              Ian believed the arts as a whole connected us to ourselves and
              each other. Ian was diagnosed with brain cancer in August 2016 and
              fought valiantly for the next 13 months and passed away at the age
              of 31 on Sunday, September 17th, surrounded by the love of family
              and friends. Ian left a special mark on Anacortes High School and
              on the community. Anacortes Schools Superintendent Mark Wenzel
              said, "He came to us fresh from the University of Washington in
              2009 with a passion for how music can inspire students and bring a
              community together. He did just that — building an award-winning
              band program and helping music flourish throughout Anacortes. He
              set high standards for our students, and they rose to the
              occasion. While his time with us was tragically cut short, he
              leaves a legacy of high achievement, connectedness and care for
              students that will live on with his memory." At his passing, Ian's
              family, many friends and students contributed to a memorial fund
              honoring this legacy. Through this fund, the Anacortes Schools
              Foundation has established the "Ian Simensen Music Scholarship."
              Ian's legacy of family, friends, fun, and music will live on in
              all of those who were blessed to know and love him, through the
              many students he inspired in person and will continue to inspire
              through his scholarship. "Band students are intelligent, musical
              and funny, and their growth is the best part of my job," Ian once
              said, so it is fitting that his scholarship will be awarded to
              students with a passion for music and a commitment to academics.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
---
