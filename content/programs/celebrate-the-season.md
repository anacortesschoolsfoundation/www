---
title: Celebrate the Season
aliases:
  - /celebrate2021
  - /celebrate2022
  - /celebrate
  - /events/celebrate-the-season.html
description: >-
  Join us for a holiday event hosted by ASF! More information coming soon so
  stay tuned!
keywords: >-
  Anacortes Schools, Anacortes School District, Celebrate the season, anacortes
  port, anacortes school fundraiser, anacortes schools foundation, anacortes
  school foundation fundraiser, anacortes auction, anacortes school auction, art
  contest, anacortes art contest, anacortes school art contest, anacortes
  foundation dinner, anacortes school celebration, school foundation fundraiser
  anacortes, anacortes district fundraiser, celebrate anacortes, anacortes
  school district, anacortes stem grants, anacortes scholarship fund fundraiser,
  anacortes celebration, ASD fundraiser, ASF Fundraiser
featuredImage: https://asfmedia.imgix.net/Featured Image.png
layout: blocks
mainclass:
content_blocks:
  - _bookshop_name: columns
    num_cols: 1
    columns:
      - name: Logo
        col_span: 1
        divclass: '!space-y-0'
        backgroundImage:
        column_components:
          - _bookshop_name: content
            title:
            content: >-
              {{< asf-figure title="Celebrate the Season Logo" alt="Celebrate
              the Season 2024"
              src="https://asfmedia.imgix.net/CTS-2024-Logo-600w.png" width=600
              link="https://event.auctria.com/70a0c00d-6349-4bf2-842f-6a6165df2a31/0c80ecb6db8e4a248d7e4eabaafc1fc5"
              divclass="hidden md:block mx-auto" figclass="w-full mx-auto"
              imgclass="mx-auto" >}}


              {{< asf-figure title="Celebrate the Season" alt="Celebrate the
              Season 2024"
              src="https://asfmedia.imgix.net/CTS-2024-Logo-Mobile.png"
              width=400
              link="https://event.auctria.com/70a0c00d-6349-4bf2-842f-6a6165df2a31/0c80ecb6db8e4a248d7e4eabaafc1fc5"
              divclass="block md:hidden mx-auto" figclass="w-full mx-auto"
              imgclass="mx-auto" >}}


              &nbsp;
            sectionclass: bg-moodyblue bg-opacity-90 p-8
          - _bookshop_name: media-text
            heading: Celebrate with us
            text: >
              This evening is not to be missed, featuring a live auction and an
              online auction, live entertainment, and celestial surprises.


              Our auction features items donated by local businesses,
              organizations, and families.
            image_right: true
            captions: false
            images:
              - title: 'Auction item: "Fill Your Winter with Baking"'
                src: >-
                  https://asfmedia.imgix.net/25-FillYourWinterWithBaking-Auctria.jpg
              - title: 'Auction item: "The Orcas Project"'
                src: https://asfmedia.imgix.net/999-OrcasProject.jpg
              - title: 'Auction item: "Fidalgo Coffee Roasters Bundle"'
                src: https://asfmedia.imgix.net/S6-Fidalgo.jpg
              - title: 'Auction item: "Hot Chocolate Bar"'
                src: https://asfmedia.imgix.net/Unknown2.jpg
              - title: 'Auction item: "Charcuterie Boards"'
                src: https://asfmedia.imgix.net/Unknown-Charcuterie.jpg
              - title: >-
                  Auction item: "The Brown Lantern Ale House Gift Cards and
                  Merch"
                src: https://asfmedia.imgix.net/15-BrownLantern-Auctria.jpg
            video: {}
            url:
            action:
            sectionclass: bg-moodyblue bg-opacity-90 2xl:px-24
            headingclass: font-caps text-ctsgold
            textclass: text-white
            actionclass:
          - _bookshop_name: media-text
            heading: Gala Fundraiser Dinner
            text: >-
              The Celebrate the Season Gala Fundraiser Dinner is ASF's biggest
              fundraiser of the year and features an online and live auction,
              live entertainment, an exquisite dinner and our Raise the Paddle.
              Stay tuned for more information coming soon!
            image_right: false
            images:
              - title: Celebrate the Season Gala
                src: https://asfmedia.imgix.net/2021-cts-001.jpg
              - title: Celebrate the Season Gala
                src: https://asfmedia.imgix.net/2022-cts-01.jpg
              - title: Celebrate the Season Gala
                src: https://asfmedia.imgix.net/2022-cts-02.jpg
              - title: Celebrate the Season Gala
                src: https://asfmedia.imgix.net/2022-cts-03.jpg
              - title: Celebrate the Season Gala
                src: https://asfmedia.imgix.net/2021-cts-1.jpg
              - title: Celebrate the Season Gala
                src: https://asfmedia.imgix.net/2021-cts-3.jpg
              - title: Celebrate the Season Gala
                src: https://asfmedia.imgix.net/2021-cts-4.jpg
            video: {}
            url:
            action:
            sectionclass: bg-moodyblue bg-opacity-90 2xl:px-24
            headingclass: font-caps text-ctsgold
            textclass: text-white
            actionclass:
          - _bookshop_name: media-text
            heading:
            text: >-
              As Washington State schools continue to struggle with funding
              basic education, ASF’s priorities remain the same. We want
              students to do more than wish upon a star. We want them to succeed
              in all areas of learning: academic, physical, and
              social-emotional.  


              In years past, we have targeted our Raise the Paddle to fund a
              specific program or purchase. Keeping our Raise the Paddle general
              will allow donations this year to continue funding all the
              programs we support. Because the need really is everywhere.
            image_right: true
            images:
              - title: Celebrate the Season Gala
                src: https://asfmedia.imgix.net/2022-cts-05.jpg
              - title: Celebrate the Season Gala
                src: https://asfmedia.imgix.net/2021-cts-6.jpg
              - title: Celebrate the Season Gala
                src: https://asfmedia.imgix.net/CTS-check.jpg
              - title: Celebrate the Season Gala
                src: https://asfmedia.imgix.net/CTS-registration.jpg
              - title: Celebrate the Season Gala
                src: https://asfmedia.imgix.net/CTS-Curtis.jpg
              - title: Celebrate the Season Gala
                src: https://asfmedia.imgix.net/CTS-Chavers-Irish.jpg
            video: {}
            url:
            action:
            sectionclass: bg-moodyblue bg-opacity-90 2xl:px-24
            headingclass: font-caps
            textclass: text-white text-orange-100
            actionclass:
          - _bookshop_name: media-text
            heading:
            text: >
              Please consider joining us as a <a href="/giving/sponsor/"><span
              style="color: #9ed0f0">2024 Celebrate the Season
              sponsor</span></a> and helping ASF provide an anchor in the storm
              that is mental health.
            image_right: false
            images:
              - title: Celebrate the Season Gala
                src: https://asfmedia.imgix.net/2021-cts-12.jpg
              - title: Celebrate the Season Gala
                src: https://asfmedia.imgix.net/2021-cts-13.jpg
              - title: Celebrate the Season Gala
                src: https://asfmedia.imgix.net/2021-cts-14.jpg
              - title: Celebrate the Season Gala
                src: https://asfmedia.imgix.net/2021-cts-16.jpg
            video: {}
            url: >-
              https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2024CelebrateTheSeasonSponsorshipForm
            action: Become a Celebrate Sponsor Today!
            sectionclass: bg-moodyblue bg-opacity-90 2xl:px-24
            headingclass: font-caps
            textclass: text-white text-orange-100
            actionclass:
    sectionclass: >-
      bg-[url('https://asfmedia.imgix.net/sparkle-bg.png')] max-w-none !py-0
      bg-fixed bg-no-repeat bg-cover
  - _bookshop_name: columns
    num_cols: 1
    columns:
      - name: Thank you!
        col_span: 1
        divclass: '!space-y-0'
        backgroundImage:
        column_components:
          - _bookshop_name: media-text
            heading: Thank you for your extraordinary support!
            text: >-
              Anacortes Schools Foundation would like to extend a huge thank you
              to our community for making "Celebrate the Season" a huge success
              each year!
            image_right: true
            images:
              - title:
                src:
                width:
                link:
                caption:
            video:
              heading:
              divclass:
              youtubeID: uUzYyo8ffGw
              text:
            url:
            action:
            sectionclass:
            headingclass:
            textclass:
            actionclass:
          - _bookshop_name: sponsors-grid
            title: Thank you to our generous sponsors of 2024 Celebrate the Season
            filename: sponsors-cts-2022
            headingclass: text-asfpurple font-caps
    sectionclass: max-w-none !py-0
---
