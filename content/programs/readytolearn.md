---
title: Ready to Learn Fair
aliases:
  - /ready
  - /ready-to-learn-fair.html
bannerImage: https://asfmedia.imgix.net/page-title-rtlf-2022.jpg
description: >-
  ASF provides back-to-school essentials for Anacortes families who need a
  little extra help. Families receive a new FREE backpack full of supplies
  tailored to their child(ren)'s specific year in school (K-12).
keywords: >-
  Anacortes Schools, Anacortes School District, ready to learn fair, ready to
  learn, school supplies, anacortes school supplies, anacortes school district
  school supplies list, anacortes school district supplies, where can I get free
  school supplies
featuredImage: https://asfmedia.imgix.net/prog-rtl3.jpg
layout: blocks
content_blocks:
  - _bookshop_name: hero
    title: Ready to Learn Fair
    bannerImage: https://asfmedia.imgix.net/page-title-rtlf-2022.jpg
  - _bookshop_name: columns
    num_cols: 2
    columns:
      - name: RTLF Calendar Info
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: slides
            captions:
            width: 650
            height:
            images:
              - title:
                src: https://asfmedia.imgix.net/2025 RTLF Website Graphic v1.png
            divclass:
      - name: Video
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: video
            heading:
            youtubeID: 7H2VB61n-3c
            text: >-
              The annual Ready to Learn Fair helps kids start the year feeling
              proud! ASF and community partners provide FREE back-to-school
              essentials including backpacks and supplies, books, gently used
              clothing, haircuts, new underwear and socks,  resources, and
              information. A team of dedicated volunteers puts the fair on with
              the support of the Anacortes School District and funding from ASF.
            divclass:
  - _bookshop_name: columns
    num_cols: 2
    columns:
      - name: Steps
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: steps
            title: We need your help!
            title_url:
            text: >-
              We would love to have YOUR HELP at this year's event and there are
              plenty of ways you can participate!
            steps:
              - heading: Make a Donation
                heading_url: /giving
                url:
                text: >-
                  For just under $30 each, ASF provides students with a new
                  backpack and an assortment of necessary, grade-level
                  appropriate school supplies. To meet this year's critical
                  needs, over $18,000 and the support of many volunteers will be
                  needed to make the 2025 Ready to Learn Fair a success. We
                  invite you to support this important effort in any way you
                  can.
                action: Donate
                action_url: /giving
              - heading: Be a Sponsor
                heading_url: >-
                  https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2025ReadyToLearnFairSponsorshipForm
                url: >-
                  https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2025ReadyToLearnFairSponsorshipForm
                text: >-
                  Support education while promoting your business. Fill out the
                  <a
                  href="https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2025ReadyToLearnFairSponsorshipForm"
                  target="_blank" rel="noopener">Sponsor Form</a> and contact
                  [execdirector@asfkids.org](mailto:execdirector@asfkids.org)
                  for more information.
                action: Learn More
                action_url: >-
                  https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2025ReadyToLearnFairSponsorshipForm
              - heading: Volunteer
                url: /volunteer
                heading_url: /volunteer
                text: >-
                  The annual Ready to Learn Fair has been an essential service
                  for Anacortes students for over 20 years and it wouldn't be
                  possible without our amazing volunteers! If you'd like to
                  volunteer at this year's Ready to Learn Fair, we'd love to
                  have your help! Click the Volunteer button to complete our
                  Volunteer Interest Form and we'll be in touch!
                action: Volunteer
                action_url: /volunteer
      - name: Slides
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: slides
            width: 650
            height:
            images:
              - src: https://asfmedia.imgix.net/rtl-2023-5.jpg
                title: Donate, Sponsor and Volunteer
              - src: https://asfmedia.imgix.net/bottom carousel-500.jpg
                title: Donate, Sponsor and Volunteer
              - src: https://asfmedia.imgix.net/bottom carousel 3-500.jpg
                title: Donate, Sponsor and Volunteer
              - src: https://asfmedia.imgix.net/bottom carousel 2-500.jpg
                title: Donate, Sponsor and Volunteer
  - _bookshop_name: media-text
    heading: Questions?
    text: >-
      Ready to Learn Fair Committee Chair: Rachel Esposito at
      [rachel@asfkids.org](mailto:rachel@asfkids.org)



      {{< button text="Register HERE beginning August 1st"
      url="https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2025ReadyToLearnFairRegistrationForm"
              >}}
              

              
      {{< button text="Regístrese AQUÍ a partir del 1 de agosto"
      url="https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2025ReadyToLearnFairFormularioDeInscripción"
              >}}
    image_right: false
    images:
      - title: Ready to Learn
        src: https://asfmedia.imgix.net/rtl-2023-5.jpg
      - title: Ready to Learn
        src: https://asfmedia.imgix.net/rtl-2023-6.jpg
      - title: Ready to Learn
        src: https://asfmedia.imgix.net/rtl-4.jpg
      - title: Ready to Learn
        src: https://asfmedia.imgix.net/rtl-6.jpg
    video: {}
    url:
    action:
  - _bookshop_name: marquee
    width: 200
    message:
      heading: >-
        2024 Ready to Learn Fair was made possible in part by our generous
        sponsors.
      text: >-
        Additional thanks to many individuals, organizations and businesses who
        made donations to support this important event.
      url:
    images:
      - name: Strandberg
        url: https://strandbergconstruction.com/
        image: https://asfmedia.imgix.net/Sponsors_strandberg.jpg
      - name: Windermere Real Estate
        url: https://anacortesrealestate.com/
        image: https://asfmedia.imgix.net/2022-cts-windermere-r.jpg
      - name: Anacortes Rotary
        url: https://anacortesrotary.org/
        image: https://asfmedia.imgix.net/Sponsors_anacortes-rotary.jpg
      - name: Kiwanis Sunrisers
        url: https://www.facebook.com/AnacortesKiwanis/
        image: https://asfmedia.imgix.net/Sponsors_kiwanis-sunrisers.jpg
      - name: Kiwanis Noon Kiwanis Club
        url: http://www.anacorteskiwanis.org/
        image: https://asfmedia.imgix.net/Sponsors_kiwanis-noon.jpg
      - name: Samish Indian Nation
        url: https://www.samishtribe.nsn.us/
        image: https://asfmedia.imgix.net/Samish.jpg
      - name: Soroptimist International
        url: https://soroptimistanacortes.org/
        image: https://asfmedia.imgix.net/Soroptimist.jpg
    sectionclass: items-center
    height: 100
    speed: 10000
---
