---
title: Fueling Education Fun Run
aliases:
  - /events/fueling-education-fun-run.html
  - /news/144-successful-fun-run-supports-anacortes-schools-foundation.html
  - /news/155-island-view-elementary-receives-5-000-check.html
  - /funrun
bannerImage: https://asfmedia.imgix.net/2K9A7955.jpg
description: >-
  Every September, ASF partners with Puget Sound Refinery and races into the
  school year at the annual Fueling Education Fun Run.
keywords: >-
  Anacortes Schools, Anacortes School District, anacortes school fundraiser,
  anacortes 5k, anacortes fun run, anacortes shell run, anacortes school
  foundation fundraiser, ASD fundraiser, ASF Fundraiser
featuredImage: https://asfmedia.imgix.net/2K9A8159.jpg
layout: blocks
content_blocks:
  - _bookshop_name: hero
    title: Fueling Education Fun Run
    bannerImage: https://asfmedia.imgix.net/2K9A7955.jpg
  - _bookshop_name: columns
    num_cols: 2
    columns:
      - name: Logo
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: slides
            captions:
            width: 650
            height:
            images:
              - title:
                src: https://asfmedia.imgix.net/2024 FEFR Race Day Checklist.png
            divclass:
      - name: Video
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: video
            heading:
            youtubeID: NxO79bUwEy0
            text:
            divclass:
  - _bookshop_name: columns
    num_cols: 2
    columns:
      - name: Slides
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: slides
            width:
            height:
            images:
              - src: https://asfmedia.imgix.net/IMG_7526.jpg
                title: Fueling Education 5k & Fun Run
              - src: https://asfmedia.imgix.net/IMG_7598.jpg
                title: Fueling Education 5k & Fun Run
              - src: https://asfmedia.imgix.net/2K9A8245.jpg
                title: Fueling Education 5k & Fun Run
              - src: https://asfmedia.imgix.net/2K9A7964.jpg
                title: Fueling Education 5k & Fun Run
      - name: Text
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: content
            title:
            content: >-
              In September, ASF partners with [HF
              Sinclair](https://www.hfsinclair.com/operations/facilities/us/anacortes-wa/)
              and races into the school year at the annual Fueling Education Fun
              Run.


              HF Sinclair puts on this amazing event which brings our community
              together and helps raise close to $60,000 a year to support
              Anacortes students. Thanks to generous donor and community
              members, we have raised almost $500,000 over the past 10 years!


              In previous years, funds raised supported COVID Relief Grants
              (2020), mental health supports for Anacortes students (2021), STEM
              enrichment and CTE programming (2022), 3rd Grade Swim Program
              (2023), High School Science Equipment (2024) and STEM College
              Scholarships.


              We'd love to see your race day pictures! Email them to
              [rachel@asfkids.org](mailto:rachel@asfkids.org) or share them on
              social media with the privacy set to public and use the hashtag
              **\#DINOFunRun2024** to join in on the fun!
  - _bookshop_name: columns
    num_cols: 2
    columns:
      - name: Steps
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: steps
            title: We can't do this without you!
            text: >-
              We would love to have YOUR HELP at this year's event and there are
              plenty of ways you can participate!
            steps:
              - heading: Register for the Race
                text: >-
                  Sign up to participate in the 2.3 mile Race, the .3 mile Kids
                  Race or the Virtual Race. **Register by August 23rd** to
                  guarantee your shirt size!
                action: Sign Up HERE
                url: https://runsignup.com/Race/WA/Anacortes/FuelingEducationFunRun
              - heading: Make a Donation
                text: Help support our event with a financial donation.
                action: Donate
                url: /giving
              - heading: Be a Sponsor
                text: >-
                  Support education while promoting your business. Fill out the
                  [Sponsor
                  Form](https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2024FuelingEducationFunRunSponsorshipForm)
                  and contact
                  [execdirector@asfkids.org](mailto:execdirector@asfkids.org)
                  for more information.
                action: Learn More
                url: >-
                  https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2024FuelingEducationFunRunSponsorshipForm
      - name: Slides
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: slides
            width:
            height: 300
            images:
              - src: https://asfmedia.imgix.net/2022-FEFR-IMG_7444.jpg
                title: Fueling Education 5k & Fun Run
              - src: https://asfmedia.imgix.net/IMG_7610.jpg
                title: Fueling Education 5k & Fun Run
              - src: https://asfmedia.imgix.net/IMG_7613.jpg
                title: Fueling Education 5k & Fun Run
              - src: https://asfmedia.imgix.net/IMG_7615.jpg
                title: Fueling Education 5k & Fun Run
              - src: https://asfmedia.imgix.net/IMG_7617.jpg
                title: Fueling Education 5k & Fun Run
  - _bookshop_name: columns
    num_cols: 2
    columns:
      - name: Slides
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: slides
            width: 600
            height:
            images:
              - src: https://asfmedia.imgix.net/2K9A8347.jpg
                title: Fueling Education 5k & Fun Run
              - src: https://asfmedia.imgix.net/2K9A8212.jpg
                title: Fueling Education 5k & Fun Run
              - src: https://asfmedia.imgix.net/2K9A8045.jpg
                title: Fueling Education 5k & Fun Run
              - src: https://asfmedia.imgix.net/2K9A8441.jpg
                title: Fueling Education 5k & Fun Run
              - src: https://asfmedia.imgix.net/2K9A8087.jpg
                title: Fueling Education 5k & Fun Run
      - name: Steps
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: content
            title:
            content: >-
              Every year, HF Sinclair provides a $5,000 grant to the ASD school
              with the highest percentage of their student body that
              participates in this race. All ASD staff and students are eligible
              to participate in this competition to help their school win
              $5,000! Last year it was a tight competition, and we want to make
              sure we count all eligible racers, so the winning school will be
              announced by ASF on social media the week after the race. Follow
              us on [Facebook](https://www.facebook.com/asfkids) and
              [Instagram](https://www.instagram.com/asf4kids/) to be the first
              to find out who the winner is!


              In addition to the ASD Grant, Race Day Prizes will include:


              * Awards will be given to 1st, 2nd, and 3rd Place – Male & Female
              in the race.

              * An Award will be given to the fastest Teacher in Anacortes
              School District.

              * An Award will be given to the fastest HF Sinclair Anacortes
              Refinery employee.

              * An Award will be given to the fastest person who is a Sponsor of
              the Race

              * Trophies will be given to the fastest child in each age category
              for the kids race.

              * Everyone will receive a finisher medal!

              * There will be raffle prizes randomly given out Day of Race.


              Race Director: James Steller - (360) 293-1766 -
              [James.Steller@HFSinclair.com](mailto:James.Steller@HFSinclair.com).


              Questions about race sponsorship? Contact Treva King via phone
              (360) 488-2343 or email
              [execdirector@asfkids.org](mailto:execdirector@asfkids.org).
  - _bookshop_name: heading
    title: >-
      Thank you to everyone that supported the 10th Anniversary of the Annual
      Fueling Education Fun Run!
    subtitle:
    text: >-
      Check out pictures of this amazing day in our photo album by clicking the
      button below.  Enjoy!
    url: https://karladecampphotography.pixieset.com/hfsinclair/funrun2024/
    action: View Album
    sectionclass: items-center text-center max-w-4xl
  - _bookshop_name: marquee
    width: 200
    message:
      heading: Thank You  this year's Fun Run Sponsors!
      text: >-
        We are so grateful to our many community organizations that sponsor the
        Fueling Education Fun Run each year!
      url: /giving/sponsor/
    images:
      - name: Anacortes School District
        url: https://www.asd103.org/
        image: https://asfmedia.imgix.net/Sponsors_ASD.jpg
      - name: City of Anacortes
        url: https://www.anacorteswa.gov/
        image: https://asfmedia.imgix.net/Sponsors_cityofanacortes.jpg
      - name: HF Sinclair
        url: https://www.hfsinclair.com/operations/facilities/us/anacortes-wa/
        image: https://asfmedia.imgix.net/Sponsors_hfsinclair.png
      - name: WR Grace
        url: https://grace.com/
        image: https://asfmedia.imgix.net/Sponsors_grace.jpg
      - name: Skagit Publishing
        url: https://www.goskagit.com/
        image: https://asfmedia.imgix.net/Sponsors_skagit-publishing.jpg
      - name: Central Welding Supply
        url: https://www.centralwelding.com/
        image: https://asfmedia.imgix.net/Sponsors_central-welding-supply.jpg
      - name: MPe
        url: https://mpenviro.com/
        image: https://asfmedia.imgix.net/Sponsors_mpe.jpg
      - name: OnPoint
        url: https://www.onpoint-us.com/
        image: https://asfmedia.imgix.net/1k-onpoint-color.jpg
      - name: Stronghold Companies
        url: https://www.thestrongholdcompanies.com/
        image: https://asfmedia.imgix.net/1-5k-stronghold-companies.jpg
      - name: Western Refinery
        url: https://wrsweb.com/
        image: https://asfmedia.imgix.net/Sponsors_wrs.jpg
      - name: Matrix Services
        url: https://www.matrixservice.com/
        image: https://asfmedia.imgix.net/matrix-services-inc.png
      - name: Derek Damon Orthodontics
        url: https://www.derekdamonortho.com/
        image: https://asfmedia.imgix.net/2021-cts-damon-r.jpg
      - name: Envirogreen Technologies
        url: https://envirogreentech.com/
        image: https://asfmedia.imgix.net/Sponsors_envirogreen.jpg
      - name: Granite
        url: https://www.graniteconstruction.com
        image: https://asfmedia.imgix.net/1K - Granite Construction (png).jpg
      - name: Lopez Island Creamery
        url: https://lopezislandcreamery.com
        image: https://asfmedia.imgix.net/Lopez 2.jpg
    speed: 20000
    height: 100
---
