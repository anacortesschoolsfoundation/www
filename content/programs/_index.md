---
title: Programs
aliases:
  - /our-programs/mountain-school.html
  - /early-learning-fund.html
  - /our-programs/stem-enrichment.html
  - /our-programs.html
  - /events.html
  - /our-programs/impact-summer-school.html
bannerImage: https://asfmedia.imgix.net/page-title-programs.jpg
description: >-
  ASF supports Anacortes School District through funding, district-wide support,
  and special programs, like the art and STEM enrichment, mental health support,
  early learning scholarships, college scholarships, Impact Summer School, Covid
  relief projects, and Ready to Learn Fair.
keywords: >-
  Anacortes Schools, Anacortes School District, art enrichment, STEM enrichment,
  mental health support, early learning scholarships, college scholarships,
  Impact Summer School, Covid relief projects, Ready to Learn Fair, celebrate
  the season, fueling education
featuredImage: https://asfmedia.imgix.net/books.jpg
layout: blocks
content_blocks:
  - _bookshop_name: hero
    title: Programs
    bannerImage: https://asfmedia.imgix.net/page-title-programs.jpg
  - _bookshop_name: heading
    title: What does ASF do for our Students?
    subtitle:
    text: >-
      ASF's mission is to empower and inspire Anacortes students through
      scholarships and enriched learning opportunities.
    sectionclass: items-center
  - _bookshop_name: video
    heading:
    youtubeID: nDhfWtUSZLI
    text:
    divclass: py-8 max-w-5xl
  - _bookshop_name: text-blocks
    blocks:
      - heading: Funding
        bottom_border: false
        text: >-
          The Anacortes Schools Foundation is a non-profit organization that
          raises private funds to support our schools. Public funding in the
          United States is not enough to guarantee that our students receive the
          very best education.
      - heading: District-wide Support
        bottom_border: false
        text: >-
          Together we support students Kindergarten through 12th Grade through a
          variety of programs including: our annual Ready to Learn Fair, STEM
          enrichment opportunities, Mental Health services, Social Emotional
          Programs, and more!
      - heading: Special Events
        bottom_border: false
        text: >-
          ASF hosts several annual fundraising events including the Fueling
          Education Fun Run each fall and the Celebrate the Season Gala and
          Holiday Fair each December.
  - _bookshop_name: cta-large
    divclass: bg-asfpurple hover:bg-ultraviolet text-center
    linkclass: text-2xl text-white hover:text-white
    text: >-
      ASF helps support the district by asking parents, local businesses and
      community members to help.
    action: Donate Here
    url: /giving
  - _bookshop_name: media-text
    heading: Ready to Learn Fair
    text: >-
      Backpacks and school supplies. Haircuts. Clothing. Books. Community
      Resources. Each August ASF partners with the district and community
      partners to hold the Ready to Learn Fair providing back-to-school
      essentials to over 500 students who need a little extra support.
    image_right: false
    images:
      - title: Ready to Learn
        src: https://asfmedia.imgix.net/rtl-2023-2.jpg
      - title: Ready to Learn
        src: https://asfmedia.imgix.net/rtl-2023-5.jpg
      - title: Ready to Learn
        src: https://asfmedia.imgix.net/rtl-2023-4.jpg
    video: {}
    url: readytolearn
    action: Learn More
  - _bookshop_name: media-text
    heading: Celebrate the Season
    text: >-
      Each December, ASF hosts Celebrate the Season. The Celebrate the Season
      Gala and Auction is ASF's biggest fundraiser of the year.
    image_right: true
    images:
      - title: Celebrate the Season
        src: https://asfmedia.imgix.net/prog-cts.jpg
      - title: Celebrate the Season
        src: https://asfmedia.imgix.net/prog-cts2.jpg
      - title: Celebrate the Season
        src: https://asfmedia.imgix.net/prog-cts3.jpg
    video: {}
    url: celebrate-the-season
    action: Learn More
  - _bookshop_name: media-text
    heading: Fueling Education Fun Run
    subheading: Presented by HF Sinclair
    text: >-
      Each September, ASF kicks off the school year with our annual Fueling
      Education Fun Run. Presenting sponsor HF Sinclair puts on the event which
      brings our community together and helps raise close to $60,000 a year for
      Anacortes students. Funds raised during this annual event support
      enrichment programs, the School Challenge Grant (won by the school with
      the highest percentage of staff and students registered) and STEM
      Scholarships for AHS graduating seniors.
    image_right: false
    images:
      - title: Fueling Education 5k & Fun Run
        src: https://asfmedia.imgix.net/IMG_7526.jpg
      - title: Fueling Education 5k & Fun Run
        src: https://asfmedia.imgix.net/IMG_7598.jpg
      - title: Fueling Education 5k & Fun Run
        src: https://asfmedia.imgix.net/IMG_7503.jpg
    video: {}
    url: run
    action: Learn More
  - _bookshop_name: asf-divider
  - _bookshop_name: card-grid
    singleColumn: false
    cards:
      - heading: Art Enrichment
        text: >-
          ASF awards grants to choir, band, drama, digital media, ceramics, the
          After School Arts Program and more. Each year, we facilitate the
          Brodniak Award and we are excited to partner with the Anacortes Arts
          Festival to suport the Resident Artists in Schools program which
          provides art instruction to all elementary students K-5.
        images:
          - title: Art Enrichment
            src: https://asfmedia.imgix.net/prog-art.jpg
      - heading: STEM Enrichment
        text: >-
          Mountain School, Island Adventures 4th Grade Salish Sea Trip and the
          Spark Museum are all programs funded by the Anacortes Schools
          Foundation. We strongly encourage science exploration!
        images:
          - title: STEM Enrichment
            src: https://asfmedia.imgix.net/prog-stem.jpg
          - title: STEM Enrichment
            src: https://asfmedia.imgix.net/prog-stem2.jpg
          - title: STEM Enrichment
            src: https://asfmedia.imgix.net/prog-stem3.jpg
          - title: STEM Enrichment
            src: https://asfmedia.imgix.net/prog-stem4.jpg
      - heading: Mental Health
        text: >-
          During these unprecedented times, every student needs access to
          social-emotional learning and support. The Anacortes Schools
          Foundation provides support for mental health counselors to help our
          students maintain well-being, resiliency, and hope in their personal
          and academic lives.
        images:
          - title: Mental Health
            src: https://asfmedia.imgix.net/prog-mental.jpg
      - heading: Early Learning
        text: >-
          Early learning is essential to future success! Our preschool
          scholarships ensure that families without resources can access
          preschool opportunities for  their student, so that all children enter
          Kindergarten on an equal footing.
        cta_text: Apply Here
        cta_url: >-
          https://www.cognitoforms.com/AnacortesSchoolsFoundation/AnacortesSchoolsFoundationPreschoolScholarshipApplication
        images:
          - title: Early Learning
            src: https://asfmedia.imgix.net/prog-early.jpg
          - title: Early Learning
            src: https://asfmedia.imgix.net/prog-early2.jpg
      - heading: Scholarships
        text: >-
          ASF has funded over $2 million in scholarships since 1984, helping
          thousands of Anacortes students follow their dreams and pursue
          education after high school.
        images:
          - title: Scholarships
            src: https://asfmedia.imgix.net/prog-scholarship.jpg
      - heading: Impact K-5 Summer School
        text: >-
          We are proud to support IMPACT, a district-sponsored targeted program
          for young learners in need of additional academic and social-emotional
          support during the summer months.
        images:
          - title: Impact K-5 Summer School
            src: https://asfmedia.imgix.net/prog-impact.jpg
      - heading: Ready to Learn Fair
        text: >-
          Our late-summer fair offers every student in need a brand-new backpack
          filled with school supplies as well as toiletries, haircuts, new and
          gently used clothing, and access to a full array of community
          resources to support student and family engagement.
        images:
          - title: Ready to Learn Fair
            src: https://asfmedia.imgix.net/rtl-2023-2.jpg
          - title: Ready to Learn Fair
            src: https://asfmedia.imgix.net/rtl-2023-1.jpg
          - title: Ready to Learn Fair
            src: https://asfmedia.imgix.net/middel carousel-500.jpg
      - heading: Celebrate the Season
        text: >-
          Each December, ASF hosts Celebrate the Season, The Celebrate the
          Season Gala and Auction is ASF's biggest fundraiser of the year.
        images:
          - title: Celebrate the Season
            src: https://asfmedia.imgix.net/prog-cts3.jpg
          - title: Celebrate the Season
            src: https://asfmedia.imgix.net/prog-cts2.jpg
      - heading: Fueling Education Fun Run
        text: >-
          Each September, ASF kicks off the school year with our annual Fueling
          Education Fun Run. Presenting sponsor HF Sinclair puts on the event
          which brings our community together and helps raise close to $60,000 a
          year for Anacortes students.
        images:
          - title: Fueling Education Fun Run
            src: https://asfmedia.imgix.net/prog-run3.jpg
          - title: Fueling Education Fun Run
            src: https://asfmedia.imgix.net/prog-run2.jpg
---
<a target="_blank" rel="noopener nofollow" href="https://www.cognitoforms.com/anacortesschoolsfoundation/_20232024anacortesschoolsfoundationpreschoolscholarshipapplication">...</a>