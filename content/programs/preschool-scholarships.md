---
title: Preschool Scholarships
aliases:
  - /prek
  - /preschool-scholarships.html
bannerImage: https://asfmedia.imgix.net/prog-early2.jpg
description: >-
  Early learning is essential to future success! Our preschool scholarships
  ensure that families without resources can access preschool opportunities for
  their student, so that all children enter Kindergarten on an equal footing.
keywords: >-
  Anacortes Schools, Anacortes School District, ready to learn fair, ready to
  learn, school supplies, anacortes school supplies, anacortes school district
  school supplies list, anacortes school district supplies, where can I get free
  school supplies
featuredImage: https://asfmedia.imgix.net/prog-early.jpg
layout: blocks
content_blocks:
  - _bookshop_name: hero
    title: Preschool Scholarships
    bannerImage: https://asfmedia.imgix.net/prog-early2.jpg
  - _bookshop_name: media-text
    heading: Early Learning
    text: >-
      Early learning is essential to future success! Our preschool scholarships
      ensure that families without resources can access preschool opportunities
      for their student, so that all children enter Kindergarten on an equal
      footing.


      Questions? Email pre-k@asfkids.org
    image_right: false
    images:
      - title: Early Learning
        src: https://asfmedia.imgix.net/prog-early.jpg
    video: {}
    url:
    action:
  - _bookshop_name: cognito
    key: UyfjasXdoU6KNNOYinFa-w
    number: 34
---
