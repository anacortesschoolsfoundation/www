---
title: Be a Sponsor
bannerImage: https://asfmedia.imgix.net/page-title-sponsor-1.jpg
description: >-
  Support education while promotion your business when you sponsor one of our events!

keywords: >-
  donation, anacortes schools foundation donation, other ways to donate, asf donation, community sponsor, sponsor school foundation event

featuredImage: https://asfmedia.imgix.net/cts-sponsor.jpg
layout: blocks
content_blocks:
  - _bookshop_name: hero
    title: Be a Sponsor
    bannerImage: https://asfmedia.imgix.net/page-title-shell.jpg
  - _bookshop_name: heading
    title: Invest in our Community
    subtitle:
    text: Support education while promoting your business.
    sectionclass: text-center
  - _bookshop_name: content-complex
    title: We love our community partners
    content: >
      Investing in public education is essential to the future economic success of individuals, businesses, communities and the nation. Today, public funding from state and property taxes is not enough to meet funding requirements and ensure all our students will thrive. The money ASF raises from parents, businesses and community members helps bridge this funding gap.
      
      
      Excellent schools contribute to strong communities. We are all stakeholders in public education and education is the key to economic growth and prosperity. Your sponsorship will demonstrate to the community your dedication to preparing our students to learn and thrive.
      
      
      Let's work together to assure the continued vitality of our community.
      
      {.text-lg .font-bold}

    sectionclass: py-8 !max-w-5xl
  - _bookshop_name: content
    title: Opportunities to sponsor
    content: >-
      ASF has several sponsorship opportunities, including the [Ready to Learn Fair](/programs/readytolearn) in August, [Fueling Education Fun Run](/programs/run) in September, and [Celebrate the Season](/programs/celebrate-the-season/) in December. Each of these events has different sponsorship levels and corresponding benefits. As a sponsor, you can promote your business to families and our community and take pride in shaping our next generation.
      
      
      To find out more about how your business can sponsor an event, contact [execdirector@asfkids.org](mailto:execdirector@asfkids.org).

    sectionclass: py-8 !max-w-5xl
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: Sponsor Levels
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: card-grid
            singleColumn: true
            cards:
              - heading: Partner
                subheading: $20,000
                text: |-
                  * Logo on annual ASF Newsletter
                  * One content-driven social media post
                  * XXL logo on all event-related marketing materials
                  * Logo on Annual Report
              - heading: Benefactor
                subheading: $5,000
                text: |-
                  * One content-driven social media post
                  * XL logo on all event-related marketing materials
                  * Logo on Annual Report
              - heading: Executive
                subheading: $3,000
                text: |-
                  * Large logo on all event-related marketing materials
                  * Logo on Annual Report
              - heading: Advocate
                subheading: $1,500
                text: |-
                  * Medium logo on all event-related marketing materials
                  * Logo on Annual Report
              - heading: Honor Roll
                subheading: $1,000
                text: |-
                  * Small logo on all event-related marketing materials
                  * Logo on Annual Report
      - name: Programs
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: card-grid
            singleColumn: true
            cards:
              - heading: Ready to Learn Fair
                subheading: >-
                  <a href='https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2024ReadyToLearnFairSponsorshipForm' target='_blank' rel='noopener'>Become a 2024 Sponsor</a>

                images:
                  - title: Ready to Learn Fair
                    src: https://asfmedia.imgix.net/rtlf sponsor block-389.jpg
              - heading: Fueling Education Fun Run
                subheading: >-
                  <a href='https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2024FuelingEducationFunRunSponsorshipForm' target='_blank' rel='noopener'>Become a 2024 Sponsor</a>

                images:
                  - title: Fueling Education Fun Run
                    src: https://asfmedia.imgix.net/HFSinclair_FunRun2023_FinalLogo(2)-389.png
              - heading: Celebrate the Season
                subheading: >-
                  <a href='https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2024CelebrateTheSeasonSponsorshipForm' target='_blank' rel='noopener'>Become a 2024 Sponsor</a>

                images:
                  - title: Celebrate the Season
                    src: https://asfmedia.imgix.net/prog-cts2.jpg
  - _bookshop_name: sponsors-grid
    title: Thank you to our generous sponsors of 2023 Celebrate the Season
    filename: sponsors-cts-2022
    headingclass: text-asfpurple
---

<div class="container"><div class="center max-w-"><h1>Be a Sponsor</h1><p>Support education while promoting your business.</p></div><div><div class="clear"> </div><div class="row justify-content-between align-items-top mt-4"><div class="col-lg-2"> </div><div class="col-lg-8 pb-4"><h2>We love our community partners</h2><p>Investing in public education is essential to the future economic success of individuals, businesses, communities and the nation. Today, public funding from state and property taxes is not enough to meet funding requirements and ensure all our students will thrive. The money ASF raises from parents, businesses and community members helps bridge this funding gap.</p><p>Excellent schools contribute to strong communities. We are all stakeholders in public education and education is the key to economic growth and prosperity. Your sponsorship will demonstrate to the community your dedication to preparing our students to learn and thrive.</p><h4>Let's work together to assure the continued vitality of our community.</h4>&lt;!--<div class="p-4"><a target="_blank" rel="noopener" class="button button-3d button-rounded button-amber" href="https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2022CelebrateTheSeasonSponsorshipForm">Become a 2024 sponsor</a><h4>Or, <a href="https://asfmedia.imgix.net/2021-Celebrate_Sponsorship_Form-v2.pdf">Download the Sponsor Form</a></h4></div>--&gt;</div><div class="col-lg-2"> </div><pre><code>    &lt;/div&gt;
    &lt;div style="margin-bottom: 50px;"&gt;
        &lt;div class="row align-items-center justify-content-between"&gt;
            &lt;div class="col-lg-2"&gt;&amp;nbsp;&lt;/div&gt;
            &lt;div class="col-lg-8 p-3"&gt;
                &lt;h2&gt;Opportunities to sponsor&lt;/h2&gt;
                &lt;p&gt;ASF has several sponsorship opportunities, including the
                    &lt;a name="Ready-to-Learn" href="/programs/readytolearn"&gt; Ready to Learn Fair&lt;/a&gt; in August,
                    &lt;a name="Fun-Run" href="/programs/run"&gt;Fueling Education Fun Run&lt;/a&gt; in September, and
                    &lt;a name="Celebrate-the-Season" href="/programs/celebrate-the-season/"&gt;Celebrate the Season&lt;/a&gt; in December.
                    Each of these events has different sponsorship levels and corresponding benefits. As a sponsor,
                    you can promote your business to families and our community and take pride in shaping our next
                    generation.
                &lt;/p&gt;
                &lt;p&gt;To find out more about how your business can sponsor an event, contact
                    &lt;a name="send-email" href="mailto:execdirector@asfkids.org"&gt;execdirector@asfkids.org&lt;/a&gt;.
                &lt;/p&gt;
            &lt;/div&gt;
            &lt;div class="col-lg-2"&gt;&amp;nbsp;&lt;/div&gt;
</code></pre><pre><code><code>&amp;lt;div class="row justify-content-between align-items-bottom"&amp;gt; &amp;lt;div class="col-lg-5"&amp;gt; &amp;lt;div class="row"&amp;gt; &amp;lt;div class="col-lg-12 p-2"&amp;gt; &amp;lt;div class="card shadow border-light"&amp;gt; &amp;lt;div class="card-body"&amp;gt; &amp;lt;h3 class="color"&amp;gt;Partner&amp;lt;/h3&amp;gt; &amp;lt;h4 class="color"&amp;gt;$20,000&amp;lt;/h4&amp;gt; &amp;lt;ul class="iconlist"&amp;gt; &amp;lt;li&amp;gt;&amp;lt;i class="icon-check-circle color"&amp;gt;&amp;lt;/i&amp;gt;&amp;lt;strong&amp;gt;Logo on annual ASF Newsletter&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;i class="icon-check-circle color"&amp;gt;&amp;lt;/i&amp;gt; One content-driven social media post&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;i class="icon-check-circle color"&amp;gt;&amp;lt;/i&amp;gt; XXL logo on all event-related marketing materials&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;i class="icon-check-circle color"&amp;gt;&amp;lt;/i&amp;gt; Logo on ASF website footer&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;i class="icon-check-circle color"&amp;gt;&amp;lt;/i&amp;gt; Logo on Annual Report&amp;lt;/li&amp;gt;&amp;lt;/strong&amp;gt; &amp;lt;/ul&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;div class="col-lg-12 p-2"&amp;gt; &amp;lt;div class="card shadow border-light"&amp;gt; &amp;lt;div class="card-body"&amp;gt; &amp;lt;h4 class="color"&amp;gt;Benefactor&amp;lt;/h4&amp;gt; &amp;lt;h4&amp;gt;$5,000&amp;lt;/h4&amp;gt; &amp;lt;ul class="iconlist"&amp;gt; &amp;lt;li&amp;gt;&amp;lt;i class="icon-check"&amp;gt;&amp;lt;/i&amp;gt;One content-driven social media post&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;i class="icon-check"&amp;gt;&amp;lt;/i&amp;gt;XL logo on all event-related marketing materials&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;i class="icon-check"&amp;gt;&amp;lt;/i&amp;gt;Logo on ASF website footer&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;i class="icon-check"&amp;gt;&amp;lt;/i&amp;gt;Logo on Annual Report&amp;lt;/li&amp;gt; &amp;lt;/ul&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;div class="col-lg-12 p-2"&amp;gt; &amp;lt;div class="card shadow border-light"&amp;gt; &amp;lt;div class="card-body"&amp;gt; &amp;lt;h4 class="color"&amp;gt;Executive&amp;lt;/h4&amp;gt; &amp;lt;h4&amp;gt;$3,000&amp;lt;/h4&amp;gt; &amp;lt;ul class="iconlist"&amp;gt; &amp;lt;li&amp;gt;&amp;lt;i class="icon-check"&amp;gt;&amp;lt;/i&amp;gt;Large logo on all event-related marketing materials&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;i class="icon-check"&amp;gt;&amp;lt;/i&amp;gt;Logo on ASF website footer&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;i class="icon-check"&amp;gt;&amp;lt;/i&amp;gt;Logo on Annual Report&amp;lt;/li&amp;gt; &amp;lt;/ul&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;div class="col-lg-12 p-2"&amp;gt; &amp;lt;div class="card shadow border-light"&amp;gt; &amp;lt;div class="card-body"&amp;gt; &amp;lt;h4 class="color"&amp;gt;Advocate&amp;lt;/h4&amp;gt; &amp;lt;h4&amp;gt;$1,500&amp;lt;/h4&amp;gt; &amp;lt;ul class="iconlist"&amp;gt; &amp;lt;li&amp;gt;&amp;lt;i class="icon-check"&amp;gt;&amp;lt;/i&amp;gt;Medium logo on all event-related marketing materials&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;i class="icon-check"&amp;gt;&amp;lt;/i&amp;gt;Medium logo on ASF website footer&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;i class="icon-check"&amp;gt;&amp;lt;/i&amp;gt;Logo on Annual Report&amp;lt;/li&amp;gt; &amp;lt;/ul&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;div class="col-lg-12 p-2"&amp;gt; &amp;lt;div class="card shadow border-light"&amp;gt; &amp;lt;div class="card-body"&amp;gt; &amp;lt;h4 class="color"&amp;gt;Honor Roll&amp;lt;/h4&amp;gt; &amp;lt;h4&amp;gt;$1,000&amp;lt;/h4&amp;gt; &amp;lt;ul class="iconlist"&amp;gt; &amp;lt;li&amp;gt;&amp;lt;i class="icon-check"&amp;gt;&amp;lt;/i&amp;gt;Small logo on all event-related marketing materials&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;i class="icon-check"&amp;gt;&amp;lt;/i&amp;gt;Small logo on ASF website footer&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;i class="icon-check"&amp;gt;&amp;lt;/i&amp;gt;Logo on Annual Report&amp;lt;/li&amp;gt; &amp;lt;/ul&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;div class="col-lg-7 p-3"&amp;gt; &amp;lt;div class="row align-items-center justify-content-center"&amp;gt; &amp;lt;div class="col-md-6 col-lg-5 p-3"&amp;gt; &amp;lt;div class="card shadow border-light"&amp;gt; &amp;lt;a name="Ready-to-Learn" href="/programs/readytolearn"&amp;gt; &amp;lt;img src="https://asfmedia.imgix.net/rtlf sponsor block-389.jpg&amp;amp;w=389" width="389" alt="Ready to Learn Fair" class="card-img-top"&amp;gt; &amp;lt;/a&amp;gt; &amp;lt;div class="card-body" style="text-align: center;"&amp;gt; &amp;lt;h4&amp;gt; &amp;lt;a name="Ready-to-Learn" href="/programs/readytolearn"&amp;gt;Ready to Learn Fair&amp;lt;/a&amp;gt; &amp;lt;div class="pt-2" style="text-align: center;"&amp;gt; &amp;lt;a href="https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2023ReadyToLearnFairSponsorshipForm" target="_blank" rel="noopener"&amp;gt;&amp;lt;span style="color: #4b2e62; font-weight: 600;"&amp;gt;Become a 2023 Sponsor&amp;lt;/span&amp;gt;&amp;lt;/a&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/h4&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;div class="col-md-6 col-lg-5 p-3"&amp;gt; &amp;lt;div class="card shadow border-light"&amp;gt; &amp;lt;a name="Fun-Run" href="/programs/run"&amp;gt; &amp;lt;img src="https://asfmedia.imgix.net/HFSinclair_FunRun2023_FinalLogo (2)-389.png&amp;amp;w=389" width="389" alt="Fueling Education Fun Run" class="card-img-top"&amp;gt; &amp;lt;/a&amp;gt; &amp;lt;div class="card-body" style="text-align: center;"&amp;gt; &amp;lt;h4&amp;gt; &amp;lt;a name="Fun-Run" href="/programs/run"&amp;gt;Fueling Education Fun Run&amp;lt;/a&amp;gt; &amp;lt;div class="pt-2" style="text-align: center;"&amp;gt; &amp;lt;a href="https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2023FuelingEducationFunRunSponsorshipForm" target="_blank" rel="noopener"&amp;gt;&amp;lt;span style="color: #4b2e62; font-weight: 600;"&amp;gt;Become a 2023 Sponsor&amp;lt;/span&amp;gt;&amp;lt;/a&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/h4&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;div class="row align-items-center justify-content-center"&amp;gt; &amp;lt;div class="col-md-6 col-lg-5 p-3"&amp;gt; &amp;lt;div class="card shadow border-light"&amp;gt; &amp;lt;a name="Celebrate-the-Season" href="/programs/celebrate-the-season"&amp;gt;&amp;lt;img src="https://asfmedia.imgix.net/prog-cts2.jpg&amp;amp;w=389" width="389" alt="Celebrate the Season" class="card-img-top"&amp;gt; &amp;lt;/a&amp;gt; &amp;lt;div class="card-body" style="text-align: center;"&amp;gt; &amp;lt;h4&amp;gt; &amp;lt;a name="Celebrate-the-Season" href="/programs/celebrate-the-season/"&amp;gt;Celebrate the Season&amp;lt;/a&amp;gt; &amp;lt;!--&amp;lt;div class="pt-2" style="text-align: center;"&amp;gt; &amp;lt;a href="https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2022CelebrateTheSeasonSponsorshipForm" target="_blank" rel="noopener" class="button button-3d button-rounded button-amber"&amp;gt;Become a 2022 Sponsor&amp;lt;/a&amp;gt; &amp;lt;/div&amp;gt;--&amp;gt; &amp;lt;/h4&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; </code></code></pre><p><code>&lt;/div&gt; </code></p></div><div class="col-lg-12 pb-4"><pre><code>&lt;span class="cms-snippet " data-cms-snippet-id="c9c60182-5100-4c64-847c-2ca724bf0758" data-cms-snippet="eyJuYW1lIjoiY3RzLXNwb25zb3JzIiwiYXJndW1lbnRzIjoiIiwiX2hpZGRlbiI6dHJ1ZSwiX3NuaXBwZXRfdHlwZSI6Il9jY19odWdvX3Vua25vd24iLCJfbWV0YSI6eyJuYW1lIjp7ImJvdW5kYXJ5IjoiIiwib3JpZ2luYWxEYXRhIjoiY3RzLXNwb25zb3JzIn0sImFyZ3MiOnsib3JpZ2luYWxEYXRhIjpbXX19fQ=="&gt;keep&lt;/span&gt;
</code></pre></div></div></div>
