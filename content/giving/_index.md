---
title: Make a Donation
aliases:
  - /giving/donations.html
bannerImage: https://asfmedia.imgix.net/page-title-giving.jpg
description: >-
  ASF serves ALL students in the Anacortes School District by raising funds and building partnerships. Learn about the many ways to help.


keywords: >-
  donation, anacortes schools foundation donation, asf donation, make a donation online, ASF sponsor, employer matching gifts, scholarship support, legacy support


featuredImage: https://asfmedia.imgix.net/impact-2.png
layout: blocks
content_blocks:
  - _bookshop_name: hero
    title: Make a Donation
    bannerImage: https://asfmedia.imgix.net/page-title-giving.jpg
  - _bookshop_name: heading
    title: Your gift today is an investment in the future!
    subtitle:
    text: >-
      ASF serves ALL students in the Anacortes School District by raising funds and building partnerships. There are <strong>many ways </strong>to help.


    sectionclass: text-center
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: Donor Box
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: donor-box
      - name: Steps
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: steps
            text: ""
            steps:
              - heading: Make a donation online
                text: >-
                  Your gift will put Anacortes students on a path to lifelong achievement and help them succeed in school and in life. It's the best investment you'll ever make.


              - heading: Be a Sponsor
                text: >-
                  Support education while promoting your business. Contact [president@asfkids.org](mailto:president@asfkids.org) for more information.


                heading_url: sponsor
                action: Learn More
                action_url: sponsor
              - heading: Employer Matching Gifts
                text: >-
                  From time to time, we work with community partners to maximize donations to double or triple the value of your gift! Check to see if your employer has a matching gift program. Our tax ID is 91-1263495.


              - heading: Scholarship Support
                text: >-
                  Contribute to our general scholarship fund or establish a scholarship of your own or in memory of a loved one.


                heading_url: scholarship
                action: Learn More
                action_url: scholarship
              - heading: Legacy Gifts
                text: Leave a legacy of education with planned giving.
                heading_url: legacy
                action: Learn More
                action_url: legacy
  - _bookshop_name: media-text
    heading: Our Pledge to Donors
    text: >
      The Anacortes Schools Foundation promises to demonstrate integrity, transparency and respect when it comes to the stewardship of your gift. We promise that:
      
      
      *   Your gift will be honored and respected
      
      *   Your gift will be acknowledged in a timely and personal manner
      
      *   You will receive a receipt that meets the requirements for federal tax deductions
      
      *   A gift accepted with a restricted purpose will be used for that purpose
      
      *   Whenever feasible, and especially with gifts of endowment, annual balances will be shared with the donor
      
      *   You will be properly recognized, and any public recognition will first be approved by you
      
      *   Gifts will be accounted for using generally accepted accounting principles, which will provide a consistent, timely, and accurate reporting of all gifts into ASF's official financial records


    image_right: true
    images:
      - title: Student Impact
        src: https://asfmedia.imgix.net/impact-2.png
    video: {}
    url:
    action:
  - _bookshop_name: content
    title: You can help!
    content: >
      With your help, we can help make the Anacortes School District a place where each student can reach their full potential and build skills for life. Community donations - from parents, district leaders, teachers, business owners, and volunteers - make it possible to fund materials and experiences that help bring curriculum to life for Anacortes students.
      
      
      Your gift will help fuel intellectual curiosity and prepare students for post-secondary instruction and career-focused exploration. Your philanthropy stretches across the entire district to fund STEM enrichment, mental health and social-emotional programs, preschool scholarships, summer learning and more, providing resources during challenging times and helping remove barriers to learning.


    sectionclass: py-8
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: Image
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: slides
            images:
              - title: STEM in Action
                src: https://asfmedia.imgix.net/STEMRAV6.jpg
      - name: FAQ
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: faq
            title: ASF Giving FAQ
            faqs:
              - q: Don't my taxes pay for the schools?
                a: >
                  Taxes pay only a portion of what Anacortes students need. The State of Washington continues to wrestle with fully funding basic education and almost every district now has a foundation like ASF. ASF raises money from parents and the Anacortes community to elevate and enrich public education for every student in the district.


              - q: Is my gift tax-deductible?
                a: >
                  Yes! Anacortes Schools Foundation is a registered 501(c)(3) nonprofit organization. Gifts are tax-deductible to the extent permitted by law. Our tax ID is **91-1263495**.


              - q: >-
                  Why give to the Foundation if I've already given to my school/PTSA?


                a: >
                  PTSAs are the experts in individual school needs. ASF funds district-wide initiatives such as STEM enrichment, social-emotional programs, the arts and mental health support for students; initiatives that benefit every student, in every classroom. It takes your participation to make this happen.
                  
                  
                  ASF funds great programs at your school to help students become successful in school and life. Without your support, many of these programs wouldn't exist. Giving to your PTA or PTSA and your school is important—it takes both to ensure our students get the best public education possible!


              - q: How much does ASF invest in each program?
                a: >
                  The programs we support and the levels of support they receive vary each year based on the needs of Anacortes students. Every investment is  evaluated during our budgeting process.


              - q: Who decides how funds raised by the Foundation are spent?
                a: >
                  ASF's volunteer board of 16 directors makes those decisions in collaboration with the Executive Director and the Superintendent of the Anacortes School District. Directors represent the entire Anacortes community—parents, businesses and community leaders.
---

<div class="container">
        <div class="row justify-content-between align-items-center">
                <div class="col-lg-5">
                    <h2>Our pledge to Donors:</h2>
                    <p>The Anacortes Schools Foundation promises to demonstrate integrity, transparency and respect when
                        it comes to the stewardship of your gift. We promise that:</p>
                    <ul style="padding-left: 30px;">
                        <li> Your gift will be honored and respected</li>
                        <li> Your gift will be acknowledged in a timely and personal manner</li>
                        <li> You will receive a receipt that meets the requirements for federal tax deductions</li>
                        <li> A gift accepted with a restricted purpose will be used for that purpose</li>
                        <li> Whenever feasible, and especially with gifts of endowment, annual balances will be shared
                            with the donor</li>
                        <li> You will be properly recognized, and any public recognition will first be approved by you
                        </li>
                        <li> Gifts will be accounted for using generally accepted accounting principles, which will
                            provide a consistent, timely, and accurate reporting of all gifts into ASF's official
                            financial records</li>
                    </ul>
                </div>
                <div class="col-lg-6 p-4">
                    <img src="https://asfmedia.imgix.net/impact-2.png?w=612&auto=compress,format"
                        width="612" alt="" class="rounded">
                </div>
        </div>
        <div class="row justify-content-between align-items-center">
            <div class="col-lg-7">
                <h2>You can help!</h2>
                <p>With your help, we can help make the Anacortes School District a place where each student can
                    reach their full potential and build skills for life. Community donations - from parents,
                    district leaders, teachers, business owners, and volunteers - make it possible to fund materials
                    and experiences that help bring curriculum to life for Anacortes students.</p>
                <p>Your gift will help fuel intellectual curiosity and prepare students for post-secondary
                    instruction and career-focused exploration. Your philanthropy stretches across the entire
                    district to fund STEM enrichment, mental health and social-emotional programs, preschool
                    scholarships, summer learning and more, providing resources during challenging times and helping
                    remove barriers to learning.</p>
            </div>
    </div>
        <div class="row justify-content-between align-items-top my-6 col-mb-50">
                <div class="col-lg-4 d-md-none d-lg-block">
                    <img src="https://asfmedia.imgix.net/STEMRAV6.jpg?w=410&auto=compress,format"
                        width="410" alt="" class="rounded">
                </div>
                <div class="col-lg-7">
                    <h2>ASF Giving FAQ</h2>
                    <div class="toggle">
                        <div class="toggle-header">
                            <div class="toggle-icon"> <i class="toggle-closed icon-question-sign"></i> <i
                                    class="toggle-open icon-caret-down1"></i> </div>
                            <div class="toggle-title">
                                <h4> Don't my taxes pay for the schools? </h4>
                            </div>
                        </div>
                        <div class="toggle-content">Taxes pay only a portion of what Anacortes students need. The State
                            of Washington continues to wrestle with fully funding basic education and almost every
                            district now has a foundation like ASF. ASF raises money from parents and the Anacortes
                            community to elevate and enrich public education for every student in the district.
                        </div>
                    </div>
                    <div class="toggle">
                        <div class="toggle-header">
                            <div class="toggle-icon"> <i class="toggle-closed icon-question-sign"></i> <i
                                    class="toggle-open icon-caret-down1"></i> </div>
                            <div class="toggle-title">
                                <h4> Is my gift tax-deductible? </h4>
                            </div>
                        </div>
                        <div class="toggle-content">Yes! Anacortes Schools Foundation is a registered 501(c)(3)
                            nonprofit organization. Gifts are tax-deductible to the extent permitted by law. Our tax ID
                            # is 91-1263495.
                        </div>
                    </div>
                    <div class="toggle">
                        <div class="toggle-header">
                            <div class="toggle-icon"> <i class="toggle-closed icon-question-sign"></i> <i
                                    class="toggle-open icon-caret-down1"></i> </div>
                            <div class="toggle-title">
                                <h4>Why give to the Foundation if I've already given to my school/PTSA? </h4>
                            </div>
                        </div>
                        <div class="toggle-content">
                            <p>PTSAs are the experts in individual school needs. ASF funds district-wide initiatives
                                such as STEM enrichment, social-emotional programs, the arts and mental health support
                                for students; initiatives that benefit every student, in every classroom. It takes your
                                participation to make this happen.</p>
                            <p>ASF funds great programs at your school to help students become successful in school and
                                life. Without your support, many of these programs wouldn't exist. Giving to your PTA or
                                PTSA and your school is important—it takes both to ensure our students get the best
                                public education possible!</p>
                        </div>
                    </div>
                    <div class="toggle">
                        <div class="toggle-header">
                            <div class="toggle-icon"> <i class="toggle-closed icon-question-sign"></i> <i
                                    class="toggle-open icon-caret-down1"></i> </div>
                            <div class="toggle-title">
                                <h4>How much does ASF invest in each program? </h4>
                            </div>
                        </div>
                        <div class="toggle-content">The programs we support and the levels of support they receive vary
                            each year based on the needs of Anacortes students. Every investment is evaluated during our
                            budgeting process.
                        </div>
                    </div>
                    <div class="toggle">
                        <div class="toggle-header">
                            <div class="toggle-icon"> <i class="toggle-closed icon-question-sign"></i> <i
                                    class="toggle-open icon-caret-down1"></i> </div>
                            <div class="toggle-title">
                                <h4> Who decides how funds raised by the Foundation are spent?</h4>
                            </div>
                        </div>
                        <div class="toggle-content">ASF's volunteer board of 16 directors makes those decisions in
                            collaboration with the Executive Director and the Superintendent of the Anacortes School
                            District. Directors represent the entire Anacortes community—parents, businesses and
                            community leaders.
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
