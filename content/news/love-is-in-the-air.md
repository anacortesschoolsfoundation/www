---
title: Love is in the air!
subtitle:
author: Treva King, ASF Executive Director
date: 2024-02-14T00:00:00-08:00
description: >-
  Valentine’s Day signifies that love is in the air. For me, it means baking
  heart-shaped cookies and decorating them with personal messages, then gifting
  them to those I love. But it’s also a reminder that we can show our love in
  many ways. 
keywords: ready to learn fair
featuredImage: https://asfmedia.imgix.net/RTLF Ute.jpg
photoPosition: center
featuredPhotoCredit:
featuredPhotoWebsite:
categories:
  - Ready to Learn Fair
top: true
featured: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            hide_featured: false
            content: >-
              One of my favorite events is our Ready to Learn Fair, where close
              to 100 volunteers show up to make sure students K-12 are prepared
              for the beginning of school. Last year we had 30+ community
              partners join us to give families access to free resources, not to
              mention our own ASF committee members, who put in hours through
              the spring and summer to organize the big event. We give out free
              backpacks and school supplies, free books, clothing, new socks and
              underwear, and offer haircuts and vaccinations for free. We
              wouldn't be able to pull it off without the generosity of our
              loyal donors and sponsors. I can’t think of a more poignant
              reminder that love really is all around us.


              To learn more about how you can be involved with Ready to Learn
              Fair or any of our programs, visit <a
              href="http://www.asfkids.org/?fbclid=IwAR2hhDpVoAM-kkTyz-QqkB8TX0YL6eZo0eghjJ3nto5LBsQontB8OLn5uOo_aem_AV49LkqJIwa1fdu312QuwC-PMGTVGHBDlTzkfJtyCvAXdylToiSBlYTD02kXgpItrZAk1hD2tXFe_lM77ynQSd00"
              target="_blank" rel="noreferrer nofollow
              noopener">www.asfkids.org</a>
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
