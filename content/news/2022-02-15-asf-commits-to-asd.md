---
title: "ASF Commits $424,000 to Anacortes School District"
subtitle: "Anacortes Schools Foundation put $424,000 of funds raised in 2021 to work, committing substantial support for mental health, STEM, early learning, vocational and career technical programs, Impact Summer School and visual and performing arts programs."
author: 
date: 2022-02-15
description: "Anacortes Schools Foundation put $424,000 of funds raised in 2021 to work, committing substantial support for mental health, STEM, early learning, vocational and career technical programs, Impact Summer School and visual and performing arts programs."
keywords: "Anacortes Schools, Anacortes School District, Celebrate the season, anacortes port, anacortes school
fundraiser, anacortes schools foundation, anacortes school foundation fundraiser, anacortes auction, anacortes school
auction, art contest, anacortes art contest, anacortes school art contest, anacortes foundation dinner, anacortes school
celebration, school foundation fundraiser anacortes, anacortes district fundraiser, celebrate anacortes, anacortes
school district, anacortes stem grants, anacortes scholarship fund fundraiser, anacortes celebration, ASD fundraiser,
ASF Fundraiser"
featuredImage: https://asfmedia.imgix.net/ASF-Committed-Funding-2022.png
photoPosition: center
featuredPhotoCredit: 
featuredPhotoWebsite: 
categories:
- Celebrate the Season
featured: true
aliases:
    - /news/asf-commits-to-asd
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            
            content: |
              Each year, ASF raises money to support its mission of empowering all Anacortes students through scholarships and enriched learning opportunities. At the beginning of each calendar year, the ASF Board of Directors meets to commit funding for the following school year. On February 5, 2022, the board put $424,000 of funds raised in 2021 to work, committing substantial support for mental health, STEM, early learning, vocational and career technical programs, Impact Summer School and visual and performing arts programs. In addition to this support of enrichment, wellbeing and current programs, ASF plans to award 138 scholarships valued at approximately $348,000 on June 6, 2022 at the ASF Scholarship Awards Ceremony.
 
              "I'm extremely proud of the role ASF plays in supporting the nearly 2,500 students enrolled in the Anacortes School District," said ASF Executive Director Marta McClintock. "Thanks to generous sponsors and donors, ASF is able to respond year after year to the funding gaps that exist in our schools," said McClintock. In 2008, an economic downturn started an alarming trend where more than half of the country's school systems experienced permanent cuts to local and state funding. With little hope of government support returning, schools turned to alternative funding sources including local levies and foundations like ASF to help bridge the funding gaps in our public school system. 
              
              "All sources of funding are critical for our schools," said Justin Irish, Superintendent Anacortes School District. "We are grateful that our two renewal levies recently passed and that our foundation continues to fill additional gaps. Without funding from ASF, students and educators could be left without critical enrichment in areas including STEM, the arts, early learning, literacy, and the opportunity to pilot innovative programs like Impact Summer Learning. ASF plays a significant role in our schools," said Irish. "In addition, ASF provides critical supports for mental health and is an important link between the district and the community." ASF President Treva King added, "Anacortes continues to step up, and I am so proud to be part of a community that cares so much about students and voluntarily invests in our schools." 
              
              Government funds and levy renewals provide Anacortes students with a very good education. ASF funding helps move that needle to a great education. This, in addition to ASF's robust scholarship program, ensures that Anacortes students receive opportunities for the best education Kindergarten through 12th grade plus the funds and encouragement they need to follow their post-high school dreams. ASF is grateful for the many donors and sponsors who help ensure that Anacortes students have the tools to meet the challenges of the 21st century.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: news-sidebar
---
