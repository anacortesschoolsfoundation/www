---
title: Cause for Celebration
subtitle: >-
  When a whisper turns into a dream which turns into reality, it's cause for
  celebration. This year, Anacortes Schools Foundation celebrates ten years of
  STEM enrichment and we honor the original dreamers who turned that whisper
  into field trips, science equipment and so much more.
author:
date: 2023-05-07T00:00:00Z
description: >-
  When a whisper turns into a dream which turns into reality, it's cause for
  celebration. This year, Anacortes Schools Foundation celebrates ten years of
  STEM enrichment and we honor the original dreamers who turned that whisper
  into field trips, science equipment and so much more.
keywords: >-
  Stem women, anacortes stem teachers, Kim Post, Kristine Stultz, Michelle
  Romag, Joyce Anderson, Maggie Santos, Marilyn Hanesworth, Jody Dylan, Vicki
  Stowe
featuredImage: https://asfmedia.imgix.net/blog-STEMwomen.jpg
photoPosition: center
featuredPhotoCredit: Lisa Kuhnlein
featuredPhotoWebsite: https://www.kuhnlein.com/
categories:
  - STEM
featured: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            hide_featured: true
            content: >
              {{< asf-figure title="ASF STEM Committee" divclass="w-full
              clear-both border p-4 mt-4" figclass="mx-auto" imgclass="w-full"
              caption="ASF STEM Committee (pictured L to R) Kim Post, Kristine
              Stultz, Michelle Romag, Joyce Anderson, Maggie Santos, Marilyn
              Hanesworth, Jody Dylan and Vicki Stowe." capclass=""
              src="https://asfmedia.imgix.net/blog-STEMwomen.jpg" width=800
              link="" >}}


              A decade ago, this STEM committee approached ASF with a plan to
              create and fund enrichment and field trips focused on STEM in
              Anacortes schools. They envisioned local programs based on the
              exceptional environment of Fidalgo Island. To make their vision a
              reality, they created "Celebrate the Season," which laid the
              foundation for an event and programs  that continue today and have
              impacted thousands of students.


              "We knew STEM was being more formally addressed on a national
              level, and we saw startling statistics regarding career
              opportunities that would be unfulfilled due to a lack of

              qualified candidates here in Washington state," Vicki Stowe said.
              "So, we began exploring how to make this national priority a
              mainstay in the Anacortes School District." When the STEM
              Committee first approached ASF for funding, the foundation was
              focused almost exclusively on scholarships. But ASF was open to
              supporting this idea, and they offered the STEM Committee a seat
              on their board. How to fund their dream became the driving force
              of the committee.


              At around the same time, Island Hospital Foundation was
              discontinuing their annual "Celebration of Trees", so the group
              decided to use this holiday opportunity and adapt it to their own
              needs. Thus "Celebrate the Season" was born. "We wanted an annual
              tradition, complete with a fundraising element, but also with a
              day or two of activities that

              could be open to the public," Kristine Stultz said. "And we knew
              what a unique opportunity we had with the Port of Anacortes
              Transit Shed."


              And for ten years, that tradition has continued. What started as a
              whisper — a dinner and holiday fair for STEM enrichment, bringing
              in $30,000 in its first year — has grown into one of the most
              anticipated events of the year in our community. ASF raised more
              than $300,000 at the 2022 Celebrate the Season and plans to put as
              much of that funding as possible into the schools within the year.
              As we enter our final year of Celebrating the Season in the
              Transit Shed, we extend our gratitude to the original STEM
              Committee. All the members of the STEM Committee have continued to
              support ASF and advocate for enrichment and quality education in
              Anacortes. ASF is grateful to this incredible group of visionaries
              who contributed their creativity, time, talent and energy to make
              the event a success for many years and for sparking a trend
              towards hands-on discovery and more engaged STEM study in the
              Anacortes School District.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
