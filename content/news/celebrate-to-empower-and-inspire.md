---
title: Celebrate to empower and Inspire
subtitle:
author: Treva King, ASF Executive Director
date: 2024-11-08T00:00:00-08:00
description: >+
  It’s the most wonderful time of the year. For many. For others, it can be one
  of the hardest. Despite the joy and color that brighten our December days,
  loss of loved ones, or simply the pause in predictable school routines can be
  a struggle for kids. So as we celebrate our gala’s smashing success, we keep
  in mind why we work so hard to raise funds in the first place: to empower and
  inspire students.

keywords: Celebrate to Inspire
featuredImage: https://asfmedia.imgix.net/20241205_124347.jpg
photoPosition: center
featuredPhotoCredit:
featuredPhotoWebsite:
categories:
  - Celebrate the Season
  - ''
top: false
featured: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            hide_featured: false
            content: >-
              This month, our hardworking board takes a breath before gearing up
              to allocate all the money we raised. Our Celebrate the Season
              committee worked tirelessly (see photos) to create a stunning
              event that exceeded our expectations and generated nearly
              $240,000.


              And as always, it is the students who inspire us to do what we do,
              like the CTE classes who created gorgeous charcuterie boards and
              metal sculpture for our auction and produced a professional video
              to highlight our Raise the Paddle or the 7th grade leadership
              students who showed up the day after our event to help us take
              down and clean up. Thank you, students and teachers/advisors.


              <br>ASF wishes you all a peaceful holiday season.


              &nbsp;
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
