---
title: ASF Welcomes New Board Members Chrissy Lemrick and Amanda Cardinale
subtitle:
author:
date: 2025-03-04T00:00:00-08:00
description: >
  ASF welcomed two new members to the board: Chrissy Lemrick and Amanda
  Cardinale.
keywords:
featuredImage: https://asfmedia.imgix.net/Board-Lemrick-Cardinale.jpg
photoPosition: center
featuredPhotoCredit:
featuredPhotoWebsite:
categories:
  - board
aliases:
  -
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News Content
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            hide_featured: true
            content: >-
              &nbsp;


              {{< asf-figure title="ASF Board Member Chrissy Lemrick" alt="ASF
              Board Member Chrissy Lemrick"
              src="https://asfmedia.imgix.net/Chrissy-Lemrick-Facet800px.png"
              width=400 caption="ASF Board Member Chrissy Lemrick" >}}


              ### CHRISSY LEMRICK


              “I love the fact that ASF is for all students, all grades, all
              schools. Giving students opportunities that they might not
              otherwise get is so critical.”


              New board member Chrissy Lemrick first learned about Anacortes
              Schools Foundation when she and her family moved here back in
              2016. New to town, she and her kids were exploring the area and
              happened to walk by the Ready to Learn Fair. Fast forward five
              years and Chrissy herself became a volunteer for the annual back
              to school event.


              Volunteering comes naturally to Lemrick, who has been doing it
              since high school, when she helped out in the kindergarten class
              her mom taught.


              “When I first heard about ASF, I was unaware of the early
              education component, but it's so important,” she said.


              Originally from Georgia, Lemrick, whose father was in the
              military, moved quite a bit growing up. She earned both her
              undergraduate and graduate degrees from Dominican University in
              Virginia.


              Chrissy and husband Chad have three children: 8th grade, 11th
              grade and a freshman at Clemson University. She is a contract
              administrator for Facet, an engineering and environmental science
              firm. When not working, Lemrick enjoys baking, basketball and
              dancing, something she’s done since the age of 3, even teaching
              ballet, tap and modern dance.


              Lemrick says she is excited to learn more about the foundation and
              to help enhance or influence in any way she can. And we’re excited
              to have her. Welcome to the board, Chrissy!


              &nbsp;


              {{< asf-figure title="ASF Board Member Amanda Cardinale" alt="ASF
              Board Member Amanda Cardinale"
              src="https://asfmedia.imgix.net/Cardinale-IMG_5664800.png"
              width=400 caption="ASF Board Member Amanda Cardinale" >}}


              ### AMANDA CARDINALE


              Amanda Cardinale has been giving to our community and to children
              for 28 years as a kindergarten teacher at Mt. Erie Elementary. A
              mom of two AHS alum, Jake, 24 and Ava, 19, Cardinale has been
              involved with our foundation for at least half a decade having
              volunteered with Ready to Learn Fair as our cheerful registration
              greeter and with Celebrate the Season Gala as our
              check-out/Auctria whiz.


              “I am honored and excited to be on the board and to start giving
              back to a community that has done so much for our students and
              schools,” she said. “I am thrilled to be able to work with and
              learn from such a professional and caring group of members.”


              When Cardinale is not in the classroom, she enjoys reading,
              walking in the ACFL and hiking with family, baking, and knitting.
              And now, she can add ASF committee work to her list.


              ASF is the perfect fit for Cardinale as she is passionate about
              education, happy and well-rounded children, opportunity for all,
              and positive community relationships. She says she’s been
              impressed with how ASF can reach its lofty goals and carry out its
              mission in such a caring, organized and supportive manner. We are
              grateful she’s agreed to be a part of our team. Welcome, Amanda!
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
