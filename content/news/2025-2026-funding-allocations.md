---
title: ASF Announces $491K Funding Allocations
subtitle:
author:
date: 2025-02-19T00:00:00-08:00
description: Did you know ASF funds 𝐦𝐨𝐫𝐞 than scholarships?
keywords: >-
  ASF funds allocation, what does ASF fund, what does asf pay for, mental
  health, arts, stem, swimming, early education, ready to learn fair
featuredImage: https://asfmedia.imgix.net/2025-funding.png
photoPosition: center
featuredPhotoCredit:
featuredPhotoWebsite:
categories:
  - Ready-to-Learn-Fair
  - Scholarships
  - Stem
aliases:
  -
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News Content
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            hide_featured: true
            content: >-
              Thanks to the amazing generosity of our community, we are thrilled
              to announce our funding allocations to the Anacortes School
              District for the 2025-2026 year!⁣


              * Mental Health: $293,486

              * Secondary Intervention & Support: $34,850

              * Arts: $47,000

              * STEM: $41,000

              * 3rd Grade Swimming: $17,000

              * Early Education: $43,500

              * Ready to Learn Fair: $15,000


              ### Total: $491,836


              If you’re passionate about one of these areas, please check out <a
              href="http://www.asfkids.org/" target="_blank"
              rel="noopener">www.asfkids.org</a> for information on how you can
              support Anacortes students by donating to or volunteering with
              ASF.


              &nbsp;


              &nbsp;


              {{< asf-figure src="https://asfmedia.imgix.net/2025-funding.png"
              width=800 >}}
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
