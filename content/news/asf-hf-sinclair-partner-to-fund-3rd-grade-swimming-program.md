---
title: ASF  & HF Sinclair Partner to Fund 3rd Grade Swimming Program
subtitle:
author:
date: 2024-03-22T00:00:00-07:00
description: 2023 Fueling Education Fun Run proceeds fund ASD 3rd Grade Swimming Program
keywords: swim, hf sinclair, fun run
featuredImage: https://asfmedia.imgix.net/ASF Swim.jpg
photoPosition: center
featuredPhotoCredit:
featuredPhotoWebsite:
categories:
  - Run
top: false
featured: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            hide_featured: false
            content: >-
              Last week, a few board members and some HF Sinclair employees got
              to witness firsthand the joy and pride of third graders learning
              to swim. Thanks to a generous donation from <a
              href="https://www.facebook.com/HFSPugetSound?__cft__[0]=AZW72qZIr6kUqp1mpYNY4X_95s_1R80-_xS0Xae-otjlcbz-jgHfZRhYAc2Xn94cpMxDkU4W6ft4qoEwevlGmGnkIErbgKCxMPXvKCuCmsXHzKJ9-pML9W5mQUgxHX4WV0U&amp;__tn__=-]K-R"
              target="_blank" rel="noopener">HF Sinclair Puget Sound
              Refinery</a>, as part of a health and safety focus, all third
              grade students in the district received swim lessons this year.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
