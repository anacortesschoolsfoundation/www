---
title: ASF Funds MoNA Enrichment
subtitle:
author:
date: 2024-03-11T00:00:00-07:00
description: >-
  Did you know that through the MoNA Link program (Museum of Northwest Art), our
  elementary students get to visit MoNA in La Conner twice in one school year?
keywords: mona, enrichment, teacher training
featuredImage: https://asfmedia.imgix.net/MoNA 5.jpg
photoPosition: center
featuredPhotoCredit:
featuredPhotoWebsite:
categories:
  -
top: false
featured: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            hide_featured: false
            content: >-
              In addition, an arts educator from the museum visits the classroom
              twice. Teachers attend a three day intensive training in the
              summer and complete two additional trainings at MoNA to help them
              align art lessons with curriculum as well as whatever show happens
              to be on exhibit in the MoNA gallery. **ASF is proud to support
              this enriching opportunity for students and teachers.**
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
