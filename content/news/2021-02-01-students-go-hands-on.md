---
title: Students go hands-on with Science to Go kits
subtitle:
author:
date: 2021-02-01T00:00:00Z
description: >-
  Instead of watching a video of their science teacher explaining such
  experiments, students in kindergarten through fifth grade at the district's
  three  elementary schools are getting to conduct the experiments themselves
  every week thanks to Science to Go kits.
featuredImage: https://asfmedia.imgix.net/science-to-go.jpg
keywords: science to go, marcus knowles, anacortes science kits
photoPosition: top
featuredPhotoCredit:
featuredPhotoWebsite:
categories:
  - programs
  - STEM
aliases:
  - /news/students-go-hands-on
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            hide_featured: false
            content: >
              What does it take to make it rain? Why do trees grow so tall? Why
              do some volcanoes explode?


              Instead of watching a video of their science teacher explaining
              such experiments, students in kindergarten through fifth grade at
              the district's three elementary schools are getting to conduct the
              experiments themselves every week thanks to Science to Go kits. 


              The kits, distributed monthly, contain all that's needed to
              conduct weekly science projects. 


              When elementary science specialist Marcus Knowles, who teaches
              science to all K-5 students district-wide, started the school
              year, his science packets included links to videos and a list of
              supplies students would need to complete assignments. He knew this
              was not necessarily viable for all.


              "I just kept shaking my head thinking no one has all this stuff at
              home," he said. "It could easily be done at school if we were
              meeting in person, but I knew not all parents would have the time,
              or resources, to shop for all the supplies. And even ordering on
              Amazon could take weeks. Time we didn't have."


              Knowles had researched science subscription boxes, much like
              produce boxes that are delivered to homes, and wondered if
              Anacortes could create its own version. His goal was to start
              deliveries in January 2021, but he quickly realized from desperate
              teacher and parent emails that it needed to happen much sooner.


              But how to pay for it? Director of Teaching and Learning Angie
              Miller recommended Anacortes Schools Foundation (ASF).


              > "I've never gotten a faster yes in my life," Knowles said. "I
              nearly cried."

              {.not-prose}


              Gathering materials for one or two classes of 20 is fairly easy,
              but to tackle every student in the district, 950+, requires an
              entire community. Our town is one that comes together to find a
              solution and believes all kids should have access to learning and
              to engaging lessons, he said


              "What's been amazing is that everyone said yes. Instantly. It
              makes us all feel like we're a part of something bigger," he said.


              As much as possible, supplies are purchased locally through
              Bayshore Office Products, Sebo's and Safeway.


              Each kit contains four, some-times five, weekly outlines,
              work-sheets and video links, assess-ments, and printouts of
              science articles (in lieu of attaching a link for students to read
              on-screen), and all the materials necessary for the experiments.


              To fund the 960 kits each month, ASF earmarked $24,000 in total, 
              much of which was donated by Marathon Petroleum as part of ASF's
              annual Celebrate the Season Auction. Shell's Fuel for Education
              Fun Run covered the November kits and Strandberg Custom Homes and
              Design sponsored the December kits.


              Executive Director Marta McClintock said the project is exactly
              what the foundation's mission is all about.


              "These kits allow all students at Fidalgo, Island View and Mt.
              Erie to do the science. They re-move barriers, financial or other,
              and make enriched learning accessible for all," she said. "We're
              thrilled to support this program."


              {{< asf-figure title="ASF Discovery Funding" divclass="w-full
              border p-4" figclass="mx-auto" imgclass="w-full" caption="ASF
              Executive Director Marta McClintock and elementary science
              specialist Marcus Knowles have teamed up this year to provide
              science kits to elementary students to create at home."
              capclass=""
              src="https://asfmedia.imgix.net/mcclintock-knowles.jpg" width=800
              link="" >}}


              Each month, 15 to 18 ASF volunteers mask up and spend several
              hours assembling the kits, which are delivered to the el-ementary
              schools and then made available for families to pick up. Kits are
              prepared for individual grade levels and are based on the Mystery
              Science curriculum.


              Feedback has been overwhelmingly positive from both teachers and
              families. Not only has the consistency of students' work
              increased, but both quality and quantity have improved as well. 


              "Kids who were struggling before we started Science to Go are now
              completing their assign-ments and sharing their results with
              pride," Knowles said.


              Mt. Erie second graders Eli and Ethan Wong love the "mystery" of
              how their experiments will turn out and think it's "cool and fun"
              to actually do the experiments instead of just watching someone
              else have fun on Zoom.


              Their mom Alexis Weise has enjoyed seeing her two sons happily and
              independently apply their science learning to other projects.


              "One week's lesson taught about starting with a proto-type and
              later making design improvements based on initial performance. I
              love how my boys are using this thinking with  other assignments,"
              Weise said. "We're so grateful to Mr. Knowles, the district, ASF
              and the community for generously supplying these kits during this
              year of remote learning."


              Elementary science will be taught virtually through the end of the
              year. Knowles said this un-precedented situation has opened up new
              possibilities for teachers.


              "We're throwing some things out and trying things we'd never have
              dreamed of before," he said.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
