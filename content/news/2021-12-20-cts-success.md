---
title: "2021 Celebrate the Season Recap"
subtitle: "Thank you to all those who helped make our Celebrate the Season Gala a huge success!"
author: 
date: 2021-12-20
description: "Thank you to all those who helped make our Celebrate the Season Gala a huge success!"
keywords: "Anacortes Schools, Anacortes School District, Celebrate the season, anacortes port, anacortes school
fundraiser, anacortes schools foundation, anacortes school foundation fundraiser, anacortes auction, anacortes school
auction, art contest, anacortes art contest, anacortes school art contest, anacortes foundation dinner, anacortes school
celebration, school foundation fundraiser anacortes, anacortes district fundraiser, celebrate anacortes, anacortes
school district, anacortes stem grants, anacortes scholarship fund fundraiser, anacortes celebration, ASD fundraiser,
ASF Fundraiser"
featuredImage: https://asfmedia.imgix.net/DSC_0618.jpg
photoPosition: center
featuredPhotoCredit: 
featuredPhotoWebsite: 
categories:
- Celebrate the Season
featured: true
aliases:
    - /news/2021-cts-success
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            
            content: |
              On behalf of the Anacortes Schools Foundation, I would like to extend sincere and deep gratitude to all those who helped make our Celebrate the Season Gala a huge success. We are thankful for our sponsors and community partners; we are grateful for those who bid online and for those who raised the paddle at our in-person event; and we are so appreciative of all who donated items to our silent and live auctions. 

              Your generosity means we can continue our mission to help students all year long. The $225,000 raised at this one event will support our programs that occur throughout the year: STEM, preschool scholarships, Ready to Learn Fair, the Fueling Education Fun Run, college scholarships, Summer Impact, the Arts, and mental health support in the schools.

              It is an honor and joy to serve as president of this board, but we really could not do any of it without the support of this engaged community. Happy holidays.

              With gratitude,  
              Treva King  
              ASF President
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: news-sidebar
---
