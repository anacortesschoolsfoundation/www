---
title: Behind the Scenes
subtitle:
author: Treva King, ASF Executive Director
date: 2024-10-08T00:00:00-07:00
description: >-
  As we say goodbye to summer and the late sunsets, the carefree freedom from
  routine and the lazy mornings, I’m not sad. 
keywords: Change of Season
featuredImage: https://asfmedia.imgix.net/Fidalgot Oct 2024.jpg
photoPosition: center
featuredPhotoCredit:
featuredPhotoWebsite:
categories:
  - executive message
top: false
featured: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            hide_featured: false
            content: >-
              I welcome the crisp clear mornings and the darkening evenings,
              cozy blankets and mugs of soup. And I love all things pumpkin,
              especially the art in the windows of our elementary classrooms.


              Speaking of art, it’s one of the things I’m most proud of: our
              partnership with Anacortes Arts Festival supporting resident
              artists in the schools. Our students kicked off the year with
              colorful tape resist projects and classroom collaborative art.


              October is really a behind-the-scenes month. Our fundraising and
              marketing team has planned a wonderful event to thank our donors
              and sponsors that includes a firsthand look at life in the high
              school, including the frenetic and festive activities of
              Homecoming. Special thanks to AHS Principal, Dr. Duez for her help
              with organization.


              We will continue thanking our donors throughout the holiday season
              as we lead up to our biggest fundraiser event, Celebrate the
              Season Gala on December 7th. Our committee has been hard at work
              procuring fantastic auction items and creating the design for an
              out-of-this-world stellar experience. Tickets are limited this
              year, so be sure to get them when they go on sale in November.


              Finally, one of the most gratifying moments and really the reason
              ASF exists, is when we can give money directly to the schools.
              This month we are pleased to give $63,875.00 in teacher block
              donations. Last year our board approved $25/student, so in
              addition to funding events like Ready to Learn, experiences like
              Outdoor School and enrichment equipment in classrooms, we also
              fund individual teacher grants.


              This is the kernel of what we do. But we couldn’t do any of it
              without our big-hearted community who continually say yes when we
              ask. Our businesses continue to sponsor us, and individuals
              continue to donate. It’s humbling and awe-inspiring. And we don’t
              take it for granted. If you’d like to contribute to ASF with a
              gift or by volunteering, please visit www.asfkids.org or call me
              at 360-488-2343.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
