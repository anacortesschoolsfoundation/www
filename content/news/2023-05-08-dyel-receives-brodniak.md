---
title: Dyel Receives 2023 Brodniak Cultural Education Award
subtitle: >-
  The Anacortes Schools Foundation is proud to facilitate the annual Walter A.
  Brodniak Cultural Education Award and congratulates 2023 recipient AMS/AHS
  band director Christopher Dyel.
author:
date: 2023-05-08T00:00:00Z
description: >-
  The Anacortes Schools Foundation is proud to facilitate the annual Walter A.
  Brodniak Cultural Education Award and congratulates 2023 recipient AMS/AHS
  band director Christopher Dyel.
keywords: >-
  Christopher Dyel, anacortes band teacher award, Brodniak award, Anacortes
  scholarships
featuredImage: https://asfmedia.imgix.net/blog-dyel.jpg
photoPosition: right-top
featuredPhotoCredit:
featuredPhotoWebsite:
categories:
  - scholarships
featured: false
aliases:
  - /news/2023-Dyel-receives-brodniak
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            hide_featured: false
            content: >
              Parent Shannon Westerman nominated Dyel "because he is a rare type
              of teacher who has both an expertise and talent in his area of
              teaching (music/band), as well as the ability to teach, inspire
              and lead." 


              Fidalgo music teacher and former Brodniak Award recipient Tina
              Martin said the value of music education cannot be understated,
              and thanks to dedicated professionals like Dyel, the district
              continues to excel and create. 


              "Chris gives our students unique opportunities to express
              themselves creatively and builds their confidence in a ripple
              effect across our community," Martin said. "Often he goes from one
              event/performance to another with no down time. Teaching students
              and the community on the importance of music never stops."


              The Walter A. Brodniak Cultural Education Award was created in
              1977, upon Mr. Brodniak's retirement as Superintendent of the
              Anacortes School District, "to foster growth of the cultural arts
              in the District." The award is given each spring to a student,
              faculty member, administrator, school director, community member
              or community organization that has made an outstanding
              contribution to the cultural arts in the Anacortes School District
              during the current school year.


              Dyel, who is completing his 6th year in the district, said he's
              both flattered and deeply grateful to have been selected for the
              Brodniak Award. 


              "It shows how much the Anacortes community appreciates the arts as
              a whole, and specifically in my case, the incomparable quality of
              life that instrumental music education brings to kids," Dyel said.


              Dyel will be recognized at the school board meeting on May 18th at
              6:15 p.m. in the AHS library.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
