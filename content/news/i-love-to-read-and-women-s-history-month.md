---
title: I Love to Read and Women's History Month
subtitle:
author: Treva King, Executive Director
date: 2024-03-06T00:00:00-08:00
description: >-
  In like a lion, and hopefully out like a lamb. March is a tricky month. It’s
  long. It’s still dark, but there is hope. Signs of new life and light tease
  their way into our daily lives. 
keywords: women's history month, I love to read month
featuredImage: https://asfmedia.imgix.net/RTLF Books.jpg
photoPosition: center
featuredPhotoCredit:
featuredPhotoWebsite:
categories:
  -
top: false
featured: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            hide_featured: false
            content: >-
              And in every home across our island, children are logging hours,
              reading to others or being read to as we celebrate I Love to Read
              month. Grade-level reading and early childhood literacy have been
              shown time and again to shape a child’s future, and we are
              grateful to donors who help us boost student literacy. We give out
              hundreds of free, brand-new books each year at our Ready to Learn
              Fair, and our teacher block grants have made it possible for
              teachers to purchase books for their classroom libraries and book
              clubs. Keep calm and read on!<br><br>We also celebrate Women’s
              History this month and are grateful to our female-owned/run
              sponsors:<br><br>Kristine Stultz-Remax<br>Sarah Hill-Playhouse
              Dental<br>Jennifer Bowman<br>Christy Fischer-Fidalgo
              Creative<br>Megan Atterberry-Atterberry Gardens<br><br>The long
              march of March reminds us to take things one day at a time, which
              is also a great way to think about giving. Small, doable
              donations, be they time as a volunteer or dollars to your passion
              program, are just as valuable as large one-time gifts. To find out
              more about how you can help ASF in small and big ways, visit <a
              href="https://l.facebook.com/l.php?u=http%3A%2F%2Fwww.asfkids.org%2F%3Ffbclid%3DIwAR2ogeT4S3Y8s9ZNFFyCmCgeGUtSf_qYECh9YXqIbiABKoPeFNrb6sMUvNU_aem_AV6rKV5y4JDeadz9lURYVhmyEonVDCRjTQsyCPdQ4f09s9Hf-TcuNxpFN_SLxJma879HR3BesveVSXXbRTgde5XC&amp;h=AT3I1i0jTFvghSg0Am23FhHcaw2nZeWGeSr7JqPz0Tm2p6lY2hVxYk9E_m1ZRqn4QSoFdkri4EuErUvTffpKeN6rkXtfoHQ9bHO7tpSXNDpzyERkKgFvWRey6vwSnXTY7Q&amp;__tn__=-UK*F"
              target="_blank" rel="noreferrer nofollow
              noopener">www.asfkids.org</a> or call me (Treva King) at
              360-488-2343. Here’s to light and new growth popping up around us.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
