---
title: ASF Launches New Website!
subtitle:
author:
date: 2021-07-13
description: ASF's goal with this newly designed website is to create a user-friendly browsing experience for everyone who supports Anacortes students.
keywords: ASF website update, new website
featuredImage: https://asfmedia.imgix.net/website-launch.jpg
photoPosition: center
featuredPhotoCredit:
featuredPhotoWebsite:
categories:
featured:
aliases:
  - /news/asf-launches-new-website
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news

            content: |
              News, Scholarships, the Ready to Learn Fair, Fueling Education Fun Run, Celebrate the Season and so much more! All in one place in an exciting new, easy to navigate, user-friendly format! ASF announces the launch of its new website at www.anacortesschoolsfoundation.org. Some of the great new features of the site include:

              * [Important Dates](/scholarships)
              * Expanded [Scholarship](/scholarships) and [Program Information](/programs)
              * [Latest News and Information](/news)
              * [Sponsorship](/giving/sponsor) and [Giving Opportunities](/giving)
              * Information about the [Ready to Learn Fair](/programs/readytolearn), [Shell Fun Run](/programs/run) and [Celebrate the Season](/programs/celebrate-the-season)
              * [Volunteer Opportunities](/volunteer)

              The website has been created by Christy Fischer, an Anacortes alumni and parent of two current Anacortes students. "Christy has done an amazing job and her personal connection to the Anacortes schools really shows," said Executive Director Marta McClintock. "I love everything about the update, but I am especially excited about all the photos and news articles showcasing all that ASF does." ASF's goal with this newly designed website is to create a user-friendly browsing experience for everyone who supports Anacortes students. We hope you enjoy our new design!
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
