---
title: Welcome to ASF's New Executive Director
subtitle: Treva King accepts the role of ASF ED
author:
date: 2024-01-31T00:00:00-08:00
description: >-
  Anacortes Schools Foundation is excited to announce Treva King as our new
  Executive Director! Please see below for our press release and join us in
  congratulating Treva on her new position.
keywords: executive director
featuredImage: https://asfmedia.imgix.net/Treva+Headshot+%281%29.jpg
photoPosition: center
featuredPhotoCredit: Lisa Kuhnlein
featuredPhotoWebsite:
categories:
  -
top: false
featured: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            hide_featured: false
            content: >-
              NEWS RELEASE


              Treva King Named Executive Director, Anacortes Schools Foundation


              ANACORTES, WA – Anacortes Schools Foundation is pleased to
              announce Treva King as its new Executive Director. King replaces
              Marta McClintock, who held the position for the past 6 years and
              recently moved to her new position as ED for Island Health
              Foundation.


              King completed her three-year tenure as ASF Board President in
              December 2023 and has been a member of the ASF Board of Directors
              for the last eight years. In addition to her board
              responsibilities, King has chaired both the ASF Scholarship
              Committee and Ready To Learn Fair Committee for five years each.


              King brings vast non-profit and board experience to her new role
              as well as a background in education. She earned a BA from the
              University of Washington and an MEd from Western Washington
              University. King taught English at Anacortes High School for eight
              years before taking time off to raise her two sons. She then
              returned to the classroom as a preschool teacher for another eight
              years.


              King looks forward to hitting the ground running.


              “I’m excited to continue growing the work of ASF so that we can
              get as many opportunities and resources into our schools as
              possible,” King said. “My two passions are kids and community, and
              the Foundation marries these two perfectly. Setting students up
              for success with enrichment and scholarships only strengthens a
              community.”


              ASF Board President Brian Adams said King brings a wealth of
              experience to the position and has a heart for helping students.


              “She knows our work, she believes in what we do, and she has a
              history of deep connections to our community members,” Adams said.


              To learn more about how you can contribute your time, talents,
              and/or treasure, call Treva at 360-488-2343 or email
              [execdirector@asfkids.org](mailto:execdirector@asfkids.org).
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
