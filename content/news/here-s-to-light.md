---
title: Here's to Light
subtitle:
author: Treva King, ASF Executive Director
date: 2024-11-08T00:00:00-08:00
description: >-
  Hello darkness, my old friend. You inspire us to find the light. While most of
  us spend this month thinking about gratitude, I like to think that ASF does
  this every month. 
keywords: Gratitude and Inspiration
featuredImage: https://asfmedia.imgix.net/IMG_5405.jpg
photoPosition: center
featuredPhotoCredit:
featuredPhotoWebsite:
categories:
  - Celebrate the Season
  - ''
top: true
featured: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            hide_featured: false
            content: >-
              We are grateful for our donors and sponsors, certainly. But we are
              also thankful for teachers, paraeducators, administrators, coaches
              and advisors who spend their days teaching, nurturing and
              inspiring kids.


              As the darkness creeps in, we are throwing glitter in the air,
              gathering all the shine and preparing to Celebrate the Season
              under the stars. Our team has been busy organizing all of the
              incredible, and I mean unbelievable, auction items that have been
              generously donated for our auctions, either online or in the room
              the night of the gala.


              Like the multitude of stars in our sky, the needs of the district
              are becoming nearly countless. This is why we decided to direct
              our Raise the Paddle to “Where Most Needed: Everywhere”. Keeping
              it undesignated will allow us to continue to support more programs
              and experiences across the district.


              When we are challenged by darkness, it makes us look for light in
              places we may not have thought about before. ASF is always seeking
              to make learning a lasting experience. We want our students to
              reach for the stars, and ASF’s mission is to help by empowering
              and inspiring them through scholarships and enrichment
              opportunities: STEM, CTE, mental health and the arts.


              Make a wish upon a star and join us December 7th to Celebrate the
              Season. If you are unable to attend, you can still support by
              making a direct donation or bidding on our amazing online auction
              items. Visit <a
              href="https://l.facebook.com/l.php?u=http%3A%2F%2Fwww.anacortesschoolsfoundation.org%2F%3Ffbclid%3DIwZXh0bgNhZW0CMTAAAR1XXmmwt5E-BKDYC7qZ2AUlBESZ6k09Z6ay4rWDSIYEts8hfcIqWDuwwgs_aem_yJ9x3TzJszUGw0_qWnkbfA&amp;h=AT2_Q80NXkfWfX7BuKGP67XxylGrjKeMsQYGMQUsFprbjM3_PUwEVt0sAHO0U0p1KvF3m_3u0-nGBTGqTqtHpu6WAh5uPXxVm1nmNn5UXJCv-waFw6v3EZrDjBd0IJr6QzA86ig&amp;__tn__=-UK-R&amp;c[0]=AT0Bb6YrNkYsCre1LQ1u65wb3SsUPaa7MUM5jbEfrt6zy4VdTjPU7Q-fqNLTGhE00IgLqgDyPyi-51nzTVSMVxNMG_ZePpDXm--B4fM5sQqnI3lO8aeFoanHGAe-t1ONUWyT0ZEBWzwV-p9PJ2Cs1BCuUJ3vvFKhxgnV3QRcxjGX"
              target="_blank" rel="noreferrer nofollow
              noopener">www.anacortesschoolsfoundation.org</a> for more
              information. Here’s to light!
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
