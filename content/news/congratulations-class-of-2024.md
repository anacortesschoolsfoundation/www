---
title: Congratulations, Class of 2024!
subtitle:
author: Treva King, ASF Executive Director
date: 2024-06-05T00:00:00-07:00
description: >-
  The joy and pride on stage the night of the ceremony were palpable. As was the
  hope. 
keywords: scholarships
featuredImage: https://asfmedia.imgix.net/447495679_788290936765206_201579127396212066_n.jpg
photoPosition: center
featuredPhotoCredit: EJ Harris
featuredPhotoWebsite:
categories:
  - Scholarships
top: true
featured: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            hide_featured: false
            content: >-
              We're going to be okay. In fact, we will be great. Our future is
              bright. I know this because at the beginning of this month, ASF
              awarded 149 scholarships, totaling close to $380,000, some to
              graduating seniors and some to current college students.


              These students are chasing dreams, working toward focused goals
              and taking advantage of opportunities they might not otherwise
              have. ASF is grateful to our generous donors who have created
              scholarships for all kinds of students. We have memorial
              scholarships honoring beloved family and friends who have passed;
              we have a first generation scholarship to help students who are
              the first in their families to go on to higher learning; we have
              CTE/vocational scholarships; and we have both need-based and
              merit-based scholarships. To read about this year's recipients or
              the scholarships on our webpage, click on "Scholarships" and
              choose "Browse all available".


              The joy and pride on stage the night of the ceremony were
              palpable. As was the hope. Congratulations, Class of 2024!


              Our board takes a collective breath this month as we gear up for
              our next big events. The Ready to Learn Fair committee has been
              hard at work since March preparing to make our August 17th event
              the best ever. And the HF Sinclair Fueling Education Fun Run
              committee began its work just this week. Don't miss the annual fun
              run that kicks off the school year and raises $30-40,000 for STEM
              enrichment in the schools and for scholarships.


              We wish you a wonderful summer of relaxation and fun.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
