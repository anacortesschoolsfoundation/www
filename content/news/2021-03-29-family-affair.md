---
title: A Family Affair
subtitle: Supporting Anacortes Schools runs in the family
author:
date: 2021-03-29T00:00:00Z
description: >-
  Supporting schools for one Anacortes family is truly in the blood, three
  generations having attended Anacortes schools and graduated from Anacortes
  High School, with a fourth on its way currently at Mt. Erie Elementary.
keywords: >-
  marcus knowles, Science to go, Linda Kleven-braski, Lara Knowles, marathon
  grant, marathon refinery, marathon
featuredImage: https://asfmedia.imgix.net/moore.jpg
photoPosition: center
featuredPhotoCredit: Lisa Kuhnlein
featuredPhotoWebsite: https://www.kuhnlein.com/
categories:
  - stem
  - ready to learn fair
featured:
aliases:
  - /news/family-affair
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            hide_featured: false
            content: >
              Ruth Kleven, a twelve-year-old girl, arrived from Norway in 1924.
              She attended the one-room schoolhouse on Campbell Lake and
              eventually taught at the Nelson School  from 1939-41, her career
              being interrupted by the War. Kleven eventually returned to the
              classroom in 1954 and finished out her career subbing for another
              20 years.


              Now her great grandchildren, Thomas and Mira Knowles, 5th and 2nd
              grade, have already begun supporting Anacortes Schools Foundation
              by helping parents Lara and Marcus Knowles prep and assemble
              hundreds of Science-to-Go kits each month.


              Grandmother Linda Kleven-Braski, class of 1961 was a bus driver in
              our district for 25 years. She now helps out watching the
              grandkids while Lara and Marcus teach  full-time. Lara, in her 5th
              year as a Mt. Erie kindergarten teacher, also attended Mt. Erie
              and graduated from Anacortes High School in 1998. 


              Husband Marcus taught kindergarten at Whitney for three years
              before becoming the elementary science specialist this year.
              Tasked with teaching remote science to  every elementary student
              in the district, Knowles knew he had to get materials into the
              hands of kids who could not otherwise conduct the class
              experiments.


              ### Enter Anacortes Schools Foundation

              Through a generous $20,000 grant from Marathon Petroleum and gifts
              from Strandberg Construction and Puget Sound Refinery, plus
              individual donations, ASF has been able to supply 975 kits each
              month, making science a reality in the homes of all students from
              Fidalgo, Mt. Erie, and Island View.


              Marcus Knowles has supported ASF as well. He taught Lego/Robotics
              for the summer Impact program, a program created to decrease
              achievement and opportunity gaps in K-3 students. He has also been
              seen on stage with district band Detention Hall at ASFs largest
              annual fundraiser Celebrate the Season.


              Both Lara and Marcus recognize the benefits their own students
              have reaped from ASF events like Ready to Learn Fair and are
              grateful that the foundation helps kids in such a wide variety of
              ways.


              As for 5th grader Thomas, he's looking forward, not back.


              "I missed the Salish Sea Adventures and Mt. School, but I'm
              excited about KidWind next year in 6th grade."


              Thank you, Braski-Knowles family, for supporting ASF and the
              district for decades.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
