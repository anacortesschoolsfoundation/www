---
title: Celebrate our Career and Technical Education Program
subtitle:
author: Treva King
date: 2025-02-10T00:00:00-08:00
description: >-
  There’s nothing like a few inches of snow to bring out the child in all of us
  PNWers. The wonder and unadulterated joy is contagious. It’s the same kind of
  wonder we seek to share with students through enrichment, experiences that
  will stretch their minds and also find a lasting place in their memories and
  hearts. When a student has hands-on experience, the learning increases
  exponentially.
keywords: >-
  CTE, STEM, careers, robotics, welding, woodworking, sports medicine,
  architectural drawing, coding, robotics
featuredImage: https://asfmedia.imgix.net/hoco-photos527-1500.jpg
photoPosition: center
featuredPhotoCredit: AHS Digital Media Class
featuredPhotoWebsite:
categories:
  - executive message
aliases:
  -
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News Content
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: slides
            captions: false
            width:
            height:
            images:
              - title: Photo by Anacortes High School Digital Media Class
                src: https://asfmedia.imgix.net/CTE3-13-1500.jpg
              - title: Photo by Anacortes High School Digital Media Class
                src: https://asfmedia.imgix.net/hoco-photos527-1500.jpg
              - title: Photo by Anacortes High School Digital Media Class
                src: https://asfmedia.imgix.net/hoco2101-1500.jpg
              - title: Photo by Anacortes High School Digital Media Class
                src: https://asfmedia.imgix.net/TILY0209-1500-edit.jpg
              - title: Photo by Anacortes High School Digital Media Class
                src: https://asfmedia.imgix.net/TILY4196-1500.jpg
              - title: Photo by Anacortes High School Digital Media Class
                src: https://asfmedia.imgix.net/TILY0209-1500.jpg
            divclass:
          - _bookshop_name: content-complex
            title:
            content: '#### Photos by AHS Digital Media Class'
            sectionclass:
          - _bookshop_name: content-news
            hide_featured: false
            content: >-
              In this month of love, we celebrate our Career and Technical
              Education program and the classes that give students unique,
              challenging and fun hands-on experiences. Whether it’s welding,
              woodworking, sports medicine, architectural drawing, or coding and
              Robotics, our students are exposed to some amazing learning. Thank
              you to the skilled teachers who inspire students to learn about
              vocational opportunities. And a huge shout out to our sponsors,
              many of whom designate their donations to CTE programs.


              If there is a program you are passionate about, be it CTE or the
              performing arts, early learning or STEM, please consider donating.
              We will make sure your contribution supports the learning that
              means the most to you. And if you’re in the stage of planning for
              the future and want to make an even deeper impact, you might want
              to think about starting an endowment. This lasting investment will
              continue to support students through time without relying on
              annual donations.


              Visit
              [www.asfkids.org](https://www.anacortesschoolsfoundation.org/giving/
              "Make a Donation") for information on how to donate or join our
              [legacy
              society](https://www.anacortesschoolsfoundation.org/giving/legacy/
              "Legacy Society"). Please also feel free to call Executive
              Director Treva King at 360-488-2343 to learn more.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
