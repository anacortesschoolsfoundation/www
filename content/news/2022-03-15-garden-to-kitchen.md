---
title: "ASF is Proud to Support STEM Initatives at AMS"
subtitle: "ASF is proud to support STEM initiatives including the Garden to Kitchen Classes at Anacortes Middle School. This year we partnered with Transition Fidalgo in the building of a Greenhouse at AMS. Join us on June 16 for the ribbon cutting and celebration!"
author:
date: 2022-03-15
description: "ASF is proud to support STEM initiatives including the Garden to Kitchen Classes at Anacortes Middle School. This year we partnered with Transition Fidalgo in the building of a Greenhouse at AMS. Join us on June 16 for the ribbon cutting and celebration!"
keywords: "AMS, garden to kitchen, stem, stem anacortes, ams stem, kichen stem"
featuredImage: https://asfmedia.imgix.net/greenhouse.png
photoPosition: top
featuredPhotoCredit:
featuredPhotoWebsite:
categories:
  - STEM
featured: true
aliases:
  - /news/2022-Garden-to-Kitchen
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news

            content: |
              This year we partnered with Transition Fidalgo https://www.transitionfidalgo.org/ in the building of a Greenhouse at AMS.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
