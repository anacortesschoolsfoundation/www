---
title: Retreat? No, more like A TREAT!
subtitle:
author: Treva King, Executive Director
date: 2025-01-15T00:00:00-08:00
description: >-
  January kicked off with our Annual reTREAT– a delight to pull up our sleeves
  and spend a day re-evaluating our committee work, doing some self-reflection
  on why we serve as board members, envisioning how we can best accomplish our
  mission this coming year, and the most rewarding part: allocating our funds
  according to donors’ requests and the district’s priorities.
keywords: retreat, budget, funding, scholarships, stem
featuredImage: https://asfmedia.imgix.net/IMG_5595.jpeg
photoPosition: center
featuredPhotoCredit:
featuredPhotoWebsite:
categories:
  - Executive Message
aliases:
  -
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News Content
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            hide_featured: false
            content: >-
              We enter the new year with a theme of curiosity–seeking to keep
              kids curious throughout their learning by funding programs and
              opportunities that engage their sense of wonder *and* reminding
              ourselves as board members to remain curious, to listen, to pay
              attention.


              Despite the budget challenges of our district, we are thrilled to
              be funding so many experiences that will continue to inspire and
              empower students at all grade levels. ASF works year round to fund
              programs and materials that round out our students’ learning:
              field trips, science equipment, art and poetry in the classrooms,
              mental health support positions and more.


              Huge thank you to the Salvation Army for letting us use their
              beautiful space for this year’s retreat. Did you know that
              nonprofits can reserve the meeting room (with gorgeous views) for
              a reduced rate? And the first time is free! \#salvationarmy.


              Stay tuned for a list of our allocations and, as always, if you
              have questions or would like to donate to a specific program or to
              “Where Most Needed”, please visit
              [asfkids.org](https://www.anacortesschoolsfoundation.org/giving/
              "Make a Donation to Anacortes Schools Foundation"), email
              [execdirector@asfkids.org](mailto:execdirector@asfkids.org) or
              call 360-488-2343 any time.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
