---
title: Record-Breaking ASF Scholarship Celebration!
subtitle:
author:
date: 2021-06-15
description: On June 1, 2021, the Anacortes Schools Foundation awarded more than $315,000 in scholarships to over 120 students for the 2021-2022 academic year.
keywords: 2021 scholarship
featuredImage: https://asfmedia.imgix.net/2021-schol-2.jpg
photoPosition: center
featuredPhotoCredit:
featuredPhotoWebsite:
categories:
  - Scholarships
featured:
aliases:
  - /news/2021-scholarship-celebration
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news

            content: |
              The ASF Scholarship Celebration event was held in-person at Rice field and was hosted by ASD Superintendent Dr. Justin Irish and ASF Executive Director Marta McClintock. The celebration marks the highest number of awards and the largest amount given in the 37-year history of the foundation.

              "I'm so proud of all these incredible students" said ASF President Treva King who helped hand out scholarship certificates alongside two AHS counselors, Joyce Binchus and Ashlee Poggie. "I love reading about their experiences and hearing about their dreams for the future" King added, "I am also honored to be a part of an organization that proudly supports students going on to 4-year universities, 2-year colleges and vocational learning."

              Some of the places these students are headed: Gonzaga, UW, Northern Arizona University, Western Washington University, Everett Community College, WSU, Baylor, Skagit Valley Community College, Boise State, Auburn, Bellingham Technical College, Scripps College, George Fox, Arizona State, Cal Poly San Luis Obispo, Whitman, University of Hawaii, Evergreen Beauty School, University of Victoria, UC Santa Cruz, Yale, BYU and Grand Canyon University.

              {{< asf-figure title="Anacortes Schools Foundation ceremony" divclass="w-full" figclass="mx-auto" imgclass="w-full" caption="" capclass="" src="https://asfmedia.imgix.net/2021-schol-9.jpg" width=600 link="" >}}

              And, a few things they plan to study: English, political science, pre-med, urban planning, marine science, video game design, physical therapy, computer engineering, nursing, journalism, construction management, education, business, psychology, bioengineering, healthcare, waste treatment, physics, graphic design, aerospace engineering, environmental studies, library science, architecture, global affairs, nutrition, cosmetology, metal fabrication, pre-law, sociology and astrophysics.

              > "The Anacortes Schools Foundation Scholarship Program is stronger today than ever," said ASF Scholarship Chair Kim Post. "We have amazing, generous donors, a dedicated selection committee and of course, incredible students!" ASF has been awarding scholarships since 1984 and has a proven track record of tirelessly working to accomplish ASF's mission of empowering all Anacortes students through scholarships and enriched learning opportunities."
              {.not-prose}

              {{< asf-figure title="Anacortes Schools Foundation ceremony" divclass="w-full" figclass="mx-auto" imgclass="w-full" caption="" capclass="" src="https://asfmedia.imgix.net/2021-schol-12.jpg" width=600 link="" >}}

              Thank you notes have already begun arriving from recipients who are excited to be moving forward after an especially challenging year. "Thank you for this gift which will help me focus on the most important aspect of college – learning - without the burden of student loans," said one senior. "I am eternally grateful for your kind-hearted and generous spirit and one day I hope to change someone's life in the way you have changed mine."

              Academic, memorial and need-based scholarships reflect the diversity of Anacortes students: young people from all walks of life who are destined for a variety of professional fields. Scholarship recipients are selected based on applications and individual scholarship criteria.

              {{< asf-figure title="Bird's-eye-view of the 2021 Scholarship Ceremony" divclass="w-full" figclass="mx-auto" imgclass="w-full" caption="" capclass="" src="https://asfmedia.imgix.net/2021-schol-sky2.jpg" width=600 link="" >}}
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
