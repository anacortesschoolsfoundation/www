---
title: Fueling Education and Filling the Gaps
subtitle:
author: Treva King, ASF Executive Director
date: 2024-09-06T00:00:00-07:00
description: >-
  It’s tempting to make this month’s blog all about back-to-school: sharpened
  pencils, school bells announcing transitions, the whoosh of school buses
  braking around town.  
keywords: Fueling Education Fun Run
featuredImage: https://asfmedia.imgix.net/image1.jpeg
photoPosition: center
featuredPhotoCredit:
featuredPhotoWebsite:
categories:
  - Run
top: true
featured: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            hide_featured: false
            content: >-
              And while I love all things autumnal, September also reminds me of
              the wants and needs of our district. One of our main goals is to
              fill the gaps between what the state provides and what our
              district is able to fund: all those enrichment opportunities that
              many might argue are actually a need, not a want.


              This month we will once again partner with HF Sinclair to "fuel
              education."We will be raising money for much needed science
              materials at the high school. The equipment will be used to
              supplement various science classes and give students an enriched,
              hands-on experience . Could they learn the science without the new
              equipment? Probably. Will their lessons be more engaging and will
              students be able to participate at a deeper level? Undoubtedly.


              On September 15, hundreds of students, teachers and families will
              join us for our 10th annual fun run at Washington Park. I hope to
              see you there. The money raised will not only fund science
              enrichment materials but also CTE and STEM scholarships. Plus, the
              school with the highest percentage of participation wins $5000 for
              their school! We are beyond excited to celebrate a decade of
              “running for funding” and are very grateful for our sponsors.


              If you haven’t already signed up, please consider joining us:
              [https://runsignup.com/Race/WA/Anacortes/FuelingEducationFunRun]()


              If you are unable to be there in person, you can participate
              virtually, or simply send us a donation designated to our Fun Run.


              Settling into the back-to-school routine is a comfort of sorts.
              Soccer and football fields are populated with renewed energy.
              Playgrounds emit the soundtrack of laughter and childhood squeals.
              And it all points to what is most important to ASF: supporting
              students. Thank you for helping us do just that. If you’d like to
              get more involved, either as a volunteer or as a donor, please
              visit <a
              href="http://www.asfkids.org/?fbclid=IwZXh0bgNhZW0CMTAAAR2puY16W4N6IxrvciECJxD0bQ8RuXjY3o592WHM2agwvO6BX0zAZr3331Q_aem_pT7EgJOLkSvf9KeIHLq86g"
              target="_blank" rel="noreferrer nofollow
              noopener">www.asfkids.org</a> or contact me at 360-488-2343.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
