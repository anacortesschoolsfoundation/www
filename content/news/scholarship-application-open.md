---
title: Scholarship Application OPEN!
subtitle:
author: Treva King, ASF Executive Director
date: 2024-04-05T00:00:00-07:00
description: >-
  Our scholarship application is available now and will remain open until April
  27th at noon. 
keywords: scholarships
featuredImage: https://asfmedia.imgix.net/Barrett Scholarship photo.jpg
photoPosition: center
featuredPhotoCredit: EJ Harris
featuredPhotoWebsite:
categories:
  - Scholarships
top: false
featured: false
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            hide_featured: false
            content: >-
              Last week I had the pleasure of speaking to the Class of 2024
              about the $350,000 in scholarships that ASF will award this
              spring. We awarded our first scholarship, the Cressa and Arnold
              Houle Scholarship in 1984 and ever since, we’ve been helping
              students realize their dreams of higher education, whether that be
              a four or two year university or a trade school program.<br><br>As
              I walked around the room of 150+ seniors, I was inspired, knowing
              that so many of them will embark on new learning adventures when
              they graduate. I also sympathized with them; senior year can be
              scary. Whether real or imagined, the expectations seniors feel at
              the end of their public school career can be daunting.<br><br>This
              community is more generous and supportive than towns twice our
              size. Individual donors, corporate donors and nonprofits give
              thousands of dollars in scholarships to support every kind of
              student entering every kind of program. It’s awe-inspiring,
              really. ASF is deeply grateful for our scholarship partners, to
              all who contribute in memory or honor of a loved one, to those who
              wish to give a leg up to the resilient students who’ve had to
              fight for everything, and to those who wish to reward hard work,
              we say thank you.<br><br>Our scholarship application is available
              now and will remain open until April 27th at noon. For more
              information, email scholarships@asfkids.org or call me at
              360-488-2343.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
