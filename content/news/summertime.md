---
title: Summertime!
subtitle:
author: Treva King, ASF Executive Director
date: 2024-07-08T00:00:00-07:00
description: >-
  For many of us summer is the payoff. It’s the treat after a year of hard work
  and studying. And it’s filled with adventure, travel, relaxation and fun. 
keywords: ready to learn fair
featuredImage: https://asfmedia.imgix.net/449784297_804843211776645_2881654419796215357_n.jpg
photoPosition: center
featuredPhotoCredit:
featuredPhotoWebsite:
categories:
  - Ready to Learn Fair
top: true
featured: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            hide_featured: false
            content: >-
              When my boys were young, we were at the library several times a
              week, we went on bike rides, hikes, and ferry rides. They did
              swimming lessons and summer sports camps. And, because I was a
              teacher, they read. And read. And read some more.


              But as a former teacher, I also knew that not all kids spent their
              summer this way. Summer learning loss is real, and understandable.
              It’s why ASF has funded Summer Impact for the past five years.
              This summer will be the last year that 100 K-5th grade students
              will attend a four-week program that allows them to learn science,
              math, reading and social studies while exploring our island on
              adventurous, educational field trips. I’d like to shout out
              Fidalgo Dancworks, our amazing partner, for providing the
              enrichment activities each day that make Impact feel more like a
              fun camp than school. Teachers from Anacortes Community Theater,
              Fidalgo Danceworks, Anacortes Music Project and Anacortes Arts
              Festival run acting, dance, music and art workshops through the
              summer. And it’s all free, including bus transportation and
              lunches. We’ve been able to run Impact thanks to a generous
              anonymous donor, and we couldn’t be more grateful.


              This month also finds our Ready to Learn Fair committee hard at
              work, making sure all the pieces are in place for August 17th when
              we will welcome hundreds of families to come learn about free and
              reduced resources from our 30+ community partners. Students will
              receive free backpacks and school supplies, books, haircuts,
              clothing and lunch. The joy is palpable.


              Again, we have our donors and sponsors to thank for helping us put
              on a fantastic and meaningful event each August. If you’d like to
              donate, please click on the donate button of our
              website–[www.asfkids.org](http://www.asfkids.org) or call me at
              360-488-2343.


              Anacortes is the best place to be in the summertime. Here’s to
              long days, no humidity, endless trails, ice cream and yes,
              reading.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
