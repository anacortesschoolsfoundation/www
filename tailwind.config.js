module.exports = {
  content: ["layouts/**/*.html", "content/**/*.html", "content/**/*.md", "static/js/functions.js", "component-library/**/*.html"],
  theme: {
    extend: {
      colors: {
        asfpurple: "#4b2e62",
        blackenedpearl: "#4E4b51",
        ctsgold: "#b77b20",
        ctsgreen: "#33443c",
        ctslightblue: "#9ed0f0",
        ctsmidblue: "#6ab4e4",
        ctswhite: "#F8F2E6",
        ctsplum: "#692b39",
        ctsred: "#eb293b",
        driedmoss: "#cdbc7e",
        fidalgoblue: "#275793",
        guemesblue: "#192f5e",
        hfgreen: "#26606D",
        indigo: "#1a2244",
        silver: "#999b9b",
        ultraviolet: "#6e5c97",
        moodyblue: "#0D1B25",
        lessmoody: "#2D556D",
        ocean: "#1B405A",
        denim: "#21364B",
        primary: "#4b2e62",
        secondary: "#999b9b",
        success: "#28a745",
        info: "#6e5c97",
        warning: "#cdbc7e",
        danger: "#eb293b",
      },
      fontFamily: {
        'script': ['"Dancing Script"'],
        'vibes': ['"Great Vibes"'],
        'caps': ['Cinzel'],
        'aleo': ['Aleo']
      }
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
}
